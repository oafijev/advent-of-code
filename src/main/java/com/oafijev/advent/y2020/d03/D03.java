package com.oafijev.advent.y2020.d03;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class D03 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part1(input);
    }

    private static void part1(List<String> input) {
        char[][] map = new char[input.size()][input.get(0).length()];
        int mapIndex = 0;
        for (String line : input) {
            for (int i = 0; i < line.length(); i++) {
                map[mapIndex][i] = line.charAt(i);
            }
            mapIndex++;
        }

        System.out.println(getCount(input, 1, 2));

        System.out.println("part 1: " + getCount(input, 3, 1));
        System.out.println("part 2: " + (
                getCount(input, 1, 1)
                * getCount(input, 3, 1)
                * getCount(input, 5, 1)
                * getCount(input, 7, 1)
                * getCount(input, 1, 2)
        ));
    }

    private static long getCount(List<String> input, int rightAmount, int downAmount) {
        long count = 0;
        for (int row = 0, col = 0; row < input.size(); row += downAmount, col += rightAmount) {
            if (input.get(row).charAt(col % input.get(row).length()) == '#') {
                count++;
            }
        }
        System.out.println(" (" + rightAmount + "," + downAmount + "): " + count);
        return count;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d03/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
