package com.oafijev.advent.y2020.d01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D01 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part2(input);
    }

    private static void part2(List<String> input) {
        for (int i = 0; i < input.size() - 2; i++) {
            for (int j = i; j < input.size() - 1; j++) {
                for (int k = j; k < input.size(); k++) {
                    long a = Long.parseLong(input.get(i));
                    long b = Long.parseLong(input.get(j));
                    long c = Long.parseLong(input.get(k));
                    if ((a + b + c) == 2020) {
                        System.out.println(a + " * " + b + "* " + c + " = " + (a * b * c));
                        return;
                    }
                }
            }
        }
    }

    private static void part1(List<String> input) {
        for (int i = 0; i < input.size() - 1; i++) {
            for (int j = i; j < input.size(); j++) {
                long a = Long.parseLong(input.get(i));
                long b = Long.parseLong(input.get(j));
                if ((a + b) == 2020) {
                    System.out.println(a + " * " + b + " = " + (a * b));
                    return;
                }
            }
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d01/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
