package com.oafijev.advent.y2020.d07;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D07 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part2(input);
    }

    public static class DependentBag {
        private int amount;
        private String name;

        DependentBag(int amount, String name) {
            this.amount = amount;
            this.name = name;
        }

        public int getAmount() {
            return amount;
        }

        public String getName() {
            return name;
        }
    }

    private static void part1(List<String> input) {
        Map<String, List<DependentBag>> rules = readRules(input);

        int count = 0;
        for (String rule : rules.keySet()) {
            if (recursiveCheckCanContainGoldenBag(rule, rules)) {
                count++;
            } else {

            }
        }

        System.out.println("part1: " + (count - 1));
    }

    private static void part2(List<String> input) {
        Map<String, List<DependentBag>> rules = readRules(input);

        int count = recursiveAddContainingBags("shiny gold", rules);

        System.out.println("part2: " + (count - 1));
    }

    private static int recursiveAddContainingBags(String key, Map<String, List<DependentBag>> rules) {
        int count = 1;

        for (DependentBag bag : rules.get(key)) {
            count += bag.getAmount() * recursiveAddContainingBags(bag.getName(), rules);
        }

        return count;
    }

    private static Map<String, List<DependentBag>> readRules(List<String> input) {
        Map<String, List<DependentBag>> rules = new HashMap<>();

        for (String line : input) {
            String[] inputOutput = line.split(" bags contain ");
            System.out.println(inputOutput[0]);
            String[] outputs = inputOutput[1].split(", ");
            List<DependentBag> bagRules = new ArrayList<>();
            for (String output : outputs) {
                String[] bag = output.split(" ");
                try {
                    int amount = Integer.parseInt(bag[0]);
                    String key = bag[1] + " " + bag[2];
                    bagRules.add(new DependentBag(amount, key));
                } catch (NumberFormatException e) {

                }
            }

            rules.put(inputOutput[0], bagRules);
        }
        return rules;
    }

    private static boolean recursiveCheckCanContainGoldenBag(String rule, Map<String, List<DependentBag>> rules) {
        List<DependentBag> rulesForBag = rules.get(rule);
        boolean result = false;

        if ("shiny gold".equals(rule)) {
            return true;
        }

        for (DependentBag dependentBag : rulesForBag) {
            result = result || recursiveCheckCanContainGoldenBag(dependentBag.getName(), rules);
        }
        return result;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d07/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
