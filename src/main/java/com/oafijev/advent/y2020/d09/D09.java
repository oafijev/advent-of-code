package com.oafijev.advent.y2020.d09;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D09 {

    private static final int PREAMBLE_LENGTH = 25;
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part1(input);
    }

    private static void part1(List<String> input) {
        long[] numbers = new long[input.size()];
        int index = 0;
        for (String line : input) {
            numbers[index] = Long.parseLong(line);
            index++;
        }

        long invalidNumber = -1;
        for (int i = PREAMBLE_LENGTH; i < numbers.length; i++) {
            boolean valid = checkValid(i, PREAMBLE_LENGTH, numbers);
            if (!valid) {
                invalidNumber = numbers[i];
                System.out.println("Part1: " + numbers[i]);
                break;
            }
        }

        for (int i = 0; i < numbers.length + 1; i++) {
            long sum = 0;
            long minInRange = Long.MAX_VALUE;
            long maxInRange = 0;
            for (int j = i; j < numbers.length; j++) {
                if (numbers[j] < minInRange) {
                    minInRange = numbers[j];
                }
                if (numbers[j] > maxInRange) {
                    maxInRange = numbers[j];
                }
                sum += numbers[j];
                if (sum < invalidNumber) {
                    continue;
                } else if (sum == invalidNumber) {
                    long weakness = maxInRange + minInRange;
                    if (i != j) {
                        System.out.println(minInRange + ", " + maxInRange);
                        System.out.println("Part2: " + weakness);
                    }
                    break;
                } else if (sum > invalidNumber) {
                    break;
                }
            }
        }
    }

    private static boolean checkValid(int index, int preambleLength, long[] numbers) {
        for (int i = index - preambleLength; i < index - 1; i++) {
            for (int j = i + 1; j < index; j++) {
                if (numbers[i] + numbers[j] == numbers[index]) {
                    return true;
                }
            }
        }
        return false;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d09/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
