package com.oafijev.advent.y2020.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class D08 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part2(input);
    }

    private static void part1(List<String> input) {
        List<Instruction> instructions = parseInstructions(input);

        execute(instructions);
        System.out.println("Part 1 answer directly above.");
    }

    private static void part2(List<String> input) {
        List<Instruction> instructions = parseInstructions(input);
        for (int i = 0; i < instructions.size(); i++) {
            instructions.get(i).swapNopJmp();
            if (execute(instructions)) {
                System.out.println("Part 2 terminated properly.");
                break;
            }
            instructions = parseInstructions(input);
        }

    }

    private static List<Instruction> parseInstructions(List<String> input) {
        List<Instruction> instructions = new ArrayList<>();
        for (String line : input) {
            String[] split = line.split(" ");
            instructions.add(new Instruction(split[0], Integer.parseInt(split[1])));
        }
        return instructions;
    }

    private static boolean execute(List<Instruction> instructions) {
        int accumulator = 0;
        int instructionPointer = 0;

        while (instructionPointer < instructions.size()) {
            Instruction currentInstruction = instructions.get(instructionPointer);

            if (currentInstruction.isExecuted()) {
                System.out.println("infinite loop, accumulator: " + accumulator);
                return false;
            }
            currentInstruction.setExecuted(true);

            switch(currentInstruction.getOp()) {
                case "acc":
                    instructionPointer++;
                    accumulator += currentInstruction.getArg();
                    break;
                case "jmp":
                    instructionPointer += currentInstruction.getArg();
                    break;
                case "nop":
                    instructionPointer++;
                    break;
                default:
                    System.out.println("unknown op " + currentInstruction.getOp());
            }
        }

        System.out.println("Successful termination with accumulator: " + accumulator);
        return true;
    }

    private static class Instruction {
        private String op;
        private int arg;
        private boolean executed = false;

        public Instruction(String op, int arg) {
            this.op = op;
            this.arg = arg;
        }

        public String getOp() {
            return op;
        }

        public int getArg() {
            return arg;
        }

        public boolean isExecuted() {
            return executed;
        }

        public void setExecuted(boolean executed) {
            this.executed = executed;
        }

        public void swapNopJmp() {
            if ("nop".equals(this.op)) {
                this.op = "jmp";
            } else if ("jmp".equals(this.op)) {
                this.op = "nop";
            }
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
