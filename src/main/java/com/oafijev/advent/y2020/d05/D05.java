package com.oafijev.advent.y2020.d05;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D05 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part1(input);
    }

    private static void part1(List<String> input) {
        List<Integer> seatIds = new ArrayList<>();


        int highestSeatId = 0;
        for (String line : input) {
            int id = getId(line);
            seatIds.add(id);
            if (id > highestSeatId) {
                highestSeatId = id;
            }
        }

        Collections.sort(seatIds);
        System.out.println("part 1: " + highestSeatId);

        for (int i = 0; i < seatIds.size(); i++) {
            if (!seatIds.contains(i)) {
                System.out.println("Missing " + i);
            }
        }
    }

    private static int getId(String seat) {
        return getRow(seat) * 8 + getColumn(seat);
    }

    private static int getRow(String seat) {
        int row = Integer.parseInt(seat.substring(0, 7)
                .replaceAll("F", "0")
                .replaceAll("B", "1"), 2);
        return row;
    }

    private static int getColumn(String seat) {
        int row = Integer.parseInt(seat.substring(7)
                .replaceAll("L", "0")
                .replaceAll("R", "1"), 2);
        return row;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d05/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
