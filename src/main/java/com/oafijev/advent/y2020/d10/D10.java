package com.oafijev.advent.y2020.d10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class D10 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part1(input);
    }

    private static void part1(List<String> input) {
        List<Integer> adapters = new ArrayList<>();
        for (String line : input) {
            adapters.add(Integer.parseInt(line));
        }

        Collections.sort(adapters);
        int count1 = 0;
        int count3 = 1;
        int last = 0;
        for (Integer adapter : adapters) {
            if (adapter - last == 1) {
                count1++;
            } else if (adapter - last == 3) {
                count3++;
            }
            last = adapter;
        }

        System.out.println("Part1: " + (count1 * count3));

        // calculate reach backwards
        adapters.add(0, 0);
        int size = adapters.size();
        long[] reach = new long[size];
        reach[size - 1] = 1;
        for (int i = size - 2; i >= 0; i--) {
            reach[i] = 0;
            if (i < size - 1) {
                if (adapters.get(i+1) - adapters.get(i) <= 3) {
                    reach[i] += reach[i+1];
                }
            }

            if (i < size - 2) {
                if (adapters.get(i+2) - adapters.get(i) <= 3) {
                    reach[i] += reach[i+2];
                }
            }

            if (i < size - 3) {
                if (adapters.get(i+3) - adapters.get(i) <= 3) {
                    reach[i] += reach[i+3];
                }
            }
        }

        System.out.println("part2: " + reach[0]);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
