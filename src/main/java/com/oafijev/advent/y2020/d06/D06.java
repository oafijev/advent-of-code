package com.oafijev.advent.y2020.d06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D06 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part2(input);
    }

    private static void part2(List<String> input) {
        List<Set<String>> groups = new ArrayList<>();

        Set<String> group = new HashSet<>();
        boolean initializing = true;
        for (String line : input) {
            if ("".equals(line)) {
                System.out.println("Writing entry");
                groups.add(group);
                group = new HashSet<>();
                initializing = true;
                continue;
            }

            if (initializing) {
                for (String key : line.split("")) {
                    group.add(key);
                }
                initializing = false;
            } else {
                group.removeIf(key -> line.indexOf(key) < 0);
            }
        }
        groups.add(group);

        printSum(groups);
    }

    private static void part1(List<String> input) {
        List<Set<String>> groups = new ArrayList<>();

        Set<String> group = new HashSet<>();
        for (String line : input) {
            if ("".equals(line) && group.size() > 0) {
                System.out.println("Writing entry");
                groups.add(group);
                group = new HashSet<>();
                continue;
            }

            for (String key : line.split("")) {
                group.add(key);
            }
        }
        if (group.size() > 0) {
            groups.add(group);
        }

        printSum(groups);
    }

    private static void printSum(List<Set<String>> groups) {
        int total = groups.stream().reduce(0, (subtotal, g) -> {
            System.out.println(g.size());
            return subtotal + g.size();
        }, Integer::sum);
        System.out.println(total);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d06/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
