package com.oafijev.advent.y2020.d04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D04 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part1(input);
    }

    private static void part1(List<String> input) {
        List<Map<String, String>> passports = new ArrayList<>();

        Map<String, String> passportAccumulator = new LinkedHashMap<>();

        for (String line : input) {
            if ("".equals(line)) {
                passports.add(passportAccumulator);
                passportAccumulator = new LinkedHashMap<>();
            } else {
                String[] fields = line.split(" ");
                for (String field : fields) {
                    String[] split = field.split(":");
                    passportAccumulator.put(split[0], split[1]);
                }
            }
        }
        passports.add(passportAccumulator);

        int count = 0;
        Set<String> necessary = new HashSet<>();
        Collections.addAll(necessary, "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid");
        for (Map<String, String> passport : passports) {
            if (!passport.keySet().containsAll(necessary)) {
                continue;
            } else if (validate(passport)) {
                count++;
            }
        }

        System.out.println("part 1: " + count);
    }

    private static boolean validate(Map<String, String> passport) {
        try {
            return validateYear(passport,"byr", 1920, 2002)
                    && validateYear(passport,"iyr", 2010, 2020)
                    && validateYear(passport,"eyr", 2020, 2030)
                    && validateHeight(passport)
                    && validateHairColour(passport)
                    && validateEyeColour(passport)
                    && validatePid(passport)
                    && true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    private static boolean validatePid(Map<String, String> passport) {
        String field = passport.get("pid");
        long pid = Long.parseLong(field);
        return field.length() == 9;
    }

    private static boolean validateEyeColour(Map<String, String> passport) {
        String field = passport.get("ecl");
        Set<String> colours = new HashSet<>();
        Collections.addAll(colours, "amb", "blu", "brn", "gry", "grn", "hzl", "oth");
        return colours.contains(field);
    }

    private static boolean validateHairColour(Map<String, String> passport) {
        String field = passport.get("hcl");
        Long colour = Long.parseLong(field.substring(1), 16);
        return field.charAt(0) == '#';
    }

    private static boolean validateHeight(Map<String, String> passport) {
        String field = passport.get("hgt");
        int unitIndex = field.length() - 2;
        String unit = field.substring(unitIndex);
        int measure = Integer.parseInt(field.substring(0, unitIndex));
        if ("cm".equals(unit)) {
            return 150 <= measure && measure <= 193;
        } else if ("in".equals(unit)) {
            return 59 <= measure && measure <= 76;
        } else {
            throw new IllegalArgumentException("Bad unit, invalid: " + unit);
        }
    }

    private static boolean validateYear(Map<String, String> passport, String key, int start, int end) {
        String value = passport.get(key);
        int year = Integer.parseInt(value);
        return (year >= start) && (year <= end);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d04/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
