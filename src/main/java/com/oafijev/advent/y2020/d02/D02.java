package com.oafijev.advent.y2020.d02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class D02 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        part2(input);
    }

    private static void part2(List<String> input) {
        int total = 0;
        for (String line : input) {
            String[] split = line.split(" ");
            int min = Integer.parseInt(split[0].split("-")[0]);
            int max = Integer.parseInt(split[0].split("-")[1]);
            char symbol = split[1].charAt(0);
            String password = split[2];

            System.out.println(min + "-" + max + " [" + symbol + "] : " + password);
            if (password.charAt(min-1) == symbol ^ password.charAt(max-1) == symbol) {
                total++;
            }
        }
        System.out.println("part2: " + total);
    }

    private static void part1(List<String> input) {
        int total = 0;
        for (String line : input) {
            String[] split = line.split(" ");
            int min = Integer.parseInt(split[0].split("-")[0]);
            int max = Integer.parseInt(split[0].split("-")[1]);
            char symbol = split[1].charAt(0);
            String password = split[2];
            String[] tmp = split[2].split("");

            System.out.println(min + "-" + max + " [" + symbol + "] : " + password);
            long count = Stream.of(tmp).filter(s -> s.charAt(0) == symbol).count();
            System.out.println(count);
            if (min <= count && count <= max) {
                total++;
            }
        }
        System.out.println("part1: " + total);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2020/d02/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
