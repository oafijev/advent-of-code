package com.oafijev.advent.y2018.d15;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class D15A {

    public static int elfAttack = 3;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        while (true) {
            List<Unit> allUnits = new ArrayList<>();
            String[][] map = getMap(input, allUnits);

            int rounds = 0;
            boolean done = false;
            while (isBothSidesAlive(allUnits)) {
                Collections.sort(allUnits, new HasLocationOrderComparator());
                for (Unit unit : allUnits) {
                    if (!isBothSidesAlive(allUnits)) {
                        done = true;
                        break;
                    } else if (unit.isDead()) {
                        continue;
                    } else {
                        if (!inRangeOfEnemy(unit, allUnits)) {
                            List<Square> squares = getSquaresAdjacentToEnemies(map, unit, allUnits);
                            moveUnit(map, squares, unit, allUnits);
                        }
                        attackAdjacentUnits(unit, allUnits.stream().filter(a -> a.getType() != unit.getType()).collect(Collectors.toList()));
                    }
                }
                printMap(map, allUnits);
                if (done) {
                    break;
                }
                rounds++;
            }

            if (allUnits.stream().filter(Unit::isDead).noneMatch(a -> a.getType() == UnitType.ELF)) {
                finish(map, allUnits, rounds);
                System.out.println(elfAttack);
                break;
            }
            elfAttack++;
        }
    }

    private static void finish(String[][] map, List<Unit> allUnits, int rounds) {
        printMap(map, allUnits);
        int remainingHp = 0;
        for (Integer hp : allUnits.stream().filter(Unit::isAlive).map(Unit::getHp).collect(Collectors.toList())) {
            remainingHp += hp;
        }
        System.out.println(rounds);
        System.out.println(remainingHp);
        System.out.println(rounds * remainingHp);
    }

    private static boolean isBothSidesAlive(List<Unit> allUnits) {
        return allUnits.stream().filter(Unit::isAlive).anyMatch(a -> a.getType() == UnitType.GOBLIN) && allUnits.stream().filter(Unit::isAlive).anyMatch(a -> a.getType() == UnitType.ELF);
    }

    private static void attackAdjacentUnits(Unit unit, List<Unit> enemies) {
        Unit nUnit = null;
        Unit wUnit = null;
        Unit eUnit = null;
        Unit sUnit = null;
        int leastHp = Integer.MAX_VALUE;

        nUnit = enemies.stream().filter(Unit::isAlive).filter(a -> a.getRow() == unit.getRow() - 1 && a.getColumn() == unit.getColumn()).findFirst().orElse(null);
        wUnit = enemies.stream().filter(Unit::isAlive).filter(a -> a.getRow() == unit.getRow() && a.getColumn() == unit.getColumn() - 1).findFirst().orElse(null);
        eUnit = enemies.stream().filter(Unit::isAlive).filter(a -> a.getRow() == unit.getRow() && a.getColumn() == unit.getColumn() + 1).findFirst().orElse(null);
        sUnit = enemies.stream().filter(Unit::isAlive).filter(a -> a.getRow() == unit.getRow() + 1 && a.getColumn() == unit.getColumn()).findFirst().orElse(null);

        if (nUnit != null && nUnit.getHp() < leastHp) {
            leastHp = nUnit.getHp();
        }
        if (eUnit != null && eUnit.getHp() < leastHp) {
            leastHp = eUnit.getHp();
        }
        if (wUnit != null && wUnit.getHp() < leastHp) {
            leastHp = wUnit.getHp();
        }
        if (sUnit != null && sUnit.getHp() < leastHp) {
            leastHp = sUnit.getHp();
        }

        Unit enemy = null;
        if (nUnit != null && nUnit.getHp() == leastHp) {
            enemy = nUnit;
        } else if (wUnit != null && wUnit.getHp() == leastHp) {
            enemy = wUnit;
        } else if (eUnit != null && eUnit.getHp() == leastHp) {
            enemy = eUnit;
        } else if (sUnit != null && sUnit.getHp() == leastHp) {
            enemy = sUnit;
        }

        if (enemy == null) {
            return;
        }

        enemy.setHp(enemy.getHp() - unit.getAttack());
        if (enemy.getHp() <= 0) {
            enemy.setDead(true);
        }
    }

    private static void moveUnit(String[][] map, List<Square> squares, Unit unit, List<Unit> allUnits) {
        Map<Integer, List<Square>> reachableDistancesAndSquares = new HashMap<Integer, List<Square>>();

        Integer[][] distances = generateDistanceMatrixFromSquare(map, unit, allUnits);

        for (Square square : squares) {
            Integer distance = distances[square.getRow()][square.getColumn()];
            if (distance != null && distance > 0) {
                if (!reachableDistancesAndSquares.containsKey(distance)) {
                    reachableDistancesAndSquares.put(distance, new ArrayList<>());
                }
                reachableDistancesAndSquares.get(distance).add(square);
            }
        }

        List<Integer> reachableDistances = new ArrayList<>(reachableDistancesAndSquares.keySet());
        Collections.sort(reachableDistances);

        if (reachableDistances.isEmpty()) {
            return;
        }

        List<Square> nearestSquares = reachableDistancesAndSquares.get(reachableDistances.get(0));
        Collections.sort(nearestSquares, new HasLocationOrderComparator()); // nearest square by reading order
        Square squareToHeadTowards = nearestSquares.get(0);

//		System.out.println("Calculating distances for " + unit.getType());
//		printMap(distances);

        List<Unit> allUnitsButMyself = new ArrayList<>(allUnits.stream().filter(Unit::isAlive).collect(Collectors.toList()));
        allUnitsButMyself.remove(unit);
        Integer[][] backwardsDistances = generateDistanceMatrixFromSquare(map, squareToHeadTowards, allUnitsButMyself);

//		System.out.println("Calculating back distances back to " + unit.getType());
//		printMap(backwardsDistances);

        int distanceBackToUnit = backwardsDistances[unit.getRow()][unit.getColumn()];
        int distanceOneStepFromUnit = distanceBackToUnit - 1;
        if (backwardsDistances[unit.getRow() - 1][unit.getColumn()] != null && backwardsDistances[unit.getRow() - 1][unit.getColumn()] == distanceOneStepFromUnit) {
            unit.setRow(unit.getRow() - 1);
        } else if (backwardsDistances[unit.getRow()][unit.getColumn() - 1] != null && backwardsDistances[unit.getRow()][unit.getColumn() - 1] == distanceOneStepFromUnit) {
            unit.setColumn(unit.getColumn() - 1);
        } else if (backwardsDistances[unit.getRow()][unit.getColumn() + 1] != null && backwardsDistances[unit.getRow()][unit.getColumn() + 1] == distanceOneStepFromUnit) {
            unit.setColumn(unit.getColumn() + 1);
        } else if (backwardsDistances[unit.getRow() + 1][unit.getColumn()] != null && backwardsDistances[unit.getRow() + 1][unit.getColumn()] == distanceOneStepFromUnit) {
            unit.setRow(unit.getRow() + 1);
        } else {
            throw new IllegalArgumentException("big problem pathing back from reachable square to head towards, back to the unit.");
        }

    }

    private static void printMap(Integer[][] map) {
        for (Integer[] objs : map) {
            for (Integer obj : objs) {
                System.out.print(obj + "\t");
            }
            System.out.println();
        }
    }

    private static Integer[][] generateDistanceMatrixFromSquare(String[][] map, HasLocation location, List<Unit> allUnits) {
        Integer[][] distances = new Integer[map.length][map[0].length];
        distances[location.getRow()][location.getColumn()] = 0;
        ArrayDeque<Square> queue = new ArrayDeque<>();
        queue.addLast(new Square(location.getRow(), location.getColumn()));

        while (!queue.isEmpty()) {
            Square square = queue.removeFirst();
            int currentDistance = distances[square.getRow()][square.getColumn()];

            if (openAndUnoccupied(map, square.getRow() - 1, square.getColumn(), allUnits) && distances[square.getRow() - 1][square.getColumn()] == null) {
                distances[square.getRow() - 1][square.getColumn()] = currentDistance + 1;
                queue.addLast(new Square(square.getRow() - 1, square.getColumn()));
            }
            if (openAndUnoccupied(map, square.getRow() + 1, square.getColumn(), allUnits) && distances[square.getRow() + 1][square.getColumn()] == null) {
                distances[square.getRow() + 1][square.getColumn()] = currentDistance + 1;
                queue.addLast(new Square(square.getRow() + 1, square.getColumn()));
            }
            if (openAndUnoccupied(map, square.getRow(), square.getColumn() - 1, allUnits) && distances[square.getRow()][square.getColumn() - 1] == null) {
                distances[square.getRow()][square.getColumn() - 1] = currentDistance + 1;
                queue.addLast(new Square(square.getRow(), square.getColumn() - 1));
            }
            if (openAndUnoccupied(map, square.getRow(), square.getColumn() + 1, allUnits) && distances[square.getRow()][square.getColumn() + 1] == null) {
                distances[square.getRow()][square.getColumn() + 1] = currentDistance + 1;
                queue.addLast(new Square(square.getRow(), square.getColumn() + 1));
            }
        }
        return distances;
    }

    private static List<Square> getSquaresAdjacentToEnemies(String[][] map, Unit unit, List<Unit> allUnits) {
        List<Square> result = new ArrayList<Square>();

        for (Unit enemy : allUnits.stream().filter(Unit::isAlive).filter(a -> a.getType() != unit.getType()).collect(Collectors.toList())) {
            if (openAndUnoccupied(map, enemy.getRow(), enemy.getColumn() - 1, allUnits)) {
                result.add(new Square(enemy.getRow(), enemy.getColumn() - 1));
            }
            if (openAndUnoccupied(map, enemy.getRow(), enemy.getColumn() + 1, allUnits)) {
                result.add(new Square(enemy.getRow(), enemy.getColumn() + 1));
            }
            if (openAndUnoccupied(map, enemy.getRow() - 1, enemy.getColumn(), allUnits)) {
                result.add(new Square(enemy.getRow() - 1, enemy.getColumn()));
            }
            if (openAndUnoccupied(map, enemy.getRow() + 1, enemy.getColumn(), allUnits)) {
                result.add(new Square(enemy.getRow() + 1, enemy.getColumn()));
            }
        }

        return result;
    }

    private static boolean openAndUnoccupied(String[][] map, final int row, final int column, List<Unit> allUnits) {
        if (row < 0 || row >= map.length || column < 0 || row >= map[0].length) {
            return false;
        } else if (map[row][column].equals("#")) {
            return false;
        } else if (allUnits.stream().filter(Unit::isAlive).anyMatch(a -> a.getRow() == row && a.getColumn() == column)) {
            return false;
        }
        return true;
    }

    public static class Square implements HasLocation {
        private int row;
        private int column;

        public Square(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }
    }

    private static boolean inRangeOfEnemy(Unit unit, List<Unit> allUnits) {
        UnitType type = unit.getType();
        for (Unit enemy : allUnits.stream().filter(a -> a.getType() != type).filter(Unit::isAlive).collect(Collectors.toList())) {
            int deltaR = enemy.getRow() - unit.getRow();
            int deltaC = enemy.getColumn() - unit.getColumn();
            if ((Math.abs(deltaR) == 1 && deltaC == 0) || (deltaR == 0 && Math.abs(deltaC) == 1)) {
                return true;
            }
        }
        return false;
    }

    public static interface HasLocation {
        public int getRow();

        public int getColumn();
    }

    public static class HasLocationOrderComparator implements Comparator<HasLocation> {
        @Override
        public int compare(HasLocation o1, HasLocation o2) {
            if (o1.getRow() == o2.getRow()) {
                return o1.getColumn() - o2.getColumn();
            } else {
                return o1.getRow() - o2.getRow();
            }
        }
    }

    private static void printMap(String[][] map, List<Unit> allUnits) {
        for (int r = 0; r < map.length; r++) {
            final int row = r;
            for (int c = 0; c < map[r].length; c++) {
                final int column = c;
                if (allUnits.stream().filter(Unit::isAlive).filter(a -> a.getType() == UnitType.GOBLIN).anyMatch(a -> a.getRow() == row && a.getColumn() == column)) {
                    System.out.print("G");
                } else if (allUnits.stream().filter(Unit::isAlive).filter(a -> a.getType() == UnitType.ELF).anyMatch(a -> a.getRow() == row && a.getColumn() == column)) {
                    System.out.print("E");
                } else {
                    System.out.print(map[r][c]);
                }
            }
            System.out.println();
        }

        for (Unit unit : allUnits.stream().filter(Unit::isAlive).collect(Collectors.toList())) {
            System.out.println(unit.getType() + "(" + unit.getHp() + ")");
        }
    }

    private static String[][] getMap(List<String> input, List<Unit> allUnits) {
        String[][] result = new String[input.size()][input.get(0).length()];
        int row = 0;
        for (String line : input) {
            int column = 0;
            for (String square : line.split("")) {
                if ("#".equals(square)) {
                    result[row][column] = "#";
                } else {
                    result[row][column] = ".";
                    if ("G".equals(square)) {
                        allUnits.add(new Unit(row, column, UnitType.GOBLIN));
                    } else if ("E".equals(square)) {
                        allUnits.add(new Unit(row, column, UnitType.ELF));
                    }
                }
                column++;
            }
            row++;
        }
        return result;
    }

    public enum UnitType {
        GOBLIN, ELF;
    }

    public static class Unit implements HasLocation {
        int row;
        int column;
        int hp = 200;
        UnitType type;
        boolean dead = false;

        public boolean isDead() {
            return dead;
        }

        public boolean isAlive() {
            return !dead;
        }

        public void setDead(boolean dead) {
            this.dead = dead;
        }

        public Unit(int row, int column, UnitType type) {
            this.row = row;
            this.column = column;
            this.type = type;
        }

        public UnitType getType() {
            return type;
        }

        public void setType(UnitType type) {
            this.type = type;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getHp() {
            return hp;
        }

        public void setHp(int hp) {
            this.hp = hp;
        }

        public int getAttack() {
            return getType() == UnitType.GOBLIN ? 3 : elfAttack;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d15/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
