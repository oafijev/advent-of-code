package com.oafijev.advent.y2018.d15;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D15B {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        System.out.println(line);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d15/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
