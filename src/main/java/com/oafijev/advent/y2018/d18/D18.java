package com.oafijev.advent.y2018.d18;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D18 {
    public static int MAXROW;
    public static int MAXCOL;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String[][] map = new String[input.size()][input.get(0).length()];
        int row = 0;
        for (String line : input) {
            for (int col = 0; col < line.length(); col++) {
                map[row][col] = "" + line.charAt(col);
            }
            row++;
        }

        MAXROW = map.length;
        MAXCOL = map[0].length;

        for (int round = 0; round < 1000000000; round++) {
            if (round == 10) {
                System.out.println("Part A: " + count(map, "|") * count(map, "#"));
            }
            if (round % 10000 == 0) {
                // notice this repeats every 70000.
                System.out.println("Round " + round + ": " + count(map, "|") * count(map, "#"));
            }
            map = doMinute(map);
//			printMap(map);
        }

    }

    private static int count(String[][] map, String value) {
        int count = 0;
        for (String[] line : map) {
            for (String square : line) {
                count += square.equals(value) ? 1 : 0;
            }
        }
        return count;
    }

    private static String[][] doMinute(String[][] map) {
        String[][] newmap = new String[map.length][];

        for (int r = 0; r < map.length; r++) {
            String[] row = map[r];
            String[] newrow = new String[row.length];
            for (int c = 0; c < row.length; c++) {
                newrow[c] = getNewSquare(map, r, c);
            }
            newmap[r] = newrow;
        }
        return newmap;
    }

    private static String getNewSquare(String[][] map, int r, int c) {
        if (map[r][c].equals(".")) {
            if (adjacentTo(map, r, c, "|", 3)) {
                return "|";
            } else {
                return ".";
            }
        }

        if (map[r][c].equals("|")) {
            if (adjacentTo(map, r, c, "#", 3)) {
                return "#";
            } else {
                return "|";
            }
        }

        if (map[r][c].equals("#")) {
            if (adjacentTo(map, r, c, "#", 1) && adjacentTo(map, r, c, "|", 1)) {
                return "#";
            } else {
                return ".";
            }
        }

        throw new IllegalArgumentException();
    }

    private static boolean adjacentTo(String[][] map, int r, int c, String value, int matches) {
        int count = 0;
        count += oneIfInBoundsAndMatches(map, r - 1, c - 1, value);
        count += oneIfInBoundsAndMatches(map, r - 1, c, value);
        count += oneIfInBoundsAndMatches(map, r - 1, c + 1, value);
        count += oneIfInBoundsAndMatches(map, r, c - 1, value);
        count += oneIfInBoundsAndMatches(map, r, c + 1, value);
        count += oneIfInBoundsAndMatches(map, r + 1, c - 1, value);
        count += oneIfInBoundsAndMatches(map, r + 1, c, value);
        count += oneIfInBoundsAndMatches(map, r + 1, c + 1, value);

        return count >= matches;
    }

    private static int oneIfInBoundsAndMatches(String[][] map, int r, int c, String value) {
        if (r >= 0 && r < MAXROW && c >= 0 && c < MAXCOL && map[r][c].equals(value)) {
            return 1;
        }
        return 0;
    }

    private static void printMap(String[][] map) {
        for (String[] line : map) {
            for (String square : line) {
                System.out.print(square);
            }
            System.out.println();
        }
        System.out.println();
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d18/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
