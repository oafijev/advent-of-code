package com.oafijev.advent.y2018.d16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D16B {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Cpu cpu = new Cpu();

        int samplesMatchingThreeOrMoreOpcodes = 0;
        int samplesNotMatching = 0;
        int totalSamples = 0;

        List<String> unmatched = new ArrayList<>();
        unmatched.addAll(getOpcodes());
        Map<Integer, String> numberToOpcode = new HashMap<>();

        int i = 0;
        for (; i < input.size(); i = i + 4) {
            if (input.get(i).trim().equals("")) {
                break;
            }
            int[] before = mapToInt(input.get(i).substring(9, input.get(i).length() - 1).split(", "));
            int[] instruction = mapToInt(input.get(i + 1).split(" "));
            int[] after = mapToInt(input.get(i + 2).substring(9, input.get(i + 2).length() - 1).split(", "));

            if (numberToOpcode.containsKey(instruction[0])) {
                continue;
            }

            int matchCount = 0;
            List<String> matches = new ArrayList<>();
            for (String opcode : getOpcodes()) {
                int[] registers = Arrays.copyOf(before, 4);

                if (numberToOpcode.containsValue(opcode)) {
                    continue;
                }
                cpu.apply(opcode, instruction[1], instruction[2], instruction[3], registers);
                if (Arrays.equals(after, registers)) {
//					System.out.println(opcode + " worked!");
                    matches.add(opcode);
                    matchCount++;
                }
            }
            System.out.println(matchCount + " matches");
            if (matchCount == 1) {
                unmatched.remove(matches.get(0));
                numberToOpcode.put(instruction[0], matches.get(0));
            }
        }

        int[] registers = new int[4];
        for (; i < input.size(); i++) {
            if (input.get(i).trim().equals("")) {
                continue;
            }
            int[] instruction = mapToInt(input.get(i).split(" "));
            cpu.apply(numberToOpcode.get(instruction[0]), instruction[1], instruction[2], instruction[3], registers);
        }

        System.out.println("Part B: " + registers[0]);
    }

    public static class Cpu {
        public void apply(String opcode, int a, int b, int c, int[] registers) {
            switch (opcode) {
                case "addr":
                    registers[c] = registers[a] + registers[b];
                    break;
                case "addi":
                    registers[c] = registers[a] + b;
                    break;
                case "mulr":
                    registers[c] = registers[a] * registers[b];
                    break;
                case "muli":
                    registers[c] = registers[a] * b;
                    break;
                case "banr":
                    registers[c] = registers[a] & registers[b];
                    break;
                case "bani":
                    registers[c] = registers[a] & b;
                    break;
                case "borr":
                    registers[c] = registers[a] | registers[b];
                    break;
                case "bori":
                    registers[c] = registers[a] | b;
                    break;
                case "setr":
                    registers[c] = registers[a];
                    break;
                case "seti":
                    registers[c] = a;
                    break;
                case "gtir":
                    registers[c] = ((a > registers[b]) ? 1 : 0);
                    break;
                case "gtri":
                    registers[c] = ((registers[a] > b) ? 1 : 0);
                    break;
                case "gtrr":
                    registers[c] = ((registers[a] > registers[b]) ? 1 : 0);
                    break;
                case "eqir":
                    registers[c] = ((a == registers[b]) ? 1 : 0);
                    break;
                case "eqri":
                    registers[c] = ((registers[a] == b) ? 1 : 0);
                    break;
                case "eqrr":
                    registers[c] = ((registers[a] == registers[b]) ? 1 : 0);
                    break;
                default:
                    throw new IllegalArgumentException(opcode);

            }
        }
    }

    private static List<String> getOpcodes() {
        return Arrays.asList(
                "addr", "addi",
                "mulr", "muli",
                "banr", "bani",
                "borr", "bori",
                "setr", "seti",
                "gtir", "gtri", "gtrr",
                "eqir", "eqri", "eqrr");
    }

    private static int[] mapToInt(String[] string) {
        int[] result = new int[string.length];
        for (int i = 0; i < string.length; i++) {
            result[i] = Integer.parseInt(string[i]);
        }
        return result;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d16/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
