package com.oafijev.advent.y2018.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D08A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        String[] params = line.split(" ");
        Queue<String> queue = new LinkedList<>(Arrays.asList(params));

        Node root = readNode(queue);

        int totalMetadata = getTotalMetadata(root);

        System.out.println(totalMetadata);
    }

    private static int getTotalMetadata(Node node) {
        int metadataTotal = 0;
        for (Node child : node.getChildren()) {
            metadataTotal += getTotalMetadata(child);
        }

        for (String data : node.getMetadata()) {
            metadataTotal += Integer.valueOf(data);
        }
        return metadataTotal;
    }

    private static Node readNode(Queue<String> queue) {
        Node node = new Node();

        int childrenCount = Integer.valueOf(queue.remove()).intValue();
        int metadataCount = Integer.valueOf(queue.remove()).intValue();

        for (int i = 0; i < childrenCount; i++) {
            Node child = readNode(queue);
            node.addChild(child);
        }

        for (int i = 0; i < metadataCount; i++) {
            node.addMetadata(queue.remove());
        }

        return node;
    }

    public static class Node {
        private List<Node> children = new ArrayList<>();
        private List<String> metadata = new ArrayList<>();

        private void addChild(Node node) {
            children.add(node);
        }

        public List<Node> getChildren() {
            return this.children;
        }

        private void addMetadata(String data) {
            metadata.add(data);
        }

        private List<String> getMetadata() {
            return this.metadata;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
