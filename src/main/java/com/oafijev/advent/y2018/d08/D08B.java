package com.oafijev.advent.y2018.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D08B {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        String[] params = line.split(" ");
        Queue<String> queue = new LinkedList<>(Arrays.asList(params));

        Node root = readNode(queue);

        int totalMetadata = getValue(root);

        System.out.println(totalMetadata);
    }

    private static int getValue(Node node) {
        int totalValue = 0;

        if (node.getChildren().isEmpty()) {
            for (String data : node.getMetadata()) {
                totalValue += Integer.valueOf(data);
            }
        } else {
            for (String indexString : node.getMetadata()) {
                int index = Integer.valueOf(indexString);

                if (index > 0 && index <= node.getChildren().size()) {
                    totalValue += getValue(node.getChildren().get(index - 1));
                }
            }
        }

        return totalValue;
    }

    private static Node readNode(Queue<String> queue) {
        Node node = new Node();

        int childrenCount = Integer.valueOf(queue.remove()).intValue();
        int metadataCount = Integer.valueOf(queue.remove()).intValue();

        for (int i = 0; i < childrenCount; i++) {
            Node child = readNode(queue);
            node.addChild(child);
        }

        for (int i = 0; i < metadataCount; i++) {
            node.addMetadata(queue.remove());
        }

        return node;
    }

    public static class Node {
        private List<Node> children = new ArrayList<>();
        private List<String> metadata = new ArrayList<>();

        private void addChild(Node node) {
            children.add(node);
        }

        public List<Node> getChildren() {
            return this.children;
        }

        private void addMetadata(String data) {
            metadata.add(data);
        }

        private List<String> getMetadata() {
            return this.metadata;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
