package com.oafijev.advent.y2018.d10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D10A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Integer[]> points = new ArrayList<>();
        List<Integer[]> velocities = new ArrayList<>();

        for (String line : input) {
            line = line.replaceAll("position=<", "");
            line = line.replaceAll("> velocity=<", " ");
            line = line.replaceAll(">", "");
            line = line.replaceAll(",", "");
            line = line.replaceAll("  ", " ").trim();
            String[] params = line.split(" ");
            int x = Integer.valueOf(params[0]).intValue();
            int y = Integer.valueOf(params[1]).intValue();
            int vx = Integer.valueOf(params[2]).intValue();
            int vy = Integer.valueOf(params[3]).intValue();

            points.add(new Integer[]{x, y});
            velocities.add(new Integer[]{vx, vy});
        }

        for (int i = 0; i < 10681; i++) {

            int minx = Integer.MAX_VALUE;
            int maxx = Integer.MIN_VALUE;
            int miny = Integer.MAX_VALUE;
            int maxy = Integer.MIN_VALUE;
            for (int j = 0; j < points.size(); j++) {
                points.get(j)[0] += velocities.get(j)[0];
                if (points.get(j)[0] > maxx) {
                    maxx = points.get(j)[0];
                } else if (points.get(j)[0] < minx) {
                    minx = points.get(j)[0];
                }
                if (points.get(j)[1] > maxy) {
                    maxy = points.get(j)[1];
                } else if (points.get(j)[1] < miny) {
                    miny = points.get(j)[1];
                }
                points.get(j)[1] += velocities.get(j)[1];
            }

            if (maxx - minx > 347 || maxy - miny > 291) {
                if (i % 100 == 0) {
                    System.out.println("skipping iteration " + i + " with (xsize, ysize) = (" + (maxx - minx) + ", " + (maxy - miny) + ")");
                }
                continue;
            }

            int xsize = maxx - minx + 1;
            int ysize = maxy - miny + 1;
            String[][] grid = new String[xsize][ysize];
            for (int j = 0; j < points.size(); j++) {
                grid[points.get(j)[0] - minx][points.get(j)[1] - miny] = "#";
            }

            for (int j = 0; j < ysize; j++) {
                for (int k = 0; k < xsize; k++) {
                    if (grid[k][j] != null) {
                        System.out.print("##");
                    } else {
                        System.out.print("..");
                    }
                }
                System.out.println();
            }
            System.out.println(i);
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
