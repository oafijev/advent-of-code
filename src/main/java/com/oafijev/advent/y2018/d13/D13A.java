package com.oafijev.advent.y2018.d13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D13A {
    private static int LEFT = 0;
    private static int UP = 1;
    private static int RIGHT = 2;
    private static int DOWN = 3;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        List<String> grid = new ArrayList<>();

        List<Cart> carts = getCarts(input, grid);

        int iteration = 0;
        System.out.println("number of carts = " + carts.size());
        boolean collided = false;
        while (!collided) {
            System.out.println(iteration);
            for (Cart cart : carts) {
                cart.advance(grid);
                if (collided(carts)) {
                    collided = true;
                    break;
                }
            }
            iteration++;
        }
    }

    private static boolean collided(List<Cart> carts) {
        for (int i = 0; i < carts.size() - 1; i++) {
            for (int j = i + 1; j < carts.size(); j++) {
                boolean collided = carts.get(i).getRow() == carts.get(j).getRow() && carts.get(i).getColumn() == carts.get(j).getColumn();
                if (collided) {
                    System.out.println("Collision between carts " + i + " and " + j);
                    System.out.println(carts.get(i).getColumn() + "," + carts.get(i).getRow());
                    return true;
                }
            }
        }
        return false;
    }

    private static List<Cart> getCarts(List<String> input, List<String> grid) {
        List<Cart> carts = new ArrayList<>();
        int lineCount = 0;
        for (String line : input) {
            while (line.indexOf("<") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("<"), LEFT));
                line = line.replaceFirst("<", "-");
            }
            while (line.indexOf("^") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("^"), UP));
                line = line.replaceFirst("\\^", "|");
            }
            while (line.indexOf(">") > -1) {
                carts.add(new Cart(lineCount, line.indexOf(">"), RIGHT));
                line = line.replaceFirst(">", "-");
            }
            while (line.indexOf("v") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("v"), DOWN));
                line = line.replaceFirst("v", "|");
            }
            grid.add(line);
            lineCount++;
        }

        return carts;
    }

    public static class Cart {
        private int row;
        private int column;
        private int facing;
        private int turns;

        public Cart(int row, int column, int facing) {
            this.row = row;
            this.column = column;
            this.facing = facing;
        }

        public void advance(List<String> grid) {
            if (this.facing == LEFT) {
                this.column--;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = UP;
                } else if (newMapLocation == '/') {
                    this.facing = DOWN;
                } else if (newMapLocation == '+') {
                    this.facing = turns(LEFT, turns);
                    turns++;
                }
            } else if (this.facing == UP) {
                this.row--;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = LEFT;
                } else if (newMapLocation == '/') {
                    this.facing = RIGHT;
                } else if (newMapLocation == '+') {
                    this.facing = turns(UP, turns);
                    turns++;
                }
            } else if (this.facing == RIGHT) {
                this.column++;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = DOWN;
                } else if (newMapLocation == '/') {
                    this.facing = UP;
                } else if (newMapLocation == '+') {
                    this.facing = turns(RIGHT, turns);
                    turns++;
                }
            } else if (this.facing == DOWN) {
                this.row++;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = RIGHT;
                } else if (newMapLocation == '/') {
                    this.facing = LEFT;
                } else if (newMapLocation == '+') {
                    this.facing = turns(DOWN, turns);
                    turns++;
                }
            }

        }

        private int turns(int currentDirection, int turnsSoFar) {
            if ((currentDirection == LEFT && 0 == (turnsSoFar % 3))
                    || (currentDirection == DOWN && 1 == (turnsSoFar % 3))
                    || (currentDirection == RIGHT && 2 == (turnsSoFar % 3))) {
                return DOWN;
            } else if ((currentDirection == UP && 0 == (turnsSoFar % 3))
                    || (currentDirection == LEFT && 1 == (turnsSoFar % 3))
                    || (currentDirection == DOWN && 2 == (turnsSoFar % 3))) {
                return LEFT;
            } else if ((currentDirection == RIGHT && 0 == (turnsSoFar % 3))
                    || (currentDirection == UP && 1 == (turnsSoFar % 3))
                    || (currentDirection == LEFT && 2 == (turnsSoFar % 3))) {
                return UP;
            } else if ((currentDirection == DOWN && 0 == (turnsSoFar % 3))
                    || (currentDirection == RIGHT && 1 == (turnsSoFar % 3))
                    || (currentDirection == UP && 2 == (turnsSoFar % 3))) {
                return RIGHT;
            }
            throw new IllegalArgumentException();
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getFacing() {
            return facing;
        }

        public void setFacing(int facing) {
            this.facing = facing;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d13/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
