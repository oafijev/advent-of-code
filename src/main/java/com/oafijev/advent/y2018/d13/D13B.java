package com.oafijev.advent.y2018.d13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class D13B {
    private static int LEFT = 0;
    private static int UP = 1;
    private static int RIGHT = 2;
    private static int DOWN = 3;
    private static int collidedCount = 0;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        List<String> grid = new ArrayList<>();

        List<Cart> carts = getCarts(input, grid);

        System.out.println("number of carts = " + carts.size());
        while (carts.size() - collidedCount >= 2) {
            for (Cart cart : carts) {
                if (!cart.isCollided()) {
                    cart.advance(grid);
                    doCollisionCheck(cart, carts);
                }
            }

            // This is important because the order in which the carts move does matter.
            Collections.sort(carts, new Comparator<Cart>() {
                @Override
                public int compare(Cart o1, Cart o2) {
                    if (o1.getRow() < o2.getRow()) {
                        return -1;
                    } else if (o1.getRow() > o2.getRow()) {
                        return 1;
                    } else {
                        return (o1.getColumn() - o2.getColumn());
                    }
                }

            });
        }
        List<Cart> remainingCarts = carts.stream().filter(a -> !a.isCollided()).collect(Collectors.toList());
        System.out.println("number of carts = " + remainingCarts.size());
        System.out.println("location: " + remainingCarts.get(0).getColumn() + "," + remainingCarts.get(0).getRow());
        remainingCarts.get(0).advance(grid);
        System.out.println("location: " + remainingCarts.get(0).getColumn() + "," + remainingCarts.get(0).getRow());
    }

    private static void doCollisionCheck(Cart firstCart, List<Cart> carts) {
        for (Cart secondCart : carts) {
            if (firstCart != secondCart && !secondCart.isCollided()) {
                boolean collided = firstCart.getRow() == secondCart.getRow() && firstCart.getColumn() == secondCart.getColumn();
                if (collided) {
                    System.out.println("Collision between carts at " + firstCart.getColumn() + "," + firstCart.getRow());
                    firstCart.setCollided(true);
                    secondCart.setCollided(true);
                    collidedCount += 2;
                    return;
                }
            }
        }
//		for (int i = 0; i < carts.size() - 1; i++) {
//			Cart firstCart = carts.get(i);
//			if (firstCart.isCollided()) {
//				continue;
//			}
//			for (int j = i + 1; j < carts.size(); j++) {
//				Cart secondCart = carts.get(j);
//				if (secondCart.isCollided()) {
//					continue;
//				}
//				boolean collided = firstCart.getRow() == secondCart.getRow() && firstCart.getColumn() == secondCart.getColumn();
//				if (collided) {
//					for (int k = j + 1; k < carts.size(); k++) {
//						Cart thirdCart = carts.get(k);
//						if (firstCart.getRow() == thirdCart.getRow() && firstCart.getColumn() == thirdCart.getColumn()) {
//							System.out.println("THIRD CART");
//						}
//					}
//					System.out.println("Collision between carts " + i + " and " + j);
//					System.out.println(firstCart.getColumn() + "," + firstCart.getRow());
//					firstCart.setCollided(true);
//					secondCart.setCollided(true);
//					collidedCount += 2;
//					return;
//				}
//			}
//		}
    }

    private static List<Cart> getCarts(List<String> input, List<String> grid) {
        List<Cart> carts = new ArrayList<>();
        int lineCount = 0;
        for (String line : input) {
            while (line.indexOf("<") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("<"), LEFT));
                line = line.replaceFirst("<", "-");
            }
            while (line.indexOf("^") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("^"), UP));
                line = line.replaceFirst("\\^", "|");
            }
            while (line.indexOf(">") > -1) {
                carts.add(new Cart(lineCount, line.indexOf(">"), RIGHT));
                line = line.replaceFirst(">", "-");
            }
            while (line.indexOf("v") > -1) {
                carts.add(new Cart(lineCount, line.indexOf("v"), DOWN));
                line = line.replaceFirst("v", "|");
            }
            grid.add(line);
            lineCount++;
        }

        return carts;
    }

    public static class Cart {
        private int row;
        private int column;
        private int facing;
        private int turns;
        private boolean collided = false;

        public Cart(int row, int column, int facing) {
            this.row = row;
            this.column = column;
            this.facing = facing;
        }

        public void setCollided(boolean collided) {
            this.collided = collided;
        }

        public boolean isCollided() {
            return this.collided;
        }

        public void advance(List<String> grid) {
            if (this.facing == LEFT) {
                this.column--;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = UP;
                } else if (newMapLocation == '/') {
                    this.facing = DOWN;
                } else if (newMapLocation == '+') {
                    this.facing = turns(LEFT, turns);
                    turns++;
                }
            } else if (this.facing == UP) {
                this.row--;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = LEFT;
                } else if (newMapLocation == '/') {
                    this.facing = RIGHT;
                } else if (newMapLocation == '+') {
                    this.facing = turns(UP, turns);
                    turns++;
                }
            } else if (this.facing == RIGHT) {
                this.column++;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = DOWN;
                } else if (newMapLocation == '/') {
                    this.facing = UP;
                } else if (newMapLocation == '+') {
                    this.facing = turns(RIGHT, turns);
                    turns++;
                }
            } else if (this.facing == DOWN) {
                this.row++;
                char newMapLocation = grid.get(row).charAt(column);
                if (newMapLocation == '\\') {
                    this.facing = RIGHT;
                } else if (newMapLocation == '/') {
                    this.facing = LEFT;
                } else if (newMapLocation == '+') {
                    this.facing = turns(DOWN, turns);
                    turns++;
                }
            }

        }

        private int turns(int currentDirection, int turnsSoFar) {
            if ((currentDirection == LEFT && 0 == (turnsSoFar % 3))
                    || (currentDirection == DOWN && 1 == (turnsSoFar % 3))
                    || (currentDirection == RIGHT && 2 == (turnsSoFar % 3))) {
                return DOWN;
            } else if ((currentDirection == UP && 0 == (turnsSoFar % 3))
                    || (currentDirection == LEFT && 1 == (turnsSoFar % 3))
                    || (currentDirection == DOWN && 2 == (turnsSoFar % 3))) {
                return LEFT;
            } else if ((currentDirection == RIGHT && 0 == (turnsSoFar % 3))
                    || (currentDirection == UP && 1 == (turnsSoFar % 3))
                    || (currentDirection == LEFT && 2 == (turnsSoFar % 3))) {
                return UP;
            } else if ((currentDirection == DOWN && 0 == (turnsSoFar % 3))
                    || (currentDirection == RIGHT && 1 == (turnsSoFar % 3))
                    || (currentDirection == UP && 2 == (turnsSoFar % 3))) {
                return RIGHT;
            }
            throw new IllegalArgumentException();
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getFacing() {
            return facing;
        }

        public void setFacing(int facing) {
            this.facing = facing;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d13/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
