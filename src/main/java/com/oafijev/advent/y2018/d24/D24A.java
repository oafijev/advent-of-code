package com.oafijev.advent.y2018.d24;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class D24A {

    private static int lineNumber = 1;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        if (!input.get(0).equals("Immune System:")) {
            throw new IllegalArgumentException();
        }

        List<Group> immune = parseGroups(input, Team.IMMUNE);

        while (!input.get(lineNumber++).equals("Infection:")) { }

        List<Group> infection = parseGroups(input, Team.INFECTION);

        List<Group> allGroups = new ArrayList<>();
        allGroups.addAll(immune);
        allGroups.addAll(infection);

        while (bothSidesAlive(allGroups)) {
            // Select targets
            for (Group group : allGroups) {
                System.out.println(group.getName() + " contains " + group.getUnits() + " units");
            }

            Map<Group, Group> targets = new LinkedHashMap<>();
            List<Group> selectorsSorted = allGroups.stream().sorted(new TargetSelectionOrderComparator()).collect(Collectors.toList());
            for (Group selector : selectorsSorted) {
                Group selectedTarget = selectTarget(selector, allGroups.stream().filter(
                        a -> a.getTeam() != selector.getTeam() && !targets.containsValue(a)).collect(Collectors.toList()));
//                System.out.println("Group with " + selector.getUnits() + " units selecting");

                if (selectedTarget != null && calculateEffectiveDamage(selector, selectedTarget) > 0) {
                    targets.put(selector, selectedTarget);
                }
            }

            // Attack!
            for (Group attacker : targets.keySet().stream().sorted(Comparator.comparing(a -> -a.getInitiative())).collect(Collectors.toList())) {
                Group defender = targets.get(attacker);
                int damage = calculateEffectiveDamage(attacker, defender);
                int kills = damage / defender.getHp();
                if (kills > defender.getUnits()) {
                    kills = defender.getUnits();
                }
                System.out.println(attacker.getName() + " attacks " + defender.getName() + ", killing " + kills + " units!");
                defender.setUnits(defender.getUnits() - kills);
                if (defender.getUnits() == 0) {
                    System.out.println(defender.getName() + " was eliminated.");
                    allGroups.remove(defender);
                }
            }
        }

        int unitsRemaining = 0;
        for (Group group : allGroups) {
            unitsRemaining += group.getUnits();
        }
        System.out.println("Part A: " + unitsRemaining);
        System.out.println("Already guessed 23785, 19530");
        System.out.println("Winner: " + allGroups.get(0).getTeam());
    }

    private static Group selectTarget(Group selector, List<Group> candidates) {
        for (Group candidate : candidates) {
            System.out.println(selector.getName() + " would deal " + candidate.getName() + " " + calculateEffectiveDamage(selector, candidate) + " damage.");
        }
        return candidates.stream().sorted(new TargetSelectionComparator(selector)).findFirst().orElse(null);
    }

    private static int calculateEffectiveDamage(Group attacker, Group defender) {
        if (defender.getImmunities().contains(attacker.getDamageType())) {
            return 0;
        } else if (defender.getWeaknesses().contains(attacker.getDamageType())) {
            return attacker.getUnits() * attacker.getDamage() * 2;
        } else {
            return attacker.getUnits() * attacker.getDamage();
        }
    }

    private static boolean bothSidesAlive(List<Group> allGroups) {
        return allGroups.stream().anyMatch(a -> a.getTeam() == Team.IMMUNE) && allGroups.stream().anyMatch(a -> a.getTeam() == Team.INFECTION);
    }

    private static List<Group> parseGroups(List<String> input, Team team) {
        List<Group> groups = new ArrayList<>();
        int groupNumber = 1;
        while (lineNumber < input.size() && !input.get(lineNumber).trim().equals("")) {
            String line = input.get(lineNumber).trim();
            if (!line.contains("initiative")) {
                line = line + input.get(++lineNumber);
            }
            line = line.replace(" units each with ", "|").replace(" hit points (", "|")
                    .replace(") with an attack that does ", "|").replace(" damage at initiative ", "|");
            String[] params = line.split("\\|");
            Group group = parseGroup(params);
            group.setTeam(team);
            if (team == Team.IMMUNE) {
                group.setDamage(group.getDamage() + 79);
            }
            group.setName("" + team + groupNumber);
            groups.add(group);
            lineNumber++;
            groupNumber++;
        }
        return groups;
    }

    private static Group parseGroup(String[] params) {
        Group group = new Group();
        group.setUnits(Integer.parseInt(params[0]));
        group.setHp(Integer.parseInt(params[1]));
        group.setDamage(Integer.parseInt(params[3].split(" ")[0]));
        group.setDamageType(params[3].split(" ")[1]);
        group.setInitiative(Integer.parseInt(params[4]));
        addImmunitiesAndWeaknesses(group, params[2]);
        return group;
    }

    private static void addImmunitiesAndWeaknesses(Group group, String clauses) {
        for (String clause : clauses.split("; ")) {
            if (clause.startsWith("weak to")) {
                group.setWeaknesses(Arrays.asList(clause.replaceAll("weak to ", "").split(", ")));
            } else if (clause.startsWith("immune to")) {
                group.setImmunities(Arrays.asList(clause.replaceAll("immune to ", "").split(", ")));

            }
        }
    }

    public enum Team {
        INFECTION, IMMUNE;
    }

    public static class Group {
        private int units;
        private int hp;
        private int damage;
        private String damageType;
        private List<String> weaknesses = new ArrayList<>();
        private List<String> immunities = new ArrayList<>();
        private int initiative;
        private Team team;
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Team getTeam() {
            return team;
        }

        public void setTeam(Team team) {
            this.team = team;
        }

        public int getUnits() {
            return units;
        }

        public void setUnits(int units) {
            this.units = units;
        }

        public int getHp() {
            return hp;
        }

        public void setHp(int hp) {
            this.hp = hp;
        }

        public int getDamage() {
            return damage;
        }

        public void setDamage(int damage) {
            this.damage = damage;
        }

        public String getDamageType() {
            return damageType;
        }

        public void setDamageType(String damageType) {
            this.damageType = damageType;
        }

        public List<String> getWeaknesses() {
            return weaknesses;
        }

        public void setWeaknesses(List<String> weaknesses) {
            this.weaknesses = weaknesses;
        }

        public List<String> getImmunities() {
            return immunities;
        }

        public void setImmunities(List<String> immunities) {
            this.immunities = immunities;
        }

        public int getInitiative() {
            return initiative;
        }

        public void setInitiative(int initiative) {
            this.initiative = initiative;
        }
    }
    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d24/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }

    private static class TargetSelectionOrderComparator implements Comparator<Group> {
        @Override
        public int compare(Group g1, Group g2) {
            int effectivePower1 = g1.getUnits() * g1.getDamage();
            int effectivePower2 = g2.getUnits() * g2.getDamage();
            if (effectivePower1 > effectivePower2) {
                return -1;
            } else if (effectivePower1 < effectivePower2) {
                return 1;
            } else {
                return g2.getInitiative() - g1.getInitiative();
            }
        }
    }

    private static class TargetSelectionComparator implements Comparator<Group> {
        private Group attacker;

        public TargetSelectionComparator(Group attacker) {
            this.attacker = attacker;
        }

        @Override
        public int compare(Group g1, Group g2) {
            int adjustedEffectiveDamage1 = calculateEffectiveDamage(attacker, g1);
            int adjustedEffectiveDamage2 = calculateEffectiveDamage(attacker, g2);

            if (adjustedEffectiveDamage1 > adjustedEffectiveDamage2) {
                return -1;
            } else if (adjustedEffectiveDamage1 < adjustedEffectiveDamage2) {
                return 1;
            } else {
                int effectivePower1 = g1.getUnits() * g1.getDamage();
                int effectivePower2 = g2.getUnits() * g2.getDamage();
                if (effectivePower1 < effectivePower2) {
                    return 1;
                } else if (effectivePower1 > effectivePower2) {
                    return -1;
                } else {
                    return g2.getInitiative() - g1.getInitiative();
                }
            }
        }
    }
}
