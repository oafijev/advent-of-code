package com.oafijev.advent.y2018.d22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D22A {
    private static final int MOD = 20183;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int depth = Integer.parseInt(input.get(0).substring(7, input.get(0).length()));
        String[] targetParams = input.get(1).substring(8, input.get(1).length()).split(",");
        int rTarget = Integer.parseInt(targetParams[1]);
        int cTarget = Integer.parseInt(targetParams[0]);

        String[][] map = new String[rTarget + 1][cTarget + 1];
        int[][] geological = new int[rTarget + 1][cTarget + 1];

        for (int r = 0; r < map.length; r++) {
            for (int c = 0; c < map[r].length; c++) {
                if (c == 0 && r == 0) {
                    geological[r][c] = 0;
                } else if (c == cTarget && r == rTarget) {
                    geological[r][c] = 0;
                } else if (r == 0) {
                    geological[r][c] = (c * 16807) % MOD;
                } else if (c == 0) {
                    geological[r][c] = (r * 48271) % MOD;
                } else {
                    geological[r][c] = ((geological[r - 1][c] + depth) % MOD) * ((geological[r][c - 1] + depth) % MOD);
                    if (r == 1 && c == 1) {
                        System.out.println(geological[r][c]);
                    }
                }
            }
        }

        int risk = 0;
        int count = 0;
        for (int r = 0; r < geological.length; r++) {
            for (int c = 0; c < geological[r].length; c++) {
                int squareRisk = ((geological[r][c] + depth) % MOD) % 3;
                map[r][c] = squareRisk == 0 ? "." : (squareRisk == 1 ? "=" : "|");
                risk += squareRisk;
                count++;
            }
        }

        for (int r = 0; r < geological.length; r++) {
            for (int c = 0; c < geological[r].length; c++) {
                System.out.print(map[r][c]);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println(count);
        System.out.println(depth);
        System.out.println(rTarget);
        System.out.println(cTarget);
        System.out.println("Part A: " + risk);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d22/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
