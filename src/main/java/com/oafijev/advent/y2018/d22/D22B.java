package com.oafijev.advent.y2018.d22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class D22B {
    private static final int MOD = 20183;
    private static String[][][] cube;
    private static int rTarget;
    private static int cTarget;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int depth = Integer.parseInt(input.get(0).substring(7, input.get(0).length()));
        String[] targetParams = input.get(1).substring(8, input.get(1).length()).split(",");
        rTarget = Integer.parseInt(targetParams[1]);
        cTarget = Integer.parseInt(targetParams[0]);

        String[][] map = new String[rTarget + 50][cTarget + 50];
        int[][] geological = new int[map.length][map[0].length];

        for (int r = 0; r < geological.length; r++) {
            for (int c = 0; c < geological[r].length; c++) {
                if (c == 0 && r == 0) {
                    geological[r][c] = 0;
                } else if (c == cTarget && r == rTarget) {
                    geological[r][c] = 0;
                } else if (r == 0) {
                    geological[r][c] = (c * 16807) % MOD;
                } else if (c == 0) {
                    geological[r][c] = (r * 48271) % MOD;
                } else {
                    geological[r][c] = ((geological[r - 1][c] + depth) % MOD) * ((geological[r][c - 1] + depth) % MOD);
                    if (r == 1 && c == 1) {
                        System.out.println(geological[r][c]);
                    }
                }
            }
        }

        int risk = 0;
        cube = new String[3][map.length][map[0].length];
        for (int r = 0; r < geological.length; r++) {
            for (int c = 0; c < geological[r].length; c++) {
                int squareRisk = ((geological[r][c] + depth) % MOD) % 3;

                if (squareRisk == 0) {
                    // rocky
                    map[r][c] = ".";
                    cube[0][r][c] = "#";
                    cube[1][r][c] = ".";
                    cube[2][r][c] = ".";
                } else if (squareRisk == 1) {
                    // wet
                    map[r][c] = "=";
                    cube[0][r][c] = ".";
                    cube[1][r][c] = ".";
                    cube[2][r][c] = "#";
                } else if (squareRisk == 2) {
                    // narrow
                    map[r][c] = "|";
                    cube[0][r][c] = ".";
                    cube[1][r][c] = "#";
                    cube[2][r][c] = ".";
                }
                if (r <= rTarget && c <= cTarget) {
                    risk += squareRisk;
                }
            }
        }

        printMap(map);
        printMap(cube[0]);
        printMap(cube[1]);
        printMap(cube[2]);

        Integer[][][] distances = generateDistanceMatrixFromRoom();

        // map is the entire map
        // cube[0] is the map of what is reachable using neither tool (wet or narrow)
        // cube[1] is the map of gear (wet or rocky)
        // cube[2] is the map of torch (rocky or narrow)
        System.out.println(depth);
        System.out.println(rTarget);
        System.out.println(cTarget);
        System.out.println("Part A: " + risk);

        printTabbedMap(distances[0]);
        printTabbedMap(distances[1]);
        printTabbedMap(distances[2]);

        System.out.println("Part B: " + distances[2][rTarget][cTarget]);
    }

    private static Integer[][][] generateDistanceMatrixFromRoom() {
        Integer[][][] distances = new Integer[3][cube[0].length][cube[0][0].length];
        distances[2][0][0] = 0;
        ArrayDeque<Room> queue = new ArrayDeque<>();
        queue.addLast(new Room(2, 0, 0));

        while (!queue.isEmpty()) {
            Room room = queue.removeFirst();
            int r = room.getRow();
            int c = room.getColumn();
            int l = room.getLevel();
            if (c == cTarget && r == rTarget && l == 2) {
                System.out.println("got to target");
                //break;
            }

            if (l == 1 && r == 11 && c == 10) {
                System.out.println("got here");
            }

            // Current level - no switching equipment
            if (uncalculatedAndOpenOrShorter(distances, l, r - 1, c, room.getDistance() + 1)) {
                distances[l][r - 1][c] = room.getDistance() + 1;
                addSorted(queue, l, r - 1, c, room.getEquipped(), room.getDistance() + 1);
            }
            if (uncalculatedAndOpenOrShorter(distances, l, r + 1, c, room.getDistance() + 1)) {
                distances[l][r + 1][c] = room.getDistance() + 1;
                addSorted(queue, l, r + 1, c, room.getEquipped(), room.getDistance() + 1);
            }
            if (uncalculatedAndOpenOrShorter(distances, l, r, c - 1, room.getDistance() + 1)) {
                distances[l][r][c - 1] = room.getDistance() + 1;
                addSorted(queue, l, r, c - 1, room.getEquipped(), room.getDistance() + 1);
            }
            if (uncalculatedAndOpenOrShorter(distances, l, r, c + 1, room.getDistance() + 1)) {
                distances[l][r][c + 1] = room.getDistance() + 1;
                addSorted(queue, l, r, c + 1, room.getEquipped(), room.getDistance() + 1);
            }
            if (uncalculatedAndOpenOrShorter(distances, (l + 1) % 3, r, c, room.getDistance() + 7)) {
                distances[(l + 1) % 3][r][c] = room.getDistance() + 7;
                addSorted(queue, (l + 1) % 3, r, c, room.getEquipped(), room.getDistance() + 7);
            }
            if (uncalculatedAndOpenOrShorter(distances, (l + 2) % 3, r, c, room.getDistance() + 7)) {
                distances[(l + 2) % 3][r][c] = room.getDistance() + 7;
                addSorted(queue, (l + 2) % 3, r, c, room.getEquipped(), room.getDistance() + 7);
            }
//			printTabbedMap(distances[0]);
//			printTabbedMap(distances[1]);
//			printTabbedMap(distances[2]);
        }

        return distances;
    }

    private static void addSorted(ArrayDeque<Room> queue, int l, int r, int c, Equipped equipped, int distance) {
        queue.add(new Room(l, r, c, equipped, distance));
    }

    private static boolean uncalculatedAndOpenOrShorter(Integer[][][] distances, int l, int r, int c, int distance) {
        if (l < 0 || l > 2 || r < 0 || r >= cube[0].length || c < 0 || c >= cube[0][0].length) {
            return false;
        }
        return (distances[l][r][c] == null || distances[l][r][c] > distance) && ".".equals(cube[l][r][c]);
    }

    public enum Equipped {
        NEITHER, GEAR, TORCH;
    }

    public static class Room {
        private int level;
        private int row;
        private int column;
        private Equipped equipped = Equipped.NEITHER;
        private int distance;

        public Room(int level, int row, int column, Equipped equipped, int distance) {
            this(level, row, column);
            this.equipped = equipped;
            this.distance = distance;
        }

        public Room(int level, int row, int column) {
            this.level = level;
            this.row = row;
            this.column = column;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public Equipped getEquipped() {
            return equipped;
        }

        public void setEquipped(Equipped equipped) {
            this.equipped = equipped;
        }
    }

    private static void printMap(Object[][] map) {
        for (int r = 0; r < map.length; r++) {
            for (int c = 0; c < map[r].length; c++) {
                System.out.print(map[r][c]);
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void printTabbedMap(Object[][] map) {
        for (int r = 0; r < map.length; r++) {
            for (int c = 0; c < map[r].length; c++) {
                System.out.print(map[r][c] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d22/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
