package com.oafijev.advent.y2018.d06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D06A {
    private static final int SIZE = 400;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Integer[]> coords = new ArrayList<>();
        for (String line : input) {
            String[] params = line.split(", ");
            int x = Integer.valueOf(params[0]).intValue();
            int y = Integer.valueOf(params[1]).intValue();
            coords.add(new Integer[]{x, y});
        }

        int[][] grid = new int[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                int closestCoord = -1;
                int closestDistance = Integer.MAX_VALUE;
                for (int k = 0; k < coords.size(); k++) {
                    Integer[] coord = coords.get(k);
                    int newDistance = distance(i, j, coord[0], coord[1]);
                    if (newDistance == closestDistance) {
                        closestCoord = -1;
                    } else if (newDistance < closestDistance) {
                        closestDistance = newDistance;
                        closestCoord = k;
                    }
                }
                grid[i][j] = closestCoord;
                System.out.print("" + ((char) (closestCoord + 'A')));
            }
            System.out.println();
        }

        Set<Integer> coordsOnEdge = new HashSet<>();
        for (int i = 0; i < SIZE; i++) {
            coordsOnEdge.add(grid[i][0]);
            coordsOnEdge.add(grid[i][SIZE - 1]);
            coordsOnEdge.add(grid[0][i]);
            coordsOnEdge.add(grid[SIZE - 1][i]);
        }
        System.out.println(coordsOnEdge.size());

        Map<Integer, Integer> coordsToInnerAreas = new HashMap<>();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                int marker = grid[i][j];
                if (!coordsOnEdge.contains(marker)) {
                    if (!coordsToInnerAreas.containsKey(marker)) {
                        coordsToInnerAreas.put(marker, 0);
                    } else {
                        coordsToInnerAreas.put(marker, coordsToInnerAreas.get(marker) + 1);
                    }
                }
            }
        }

        int largestArea = 0;
        for (Integer marker : coordsToInnerAreas.keySet()) {
            Integer area = coordsToInnerAreas.get(marker);
            System.out.println("marker: " + marker + ", size: " + area);
            if (area > largestArea) {
                largestArea = area;
            }
        }
        System.out.println(largestArea + 1);
    }

    private static int distance(int i, int j, Integer x, Integer y) {
        return Math.abs(i - x) + Math.abs(j - y);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d06/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
