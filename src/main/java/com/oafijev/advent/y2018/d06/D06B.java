package com.oafijev.advent.y2018.d06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D06B {
    private static final int SIZE = 400;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Integer[]> coords = new ArrayList<>();
        for (String line : input) {
            String[] params = line.split(", ");
            int x = Integer.valueOf(params[0]).intValue();
            int y = Integer.valueOf(params[1]).intValue();
            coords.add(new Integer[]{x, y});
        }

        int[][] grid = new int[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                int distance = 0;
                for (int k = 0; k < coords.size(); k++) {
                    Integer[] coord = coords.get(k);
                    distance += distance(i, j, coord[0], coord[1]);
                }
                if (distance < 10000) {
                    grid[i][j] = 1;
                } else {
                    grid[i][j] = 0;
                }
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }

        int area = 0;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                area += grid[i][j];
            }
        }
        System.out.println(area);
    }

    private static int distance(int i, int j, Integer x, Integer y) {
        return Math.abs(i - x) + Math.abs(j - y);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d06/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
