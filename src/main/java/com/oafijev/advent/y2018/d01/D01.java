package com.oafijev.advent.y2018.d01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class D01 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Long result = 0l;
        Set<Long> reached = new HashSet<>();
        boolean completed = false;
        boolean firstRun = true;
        Long firstRunResult = 0l;

        while (!completed) {
            for (String line : input) {
                System.out.println("r: " + result);
                Long value = new Long(line);
                result += value;
                if (reached.contains(result)) {
                    System.out.println(result);
                    completed = true;
                    break;
                } else {
                    reached.add(result);
                }
            }
            if (firstRun) {
                firstRunResult = result;
                firstRun = false;
            }
        }
        System.out.println("first star result: " + firstRunResult);
        System.out.println(input);
        System.out.println("second star result: " + result);
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d01/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
