package com.oafijev.advent.y2018.d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class D12A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;
    private static final int GENERATIONS = 20;
    private static int SIZE = 0;
    private static int ZERO_INDEX = 0;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        String[] initialStateArray = input.get(0).split(" ")[2].split("");
        List<Boolean> stateList = new ArrayList<>();

        for (int i = 0; i < initialStateArray.length; i++) {
            stateList.add(initialStateArray[i].equals("#") ? true : false);
        }

        SIZE = (stateList.size() + GENERATIONS) * 2;
        ZERO_INDEX = SIZE / 2;
        Boolean[] state = new Boolean[SIZE];
        for (int i = 0; i < stateList.size(); i++) {
            state[ZERO_INDEX + i] = stateList.get(i);
        }

        List<String> rules = new ArrayList<>();
        for (int i = 2; i < input.size(); i++) {
            if (input.get(i).split(" ")[2].equals("#")) {
                rules.add(input.get(i).split(" ")[0]);
            }
        }
        System.out.println(String.join("", Arrays.stream(state).map(a -> getChar(a)).collect(Collectors.toList())));


        for (int i = 0; i < GENERATIONS; i++) {
            Boolean[] newState = new Boolean[SIZE];
            for (int j = 2; j < SIZE - 2; j++) {
                String ruleMatch = getChar(state[j - 2]) + getChar(state[j - 1]) + getChar(state[j]) + getChar(state[j + 1]) + getChar(state[j + 2]);
                for (String rule : rules) {
                    if (ruleMatch.equals(rule)) {
                        newState[j] = true;
                        break;
                    } else {
                        newState[j] = false;
                    }
                }
            }
            state = newState;
            System.out.println(String.join("", Arrays.stream(state).map(a -> getChar(a)).collect(Collectors.toList())));
        }

        int total = 0;
        for (int i = 0; i < SIZE; i++) {
            total += (state[i] != null && state[i]) ? (i - ZERO_INDEX) : 0;
        }
        System.out.println(total);
    }

    private static String getChar(Boolean potted) {
        return (potted != null && potted) ? "#" : ".";
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d12/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
