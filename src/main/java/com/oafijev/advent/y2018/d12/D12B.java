package com.oafijev.advent.y2018.d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D12B {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;
    private static final long GENERATIONS = 50000000000L;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        String state = input.get(0).split(" ")[2];
        int zeroIndex = 0;

        List<String> rules = new ArrayList<>();
        for (int i = 2; i < input.size(); i++) {
            if (input.get(i).split(" ")[2].equals("#")) {
                rules.add(input.get(i).split(" ")[0]);
            }
        }

        System.out.println(state);

        for (int i = 0; i < GENERATIONS; i++) {
            if (i % 100000 == 0) {
                System.out.println("[" + i + "] [" + zeroIndex + "] " + state);

                int total = 0;
                for (int j = 0; j < state.length(); j++) {
                    total += (state.charAt(j) == '#') ? (j - zeroIndex) : 0;
                }
                System.out.println(total);
            }

            String oldState = "...." + state + "....";
            String newState = "";
            for (int j = 2; j < oldState.length() - 2; j++) {
                String ruleMatch = oldState.substring(j - 2, j + 3);
                String newChar = ".";
                for (String rule : rules) {
                    if (ruleMatch.equals(rule)) {
                        newChar = "#";
                        break;
                    }
                }
                newState += newChar;
            }
            zeroIndex += 2;
            while (newState.startsWith(".")) {
                zeroIndex--;
                newState = newState.substring(1);
            }
            while (newState.endsWith(".")) {
                newState = newState.substring(0, newState.length() - 1);
            }

            state = newState;


        }

        System.out.println("last");
        System.out.println("[--] [" + zeroIndex + "] " + state);
        int total = 0;
        for (int j = 0; j < state.length(); j++) {
            total += (state.charAt(j) == '#') ? (j - zeroIndex) : 0;
        }
        System.out.println(total);


    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d12/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
