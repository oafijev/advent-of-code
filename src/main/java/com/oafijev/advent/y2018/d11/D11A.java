package com.oafijev.advent.y2018.d11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D11A {
    private static final int SIZE = 300;

    public static void main(String[] args) throws Exception {
        int serialNumber = 3463;
        int[][] grid = new int[SIZE][SIZE];

        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                grid[r][c] = getPower(serialNumber, r, c);
//				System.out.print(grid[r][c] + "\t");
            }
//			System.out.println();
        }

        int bestRow = 0;
        int bestColumn = 0;
        int bestPower = Integer.MIN_VALUE;
        int bestSubSize = 0;

        for (int subSize = 1; subSize <= SIZE; subSize++) {
            System.out.println("beginning subsize " + subSize);
            for (int r = 0; r < SIZE - subSize + 1; r++) {
                for (int c = 0; c < SIZE - subSize + 1; c++) {
                    int power = 0;

                    for (int subR = 0; subR < subSize; subR++) {
                        for (int subC = 0; subC < subSize; subC++) {
                            power += grid[r + subR][c + subC];
                        }
                    }

                    if (power > bestPower) {
                        bestPower = power;
                        bestRow = r;
                        bestColumn = c;
                        bestSubSize = subSize;
                        System.out.println("found new best solution [" + bestColumn + "," + bestRow + "," + bestSubSize + "]");
                    }
                }
            }
        }
        System.out.println("[" + bestColumn + "," + bestRow + "," + bestSubSize + "]");
    }

    private static int getPower(int serialNumber, int r, int c) {
        int rackId = c + 10;
        int power = (((rackId * r + serialNumber) * rackId / 100) % 10) - 5;
        return power;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d11/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
