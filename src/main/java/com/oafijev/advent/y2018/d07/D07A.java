package com.oafijev.advent.y2018.d07;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class D07A {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Map<String, Node> nodes = new HashMap<>();

        for (String line : input) {
            String[] words = line.split(" ");
            String blocked = words[7];
            String blocker = words[1];

            if (!nodes.containsKey(blocked)) {
                nodes.put(blocked, new Node(blocked));
            }
            if (!nodes.containsKey(blocker)) {
                nodes.put(blocker, new Node(blocker));
            }

            nodes.get(blocker).blocks(nodes.get(blocked));
            nodes.get(blocked).blockedBy(nodes.get(blocker));
        }

        List<Node> namesWithNoBlockers = nodes.values().stream().filter(a -> a.getBlockedBy().isEmpty()).collect(Collectors.toList());
        assert (namesWithNoBlockers.size() == 1);
        String root = namesWithNoBlockers.get(0).getName();

        Set<String> unprocessedNames = nodes.values().stream().map(Node::getName).collect(Collectors.toSet());
        List<String> availableNames = new ArrayList<>();
        List<String> processedNames = new ArrayList<>();

        availableNames.add(root);
        unprocessedNames.remove(root);

        do {
            Collections.sort(availableNames);
            String next = availableNames.get(0);
            System.out.println(next);
            processedNames.add(next);
            availableNames.remove(next);

            List<String> nowAvailableNames = getNowAvailableNames(nodes, processedNames, availableNames, unprocessedNames);

            availableNames.addAll(nowAvailableNames);
            unprocessedNames.removeAll(nowAvailableNames);
        } while (!unprocessedNames.isEmpty() || !availableNames.isEmpty());

        System.out.println(String.join("", processedNames));
    }

    private static List<String> getNowAvailableNames(Map<String, Node> nodes, List<String> processedNames, List<String> availableNames,
                                                     Set<String> unprocessedNames) {
        List<String> result = new ArrayList<>();
        for (String name : unprocessedNames) {
            if (nodes.get(name).getBlockedBy().stream().allMatch(a -> processedNames.contains(a))) {
                result.add(name);
            }
        }
        return result;
    }

    public static String getIsBlockedBy(Map<String, Node> nodes, String name) {
        StringBuffer sb = new StringBuffer();
        for (String blocker : nodes.get(name).getBlockedBy()) {
            sb.append(blocker + "-");
            sb.append(getIsBlockedBy(nodes, blocker));
        }
        return sb.toString();
    }

    public static class Node {
        String name;
        List<String> blocks = new ArrayList<>();
        List<String> blockedBy = new ArrayList<>();

        public Node(String name) {
            this.name = name;
        }

        public List<String> getBlockedBy() {
            return this.blockedBy;
        }

        public List<String> getBlocks() {
            return blocks;
        }

        public void blocks(Node blocked) {
            blocks.add(blocked.getName());
        }

        private String getName() {
            return this.name;
        }

        public void blockedBy(Node blocker) {
            blockedBy.add(blocker.getName());
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d07/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
