package com.oafijev.advent.y2018.d19;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D19A {

    public static int ip;
    public static int ipBound;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Cpu cpu = new Cpu();
        List<Instruction> instructions = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            String line = input.get(i);
            if (line.startsWith("#ip")) {
                ipBound = Integer.parseInt(line.substring(4));
                System.out.println("binding ip to " + ipBound);
                continue;
            }

            String[] params = line.split(" ");
            Instruction instruction = new Instruction(params[0], Integer.parseInt(params[1]), Integer.parseInt(params[2]), Integer.parseInt(params[3]));
            instructions.add(instruction);
        }

//		int[] registers = new int[6];
        int[] registers = new int[]{
//				1, 2, 2, 3, 947, 1
                1, 2, 2, 3, 35, 1
        };
        ip = 4;
        long count = 0;
        while (ip >= 0 && ip < instructions.size()) {
            if (count % 10000 == 0) {
                System.out.println("executing instruction number " + count);
            }
            Instruction current = instructions.get(ip);
            if (ip == 4) {
                printStatus(registers, current);
            }
            registers[ipBound] = ip;
            cpu.apply(current, registers);
            ip = registers[ipBound];
            ip++;
            count++;
        }

        System.out.println("Part A: " + registers[0]);
    }

    private static void printStatus(int[] registers, Instruction instruction) {
        System.out.println("ip=" + ip + " " + instruction + " [" + formatRegisters(registers) + "]");
    }

    private static String formatRegisters(int[] registers) {
        StringBuilder sb = new StringBuilder();
        for (int register : registers) {
            sb.append(register).append(", ");
        }
        return sb.toString();
    }

    public static class Instruction {
        private String opcode;
        private int a;
        private int b;
        private int c;

        public Instruction(String opcode, int a, int b, int c) {
            this.opcode = opcode;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public String getOpcode() {
            return opcode;
        }

        public void setOpcode(String opcode) {
            this.opcode = opcode;
        }

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }

        public int getC() {
            return c;
        }

        public void setC(int c) {
            this.c = c;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(opcode).append(" ");
            sb.append(a).append(" ");
            sb.append(b).append(" ");
            sb.append(c);
            return sb.toString();
        }

    }

    public static class Cpu {
        public void apply(Instruction instruction, int[] registers) {
            String opcode = instruction.getOpcode();
            int a = instruction.getA();
            int b = instruction.getB();
            int c = instruction.getC();
            switch (opcode) {
                case "addr":
                    registers[c] = registers[a] + registers[b];
                    break;
                case "addi":
                    registers[c] = registers[a] + b;
                    break;
                case "mulr":
                    registers[c] = registers[a] * registers[b];
                    break;
                case "muli":
                    registers[c] = registers[a] * b;
                    break;
                case "banr":
                    registers[c] = registers[a] & registers[b];
                    break;
                case "bani":
                    registers[c] = registers[a] & b;
                    break;
                case "borr":
                    registers[c] = registers[a] | registers[b];
                    break;
                case "bori":
                    registers[c] = registers[a] | b;
                    break;
                case "setr":
                    registers[c] = registers[a];
                    break;
                case "seti":
                    registers[c] = a;
                    break;
                case "gtir":
                    registers[c] = ((a > registers[b]) ? 1 : 0);
                    break;
                case "gtri":
                    registers[c] = ((registers[a] > b) ? 1 : 0);
                    break;
                case "gtrr":
                    registers[c] = ((registers[a] > registers[b]) ? 1 : 0);
                    break;
                case "eqir":
                    registers[c] = ((a == registers[b]) ? 1 : 0);
                    break;
                case "eqri":
                    registers[c] = ((registers[a] == b) ? 1 : 0);
                    break;
                case "eqrr":
                    registers[c] = ((registers[a] == registers[b]) ? 1 : 0);
                    break;
                default:
                    throw new IllegalArgumentException(opcode);

            }
        }
    }

    private static List<String> getOpcodes() {
        return Arrays.asList(
                "addr", "addi",
                "mulr", "muli",
                "banr", "bani",
                "borr", "bori",
                "setr", "seti",
                "gtir", "gtri", "gtrr",
                "eqir", "eqri", "eqrr");
    }

    private static int[] mapToInt(String[] string) {
        int[] result = new int[string.length];
        for (int i = 0; i < string.length; i++) {
            result[i] = Integer.parseInt(string[i]);
        }
        return result;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d19/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
