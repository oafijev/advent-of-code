package com.oafijev.advent.y2018.d02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class D02A2 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int threes = 0;
        int twos = 0;

        for (String line : input) {
            String[] characters = line.split("");
            Map<String, Long> counts = Arrays.asList(characters).stream()
                    .collect(Collectors.groupingBy(String::toString, Collectors.counting()));
            twos += counts.values().stream().anyMatch(value -> value == 2) ? 1 : 0;
            threes += counts.values().stream().anyMatch(value -> value == 3) ? 1 : 0;
        }

        System.out.println(twos * threes);

        // Bonus - one-liner solution to interview question
//		List<String> words = Arrays.asList("a", "b", "c", "a");
//		System.out.println(words.stream().collect(Collectors.groupingBy(a -> a, LinkedHashMap::new, Collectors.counting()))
//				.entrySet().stream().filter(a -> Long.valueOf(1L).equals(a.getValue())).findFirst().get().getKey());
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d02/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
