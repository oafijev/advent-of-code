package com.oafijev.advent.y2018.d02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class D02A {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int threes = 0;
        int twos = 0;

        for (String line : input) {
            String[] characters = line.split("");
            Set<String> appearsOnce = new HashSet<>();
            Set<String> appearsTwice = new HashSet<>();
            Set<String> appearsThrice = new HashSet<>();
            Set<String> done = new HashSet<>();
            for (String character : characters) {
                if (done.contains(character)) {
                    continue;
                } else if (appearsThrice.contains(character)) {
                    done.add(character);
                    appearsThrice.remove(character);
                } else if (appearsTwice.contains(character)) {
                    appearsThrice.add(character);
                    appearsTwice.remove(character);
                } else if (appearsOnce.contains(character)) {
                    appearsTwice.add(character);
                    appearsOnce.remove(character);
                } else {
                    appearsOnce.add(character);
                }
            }

            if (!appearsTwice.isEmpty()) {
                twos++;
            }
            if (!appearsThrice.isEmpty()) {
                threes++;
            }
            System.out.println("2: " + !appearsTwice.isEmpty());
            System.out.println("3: " + !appearsThrice.isEmpty());
        }

        System.out.println(twos * threes);
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d02/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
