package com.oafijev.advent.y2018.d02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D02B {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int length = input.size();
        List<String[]> historical = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            String[] characters = input.get(i).split("");

            for (String[] historicalLine : historical) {
//				System.out.println("comparing " + String.join("", historicalLine));
//				System.out.println("  ------- " + String.join("", characters));
                boolean oneDiffers = false;
                boolean gotToEnd = true;
                for (int k = 0; k < characters.length; k++) {
                    if (!characters[k].equals(historicalLine[k])) {
                        if (!oneDiffers) {
                            oneDiffers = true;
                        } else {
                            gotToEnd = false;
                            break;
                        }
                    }
                }

                if (gotToEnd) {
                    System.out.println(String.join("", historicalLine));
                    System.out.println(String.join("", characters));
                }
            }

            historical.add(characters);
        }

    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d02/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
