package com.oafijev.advent.y2018.d03;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D03A {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int[][] matrix = new int[1000][1000];

        for (String line : input) {

            String[] params = line.split(" ");
            String number = params[0].substring(1);
            String[] coords = params[2].split(",");
            int x = Integer.valueOf(coords[0]).intValue();
            int y = Integer.valueOf(coords[1].substring(0, coords[1].length() - 1)).intValue();
            String[] sizes = params[3].split("x");
            int xsize = Integer.valueOf(sizes[0]).intValue();
            int ysize = Integer.valueOf(sizes[1]).intValue();

            for (int i = x; i < x + xsize; i++) {
                for (int j = y; j < y + ysize; j++) {
                    if (matrix[i][j] == 0) {
                        matrix[i][j] = 1;
                    } else if (matrix[i][j] == 1) {
                        matrix[i][j] = 2;
                    }
                }
            }
        }

        int count = 0;
        for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < 1000; j++) {
                count += (matrix[i][j] == 2) ? 1 : 0;
            }
        }

        System.out.println(count);
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d03/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
