package com.oafijev.advent.y2018.d25;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class D25A {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Point> points = new ArrayList<>();

        int newConstellationNumber = 0;

        for (String line : input) {
            String[] params = line.trim().split(",");

            int x = Integer.parseInt(params[0]);
            int y = Integer.parseInt(params[1]);
            int z = Integer.parseInt(params[2]);
            int t = Integer.parseInt(params[3]);

            Point newPoint = new Point(x, y, z, t, newConstellationNumber++);

            points.add(newPoint);
        }

        boolean changes = true;
        while (changes) {
            changes = false;
            for (Point point : points) {
                for (Point otherPoint : points.stream().filter(a -> a.getC() != point.getC()).collect(Collectors.toList())) {
                    if (distance(point, otherPoint) <= 3) {
                        // move this point and all points in its constellation to other constellation
                        for (Point pointInThisConstellation : points.stream().filter(a -> a.getC() == point.getC()).collect(Collectors.toList())) {
                            pointInThisConstellation.setC(otherPoint.getC());
                        }
                        changes = true;
                        break;
                    }
                }
            }
        }

        System.out.println("Part A: " + points.stream().map(Point::getC).collect(Collectors.toSet()).size());
    }

    public static int distance(Point a, Point b) {
        return Math.abs(a.getX() - b.getX()) + Math.abs(a.getY() - b.getY()) + Math.abs(a.getZ() - b.getZ()) + Math.abs(a.getT() - b.getT());
    }

    public static class Point {
        private int x;
        private int y;
        private int z;
        private int t;
        private int c;

        public Point(int x, int y, int z, int t, int c) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.t = t;
            this.c = c;
        }

        @Override
        public String toString() {
            return "[" + x + "," + y + "," + z + "," + t + "], constellation: " + c;
        }

        public int getC() {
            return c;
        }

        public void setC(int c) {
            this.c = c;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public int getT() {
            return t;
        }

        public void setT(int t) {
            this.t = t;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d25/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
