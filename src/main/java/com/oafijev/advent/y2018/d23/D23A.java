package com.oafijev.advent.y2018.d23;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D23A {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Bot> bots = new ArrayList<>();

        for (String line : input) {
            String[] lineParams = line.replaceAll("pos=<", "").split(">, r=");
            String[] stringCoords = lineParams[0].split(",");
            int r = Integer.parseInt(lineParams[1]);
            int x = Integer.parseInt(stringCoords[0]);
            int y = Integer.parseInt(stringCoords[1]);
            int z = Integer.parseInt(stringCoords[2]);

            bots.add(new Bot(x, y, z, r));
        }

        Bot strongestBot = null;
        int highestStrength = Integer.MIN_VALUE;

        for (Bot bot : bots) {
            if (bot.getR() > highestStrength) {
                strongestBot = bot;
                highestStrength = bot.getR();
            }
        }

        int inRange = 0;
        for (Bot bot : bots) {
            if (distance(bot, strongestBot) <= strongestBot.getR()) {
                inRange++;
            }
        }

        System.out.println("Part A bot: " + strongestBot.getX() + "," + strongestBot.getY() + "," + strongestBot.getZ());
        System.out.println("Part A: " + inRange);

        int maxx = bots.stream().max(Comparator.comparing(Bot::getX)).get().getX();
        int minx = bots.stream().min(Comparator.comparing(Bot::getX)).get().getX();
        int maxy = bots.stream().max(Comparator.comparing(Bot::getY)).get().getY();
        int miny = bots.stream().min(Comparator.comparing(Bot::getY)).get().getY();
        int maxz = bots.stream().max(Comparator.comparing(Bot::getZ)).get().getZ();
        int minz = bots.stream().min(Comparator.comparing(Bot::getZ)).get().getZ();

        int skip = 1000000;
        int bestX = Integer.MAX_VALUE;
        int bestY = Integer.MAX_VALUE;
        int bestZ = Integer.MAX_VALUE;
        Set<Integer> bestDistanceFromOrigin = new HashSet<>();
        while (skip >= 1) {
            System.out.println("=== Beginning iteration with skip = " + skip + "   =====");
            bestX = Integer.MAX_VALUE;
            bestY = Integer.MAX_VALUE;
            bestZ = Integer.MAX_VALUE;
            inRange = 0;
            bestDistanceFromOrigin.clear();
            for (int i = minx; i < maxx; i = i + skip) {
                for (int j = miny; j < maxy; j = j + skip) {
                    for (int k = minz; k < maxz; k = k + skip) {
                        int currentInRange = getInRange(bots, new Coord(i, j, k));
                        if (currentInRange == inRange) {
                            bestDistanceFromOrigin.add(Math.abs(i) + Math.abs(j) + Math.abs(k));
//                            System.out.println("Found same solution (inRange, coord) = (" + currentInRange + ", " + i + "," + j + "," + k + ") distance = " + (Math.abs(i) + Math.abs(j) + Math.abs(k)));
                        } else if (currentInRange > inRange) {
//                            System.out.println("Found new solution (inRange, coord) = (" + currentInRange + ", " + i + "," + j + "," + k + ") distance = " + (Math.abs(i) + Math.abs(j) + Math.abs(k)));
                            bestDistanceFromOrigin.clear();
                            bestDistanceFromOrigin.add(Math.abs(i) + Math.abs(j) + Math.abs(k));
                            inRange = currentInRange;
                            bestX = i;
                            bestY = j;
                            bestZ = k;
                        }
                    }
                }
            }
            maxx = bestX + skip;
            minx = bestX - skip;
            maxy = bestY + skip;
            miny = bestY - skip;
            maxz = bestZ + skip;
            minz = bestZ - skip;
            skip = skip / 10;
        }

        System.out.println("Part B - least distance from origin: " + bestDistanceFromOrigin.stream().min(Comparator.comparing(Integer::valueOf)).get().intValue());
    }

    private static int getInRange(List<Bot> bots, Coord coord) {
        int count = 0;
        for (Bot bot : bots) {
            if (distance(bot, coord) <= bot.getR()) {
                count++;
            }
        }
        return count;
    }

    private static void addBotToMap(Map<Coord, Integer> coordinateToNumber, Bot bot) {
        int range = bot.getR();

        for (int i = 0; i < range; i++) {
            for (int j = -i; j <= i; j++) {
                for (int k = -i; k <= i; k++) {
                    incrementCoordinate(coordinateToNumber, bot.getX() + range - i, bot.getY() + j, bot.getZ() + k);
                    if (i <= 0) {
                        incrementCoordinate(coordinateToNumber, bot.getX() - range + i, bot.getY() + j, bot.getZ() + k);
                    }
                }
            }
        }
    }

    private static void incrementCoordinate(Map<Coord, Integer> map, int x, int y, int z) {
        Coord coord = new Coord(x, y, z);
        if (!map.containsKey(coord)) {
            map.put(coord, 0);
        }
        map.put(coord, map.get(coord) + 1);
    }

    private static int distance(Coord bot, Coord otherBot) {
        return Math.abs(bot.getX() - otherBot.getX()) + Math.abs(bot.getY() - otherBot.getY()) + Math.abs(bot.getZ() - otherBot.getZ());
    }

    public static class Coord {
        private int x;
        private int y;
        private int z;

        public Coord(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else {
                Coord o = (Coord) obj;
                return o.x == this.x && o.y == this.y && o.z == this.z;
            }
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.x, this.y, this.z);
        }
    }

    public static class Bot extends Coord {
        private int r;

        public Bot(int x, int y, int z, int r) {
            super(x, y, z);
            this.r = r;
        }

        public int getR() {
            return r;
        }

        public void setR(int r) {
            this.r = r;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d23/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
