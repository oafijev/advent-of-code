package com.oafijev.advent.y2018.d05;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D05A {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        String line = input.get(0);
        System.out.println(line);

        line = react(line);

        System.out.println(line.length());
    }

    private static String react(String line) {
        boolean removed = false;

        do {
            removed = false;
            for (int i = 0; i < line.length() - 1; i++) {
                int character = (int) line.substring(i, i + 1).charAt(0);
                int nextCharacter = (int) line.substring(i + 1, i + 2).charAt(0);

//				System.out.println(character);
                if (Math.abs(character - nextCharacter) == 32) {
                    removed = true;
                    line = line.substring(0, i) + line.substring(i + 2, line.length());
                }
            }
        } while (removed == true);
        return line;
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d05/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
