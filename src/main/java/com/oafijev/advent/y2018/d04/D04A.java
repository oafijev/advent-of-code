package com.oafijev.advent.y2018.d04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D04A {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Collections.sort(input);

        Map<Integer, Integer> guardToTotalSleep = new HashMap<>();
        int currentGuard = 0;
        int fellAsleep = -1;
        for (String line : input) {
            System.out.println(line);
            String[] words = line.split(" ");
            int timestampMinute = Integer.valueOf(words[1].substring(3, 5)).intValue();
            if (words.length >= 5 && words[4].equals("begins")) {
                if (fellAsleep != -1) {
                    addSleep(guardToTotalSleep, currentGuard, fellAsleep, timestampMinute);
                }
                currentGuard = Integer.valueOf(words[3].substring(1)).intValue();
            } else if (words[2].equals("falls")) {
                fellAsleep = timestampMinute;
            } else if (words[2].equals("wakes")) {
                addSleep(guardToTotalSleep, currentGuard, fellAsleep, timestampMinute);
                fellAsleep = -1;
            }
        }

        System.out.println(guardToTotalSleep.size());
        int mostSleep = 0;
        int mostAsleep = -1;
        for (Integer guard : guardToTotalSleep.keySet()) {
            Integer sleep = guardToTotalSleep.get(guard);
            if (sleep > mostSleep) {
                mostAsleep = guard;
                mostSleep = sleep;
            }
            System.out.println("Guard #" + guard + " slept for " + sleep + " minutes");
        }
        System.out.println("Most sleep: Guard #" + mostAsleep + " slept for " + mostSleep + " minutes");

        int[] minutes = new int[60];
        for (String line : input) {
            String[] words = line.split(" ");
            int timestampMinute = Integer.valueOf(words[1].substring(3, 5)).intValue();
            if (words.length >= 5 && words[4].equals("begins")) {
                if (fellAsleep != -1) {
                    addMinutes(minutes, fellAsleep, timestampMinute);
                }
                currentGuard = Integer.valueOf(words[3].substring(1)).intValue();
            } else if (words[2].equals("falls")) {
                if (currentGuard != mostAsleep) {
                    continue;
                }
                fellAsleep = timestampMinute;
            } else if (words[2].equals("wakes")) {
                if (currentGuard != mostAsleep) {
                    continue;
                }
                addMinutes(minutes, fellAsleep, timestampMinute);
                fellAsleep = -1;
            }
        }

        int mostSleptMinute = -1;
        int numberOfSleeps = 0;
        for (int i = 0; i <= 59; i++) {
            if (numberOfSleeps < minutes[i]) {
                mostSleptMinute = i;
                numberOfSleeps = minutes[i];
            }
        }
        System.out.println(mostSleptMinute * mostAsleep);
    }

    private static void addMinutes(int[] minutes, int fellAsleep, int timestampMinute) {
        for (int i = fellAsleep; i < timestampMinute; i++) {
            minutes[i]++;
        }
    }


    private static void addSleep(Map<Integer, Integer> guardToTotalSleep, int currentGuard, int fellAsleep, int timestampMinute) {
        if (!guardToTotalSleep.containsKey(currentGuard)) {
            guardToTotalSleep.put(currentGuard, 0);
        }
        guardToTotalSleep.put(currentGuard, guardToTotalSleep.get(currentGuard) + timestampMinute - fellAsleep);
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d04/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
