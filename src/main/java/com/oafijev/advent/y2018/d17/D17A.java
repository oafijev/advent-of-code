package com.oafijev.advent.y2018.d17;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D17A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;
    private static int maxcol = Integer.MIN_VALUE;
    private static int mincol = Integer.MAX_VALUE;
    private static int maxrow = Integer.MIN_VALUE;
    private static int minrow = Integer.MAX_VALUE;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();


        for (String line : input) {
            String[] params = line.split(", ");
            if (params[0].charAt(0) == 'x') {
                int x = Integer.parseInt(params[0].substring(2));
                String[] yrange = params[1].substring(2).split("\\.\\.");
                int ystart = Integer.parseInt(yrange[0]);
                int yend = Integer.parseInt(yrange[1]);
                maxcol = x > maxcol ? x : maxcol;
                mincol = x < mincol ? x : mincol;
                maxrow = yend > maxrow ? yend : maxrow;
                minrow = ystart < minrow ? ystart : minrow;
            } else if (params[0].charAt(0) == 'y') {
                int y = Integer.parseInt(params[0].substring(2));
                String[] xrange = params[1].substring(2).split("\\.\\.");
                int xstart = Integer.parseInt(xrange[0]);
                int xend = Integer.parseInt(xrange[1]);
                maxcol = xend > maxcol ? xend : maxcol;
                mincol = xstart < mincol ? xstart : mincol;
                maxrow = y > maxrow ? y : maxrow;
                minrow = y < minrow ? y : minrow;
            } else {
                throw new IllegalArgumentException();
            }
        }
        System.out.println(mincol);
        System.out.println(maxcol);
        System.out.println(minrow);
        System.out.println(maxrow);

        String[][] map = new String[maxrow - minrow + 1][maxcol - mincol + 3];

        System.out.println(map.length);
        System.out.println(map[0].length);

        for (String line : input) {
            String[] params = line.split(", ");
            if (params[0].charAt(0) == 'x') {
                int x = Integer.parseInt(params[0].substring(2));
                String[] yrange = params[1].substring(2).split("\\.\\.");
                int ystart = Integer.parseInt(yrange[0]);
                int yend = Integer.parseInt(yrange[1]);

                for (int i = ystart; i <= yend; i++) {
                    map[i - minrow][x - mincol + 1] = "#";
                }
            } else if (params[0].charAt(0) == 'y') {
                int y = Integer.parseInt(params[0].substring(2));
                String[] xrange = params[1].substring(2).split("\\.\\.");
                int xstart = Integer.parseInt(xrange[0]);
                int xend = Integer.parseInt(xrange[1]);

                for (int i = xstart; i <= xend; i++) {
                    map[y - minrow][i - mincol + 1] = "#";
                }
            } else {
                throw new IllegalArgumentException();
            }
        }

        // drop water at column 500
        while (dropWater(map, 0, 500 - mincol + 1)) {
            printMap(map);
        }

        printMap(map);
        System.out.println(countWater(map));
    }

    private static int countWater(String[][] map) {
        int water = 0;
        for (String[] line : map) {
            for (String square : line) {
                if ("~".equals(square) || "|".equals(square)) {
                    water++;
                }
            }
        }

        return water;
    }

    private static boolean dropWater(String[][] map, int row, int col) {
        boolean forcedLeft = false;

        while (true) {
            map[row][col] = "|";
//			System.out.println("checking (" + row + ", " + col + ")");
            int decision = 0;
            if (row >= maxrow - minrow) {
                // should continue - no if water fell to the last row
                return false;
            }
            if (!isBlocked(map, row + 1, col)) {
                forcedLeft = false;
                row = row + 1;
                continue;
            }

            if (!isBlocked(map, row, col + 1) && !forcedLeft) {
                col = col + 1;
                forcedLeft = false;

                int tmpcol = col;
                while (isBlocked(map, row + 1, tmpcol) && !isBlocked(map, row, tmpcol)) {
                    map[row][tmpcol] = "|";
                    tmpcol++;
                }
                printMap(map);
                dropWater(map, row, tmpcol);
            }

            if (!isBlocked(map, row, col - 1)) {
                forcedLeft = true;
                col = col - 1;

                int tmpcol = col;
                while (isBlocked(map, row + 1, tmpcol) && !isBlocked(map, row, tmpcol)) {
                    map[row][tmpcol] = "|";
                    tmpcol--;
                }
                dropWater(map, row, tmpcol);
            } else {
                map[row][col] = "~";
                return true;
            }
        }
    }

    private static boolean isTravelled(String[][] map, int row, int col) {
        return "#".equals(map[row][col]) || "~".equals(map[row][col]) || "|".equals(map[row][col]);
    }

    private static boolean isBlocked(String[][] map, int row, int col) {
        return "#".equals(map[row][col]) || "~".equals(map[row][col]);
    }

    private static void printMap(String[][] map) {
        for (String[] line : map) {
            for (String square : line) {
                System.out.print(square == null ? "." : square);
            }
            System.out.println();
        }
        System.out.println();

    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d17/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
