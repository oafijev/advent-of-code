package com.oafijev.advent.y2018.d20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class D20A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    private static int index = 0;
    private static int minr;
    private static int maxr;
    private static int minc;
    private static int maxc;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);

        ArrayDeque<Coord> resets = new ArrayDeque<>();
        Map<Coord, Square> coordsToSquares = new HashMap<>();

        Coord current = new Coord(0,0);
        Square currentSquare = new Square(current);
        coordsToSquares.put(current, currentSquare);

        while (index < line.length()) {
            char currentChar = line.charAt(index);
            if (isValue(currentChar)) {
                Coord newCoord;
                if (currentChar == 'N') {
                    newCoord = new Coord(current.getR() - 1, current.getC());
                } else if (currentChar == 'E') {
                    newCoord = new Coord(current.getR(), current.getC() + 1);
                } else if (currentChar == 'S') {
                    newCoord = new Coord(current.getR() + 1, current.getC());
                } else if (currentChar == 'W') {
                    newCoord = new Coord(current.getR(), current.getC() - 1);
                } else {
                    throw new IllegalArgumentException();
                }
                if (!coordsToSquares.containsKey(newCoord)) {
                    coordsToSquares.put(newCoord, new Square(newCoord));
                }
                Square newSquare = coordsToSquares.get(newCoord);
                currentSquare = coordsToSquares.get(current);
                newSquare.addAdjacent(currentSquare);
                currentSquare.addAdjacent(newSquare);

                current = newCoord;
                currentSquare = newSquare;
            } else if (currentChar == '(') {
                resets.addLast(current);
                System.out.println("Creating branch point " + current);
            } else if (currentChar == '|') {
                current = resets.peekLast();
                System.out.println("Resetting to branch point and trying new branch " + current);
            } else if (currentChar == ')') {
                System.out.println("Ending branch and resetting to " + current);
                current = resets.removeLast();
            } else if (currentChar == '^') {
                System.out.println("Beginning");
            } else if (currentChar == '$') {
                System.out.println("Ending");
                break;
            } else {
                throw new IllegalStateException();
            }
            index++;
        }

        minr = coordsToSquares.keySet().stream().min(Comparator.comparing(Coord::getR)).get().getR();
        maxr = coordsToSquares.keySet().stream().max(Comparator.comparing(Coord::getR)).get().getR();
        minc = coordsToSquares.keySet().stream().min(Comparator.comparing(Coord::getC)).get().getC();
        maxc = coordsToSquares.keySet().stream().max(Comparator.comparing(Coord::getC)).get().getC();
        System.out.println(minr);
        System.out.println(maxr);
        System.out.println(minc);
        System.out.println(maxc);

        //printMap(coordsToSquares);

        ArrayDeque<Coord> queue = new ArrayDeque<>();
        Integer[][] distance = new Integer[maxr - minr + 1][maxc - minc + 1];
        distance[-minr][-minc] = 0;
        queue.addLast(new Coord(0, 0));

        while (!queue.isEmpty()) {
//            printMap(distance);
            current = queue.removeFirst();
            int currentDistance = distance[current.getR() - minr][current.getC() - minc];

            for (Square square : coordsToSquares.get(current).getAdjacent()) {
                if (distance[square.getCoord().getR() - minr][square.getCoord().getC() - minc] == null) {
                    distance[square.getCoord().getR() - minr][square.getCoord().getC() - minc] = currentDistance + 1;
                    queue.addLast(square.getCoord());
                } else if (currentDistance + 1 < distance[square.getCoord().getR() - minr][square.getCoord().getC() - minc]) {
                    distance[square.getCoord().getR() - minr][square.getCoord().getC() - minc] = currentDistance + 1;
                    queue.addLast(square.getCoord());
                }
            }
        }

        printMap(distance);

        int best = Integer.MIN_VALUE;
        int moreThan1000 = 0;
        for (int r = 0; r < distance.length; r++) {
            for (int c = 0; c < distance[r].length; c++) {
                if (distance[r][c] > best) {
                    best = distance[r][c];
                }
                if (distance[r][c] >= 1000) {
                    moreThan1000++;
                }
            }
        }
        System.out.println("Part A: " + best);
        System.out.println("Part b: " + moreThan1000);
    }

    private static void printMap(Integer[][] distance) {
        for (int r = 0; r < distance.length; r++) {
            for (int c = 0; c < distance[r].length; c++) {
                System.out.print(distance[r][c] == null ? ".\t" : distance[r][c] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void printMap(Map<Coord, Square> coordsToSquares) {

        for (int r = minr; r <= maxr; r++) {
            for (int c = minc; c <= maxc; c++) {
                Coord coord = new Coord(r, c);
                if (coordsToSquares.containsKey(coord)) {
                    System.out.print("┤");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private static boolean isValue(char currentChar) {
        return currentChar != '(' && currentChar != ')' && currentChar != '|' && currentChar != '$' && currentChar != '^';
    }

    public static class Square {
        private List<Square> adjacent = new ArrayList<>();
        private Coord coord;

        Square(Coord coord) {
            this.coord = coord;
        }

        public Coord getCoord() {
            return this.coord;
        }

        public List<Square> getAdjacent() {
            return adjacent;
        }

        public void addAdjacent(Square other) {
            if (!this.adjacent.contains(other)) {
                this.adjacent.add(other);
            }
        }

        public void makeAdjacent(Square other) {
            addAdjacent(other);
            other.addAdjacent(this);
        }
    }
    public static class Coord {
        private int r;
        private int c;

        Coord(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.r, this.c);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else {
                Coord o = (Coord) obj;
                return o.r == this.r && o.c == this.c;
            }
        }

        @Override
        public String toString() {
            return "Coord: (row,column) = (" + this.r + "," + this.c + ")";
        }

        public int getR() {
            return r;
        }

        public int getC() {
            return c;
        }

    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d20/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
