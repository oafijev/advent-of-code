package com.oafijev.advent.y2018.d09;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class D09A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    private static int current = 0;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        String[] params = line.split(" ");
        int players = Integer.valueOf(params[0]).intValue();
        int lastScore = Integer.valueOf(params[6]).intValue();

        List<Integer> marbles = new ArrayList<>();
        Map<Integer, Integer> score = new HashMap<>();

        int currentPlayer = 0;
        for (int i = 0; i < players; i++) {
            score.put(i, 0);
        }
        marbles.add(0);
        for (int i = 1; i <= lastScore; i++) {
            if (i % 23 == 0) {
                score.put(currentPlayer, score.get(currentPlayer) + i);
                int marbleToRemove = (current - 6 + marbles.size()) % marbles.size();
                score.put(currentPlayer, score.get(currentPlayer) + marbles.remove(marbleToRemove));
                current = (marbleToRemove - 1) % marbles.size();
            } else {
                placeMarble(i, marbles);
            }
            currentPlayer = (currentPlayer + 1) % players;
            if (i % 10000 == 0) {
                System.out.println(i);
            }
        }

        int bestScore = 0;
        for (Integer scoreValue : score.values()) {
            if (scoreValue > bestScore) {
                bestScore = scoreValue;
            }
        }
        System.out.println(bestScore);
    }

    private static void placeMarble(int label, List<Integer> marbles) {
        current = (current + 2) % marbles.size();
        marbles.add(current + 1, label);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d09/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
