package com.oafijev.advent.y2018.d09;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class D09B {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;


    public static void main(String[] args) throws Exception {
        String[] params = readInput().get(0).split(" ");
        int players = Integer.valueOf(params[0]).intValue();
        int lastScore = Integer.valueOf(params[6]).intValue() * 100;

        int currentPlayer = 0;
        Map<Integer, Long> score = new HashMap<>();
        for (int i = 0; i < players; i++) {
            score.put(i, 0L);
        }
        CircularLinkedList cll = new CircularLinkedList(0);

        for (int i = 1; i <= lastScore; i++) {
            if (i % 23 == 0) {
                int removedValue = cll.unadvanceSevenAndRemove();
                score.put(currentPlayer, score.get(currentPlayer) + i + removedValue);
            } else {
                cll.advanceAndAddAfterCurrentAndSetCurrentToNewNode(i);
            }
            currentPlayer = (currentPlayer + 1) % players;
            if (i % 100000 == 0) {
                System.out.println(i);
            }
        }

        long bestScore = 0;
        for (Long scoreValue : score.values()) {
            if (scoreValue > bestScore) {
                bestScore = scoreValue;
            }
        }
        System.out.println(bestScore);
    }

    public static class Node {
        private Integer value;
        private Node next;
        private Node previous;

        public Node(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrevious() {
            return previous;
        }

        public void setPrevious(Node previous) {
            this.previous = previous;
        }
    }

    public static class CircularLinkedList {
        private Node current;
        private Node root;

        public CircularLinkedList(Integer value) {
            this.current = new Node(value);
            this.current.setNext(this.current);
            this.current.setPrevious(this.current);
            this.root = this.current;
        }

        public int unadvanceSevenAndRemove() {
            this.current = this.current.getPrevious().getPrevious().getPrevious().getPrevious().getPrevious().getPrevious();

            this.current.getPrevious().setNext(this.current.getNext());
            this.current.getNext().setPrevious(this.current.getPrevious());
            return this.current.getValue();
        }

        public void advanceAndAddAfterCurrentAndSetCurrentToNewNode(Integer value) {
            this.current = this.current.getNext().getNext();
            addAfterCurrentAndSetCurrent(value);
        }

        private void addAfterCurrentAndSetCurrent(Integer value) {
            Node node = new Node(value);
            node.setPrevious(this.current);
            node.setNext(this.current.getNext());
            this.current.getNext().setPrevious(node);
            this.current.setNext(node);
        }

        @Override
        public String toString() {
            Node node = this.root;
            List<Integer> items = new ArrayList<>();
            do {
                items.add(node.getValue());
                node = node.getNext();
            } while (node != this.root);
            return items.toString();
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(
                new FileReader(new File("src/main/java/com/oafijev/advent/y2018/d09/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
