package com.oafijev.advent.y2018.d14;

import java.util.ArrayList;
import java.util.List;

public class D14A {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        int input = 170641;
//		int input = 9;
        int elf1 = 0;
        int elf2 = 1;

        List<Integer> recipes = new ArrayList<>();
        recipes.add(3);
        recipes.add(7);

        while (recipes.size() < input + 10) {
            int newValue = recipes.get(elf1) + recipes.get(elf2);
            if (newValue >= 10) {
                recipes.add(newValue / 10);
            }
            recipes.add(newValue % 10);
            elf1 = (elf1 + 1 + recipes.get(elf1)) % recipes.size();
            elf2 = (elf2 + 1 + recipes.get(elf2)) % recipes.size();
        }

        for (int i = recipes.size() - 10; i < recipes.size(); i++) {
            System.out.print(recipes.get(i));
//			System.out.println(String.join("", recipes.subList(recipes.size() - 10, recipes.size()).toArray()));
        }
    }

}
