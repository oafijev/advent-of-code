package com.oafijev.advent.y2018.d14;

import java.util.ArrayList;
import java.util.List;

public class D14B {
    public static final int OFFSET = 60;
    public static final int WORKERS = 5;

    public static void main(String[] args) throws Exception {
        String input = "170641";
//		String input = "01245";
        int count = 2;
        int elf1 = 0;
        int elf2 = 1;

        List<Integer> recipes = new ArrayList<>();
        recipes.add(3);
        recipes.add(7);

        String last10 = "37";

        while (true) {
            if (count++ % 100000 == 0) {
                System.out.println("size: " + count);
            }
            int newValue = recipes.get(elf1) + recipes.get(elf2);
            if (newValue >= 10) {
                recipes.add(newValue / 10);
                last10 = last10 + (newValue / 10);
            }
            recipes.add(newValue % 10);
            last10 = last10 + (newValue % 10);
            elf1 = (elf1 + 1 + recipes.get(elf1)) % recipes.size();
            elf2 = (elf2 + 1 + recipes.get(elf2)) % recipes.size();

            while (last10.length() > 10) {
                last10 = last10.substring(1);
            }

            if (last10.indexOf(input) > 0) {
                System.out.println(last10.indexOf(input));
                System.out.println(recipes.size() - 10 + last10.indexOf(input));
                break;
            }
        }
    }

}
