package com.oafijev.advent.y2019.d10;

import com.oafijev.advent.y2018.d25.D25A;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D10 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Set<Point> asteroids = getAsteroids(input);
        Map<Point, Integer> asteroidsInSight = new HashMap<>();

        for (Point asteroid : asteroids) {
            int numberOfSeenAsteroids = 0;
            for (Point potentiallySeenAsteroid : asteroids) {
                if (asteroid.equals(potentiallySeenAsteroid)) {
                    continue;
                }
                boolean seen = true;
                for (Point potentiallyBlockingAsteroid : asteroids) {
                    if (asteroid.equals(potentiallyBlockingAsteroid)) {
                        continue;
                    } else if (potentiallySeenAsteroid.equals(potentiallyBlockingAsteroid)) {
                        continue;
                    }

                    if (isSame(new Angle(asteroid, potentiallyBlockingAsteroid), new Angle(potentiallyBlockingAsteroid, potentiallySeenAsteroid))) {
                        seen = false;
                        break;
                    }
                }
                if (seen) {
                    numberOfSeenAsteroids++;
                }
            }
            asteroidsInSight.put(asteroid, numberOfSeenAsteroids);
            System.out.println("[" + asteroid.x + ", " + asteroid.y + "] sees " + numberOfSeenAsteroids);
        }

        Point bestAsteroid = null;
        int mostSeen = 0;
        for (Point asteroid : asteroidsInSight.keySet()) {
            int seen = asteroidsInSight.get(asteroid);
            if (seen > mostSeen) {
                bestAsteroid = asteroid;
                mostSeen = seen;
            }
        }
        System.out.println("Part 1: [" + bestAsteroid.x + ", " + bestAsteroid.y + "] sees " + mostSeen);
    }

    private static boolean isSame(Angle angle1, Angle angle2) {
        return (angle1.dx * angle2.dy == angle1.dy * angle2.dx) && (angle1.dx * angle2.dx >= 0) && (angle1.dy * angle2.dy >= 0);
    }

    public static class Angle {
        public int dx;
        public int dy;

        public Angle(Point origin, Point destination) {
            this.dx = destination.x - origin.x;
            this.dy = destination.y - origin.y;
        }
    }

    private static Set<Point> getAsteroids(List<String> input) {
        Set<Point> result = new HashSet<>();

        int y = 0;
        for (String line : input) {
            int x = 0;
            for (char character : line.toCharArray()) {
                if (character == '#') {
                    result.add(new Point(x, y));
                }
                x++;
            }
            y++;
        }

        return result;
    }

    private static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
