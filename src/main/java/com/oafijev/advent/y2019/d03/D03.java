package com.oafijev.advent.y2019.d03;

import com.oafijev.advent.y2018.d25.D25A;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D03 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        SortedSet<Point> pointsByManhattanDistance = new TreeSet<>(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Point p1 = (Point) o1;
                Point p2 = (Point) o2;
                return p1.distanceFromOrigin() - p2.distanceFromOrigin();
            }
        });

        Map<Point, Integer> line1 = getPoints(input.get(0));
        Map<Point, Integer> line2 = getPoints(input.get(1));
        Set<Point> intersections = new LinkedHashSet<>(line1.keySet());
        intersections.retainAll(line2.keySet());
        pointsByManhattanDistance.addAll(intersections);

        Point firstPoint = pointsByManhattanDistance.first();
        System.out.println("[x, y] = [" + firstPoint.getX() + ", " + firstPoint.getY() + "]");
        System.out.println("part 1: " + firstPoint.distanceFromOrigin());

        int bestSteps = Integer.MAX_VALUE;
        Point bestPoint = null;
        for (Point intersection : intersections) {
            int steps = line1.get(intersection) + line2.get(intersection);
            System.out.println("Found intersection, steps = " + steps);
            if (bestPoint == null) {
                bestPoint = intersection;
                bestSteps = line1.get(bestPoint) + line2.get(bestPoint);
            }

            if (steps < bestSteps) {
                bestPoint = intersection;
                bestSteps = steps;
            }
        }
        System.out.println("[x, y] = [" + bestPoint.getX() + ", " + bestPoint.getY() + "]");
        System.out.println("part 2: " + bestSteps);
    }

    private static Map<Point, Integer> getPoints(String line) {
        Map<Point, Integer> points = new LinkedHashMap<>();
        String[] operations = line.split(",");
        int x = 0;
        int y = 0;
        int steps = 0;
        for (String operation : operations) {
            char direction = operation.charAt(0);
            int distance = Integer.parseInt(operation.substring(1));

            for (int i = 0; i < distance; i++) {
                switch (direction) {
                    case 'U':
                        points.put(new Point(x, ++y), ++steps);
                        break;
                    case 'D':
                        points.put(new Point(x, --y), ++steps);
                        break;
                    case 'R':
                        points.put(new Point(++x, y), ++steps);
                        break;
                    case 'L':
                        points.put(new Point(--x, y), ++steps);
                        break;
                    default:
                        throw new IllegalStateException("unknown direction " + direction);
                }
            }
        }

        return points;
    }

    private static class Point {
        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int distanceFromOrigin() {
            return Math.abs(this.x) + Math.abs(this.y);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Point)) {
                return false;
            }

            Point that = (Point) obj;
            if (this == that) {
                return true;
            } else {
                return this.x == that.x && this.y == that.y;
            }
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d03/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
