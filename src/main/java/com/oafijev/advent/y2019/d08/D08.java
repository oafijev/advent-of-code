package com.oafijev.advent.y2019.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D08 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int width = 25;
        int height = 6;

        List<Layer> images = getImages(input.get(0), width, height);

        System.out.println(images.size());
        long leastZeroes = Long.MAX_VALUE;
        Layer leastZeroesLayer = null;
        for (Layer image : images) {
            long zeroes = image.getNumberOfDigits('0');
            if (zeroes < leastZeroes) {
                leastZeroesLayer = image;
                leastZeroes = zeroes;
            }
        }

        System.out.println("Part 1: " + (leastZeroesLayer.getNumberOfDigits('1') * leastZeroesLayer.getNumberOfDigits('2')));

        printResultImage(images, width, height);
    }

    private static void printResultImage(List<Layer> images, int width, int height) {
        StringBuilder result = new StringBuilder();

        for (int h = 0; h < height; h++)  {
            for (int w = 0; w < width; w++) {
                int pos = w + h * width;

                char output = '.';
                for (Layer layer : images) {
                    if (layer.charAt(pos) == '0') {
                        output = ' ';
                        break;
                    } else if (layer.charAt(pos) == '1') {
                        output = '#';
                        break;
                    }
                }
                System.out.print(output);
            }
            System.out.println("");
        }

    }

    private static List<Layer> getImages(String input, int width, int height) {
        List<Layer> result = new ArrayList<>();
        int initialIndex = 0;
        int imageSize = width * height;
        while (initialIndex < input.length()) {
            result.add(new Layer(input.substring(initialIndex, initialIndex + imageSize)));
            initialIndex += imageSize;
        }

        return result;
    }

    private static class Layer {
        private String data;

        public Layer(String data) {
            this.data = data;
        }

        public long getNumberOfDigits(char digit) {
            return this.data.chars().filter(c -> c == digit).count();
        }

        public char charAt(int i) {
            return this.data.charAt(i);
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
