package com.oafijev.advent.y2019.d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

import static java.lang.Math.abs;

public class D12 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Moon moon1 = getMoon(input.get(0));
        Moon moon2 = getMoon(input.get(1));
        Moon moon3 = getMoon(input.get(2));
        Moon moon4 = getMoon(input.get(3));

//        part1(moon1, moon2, moon3, moon4);
        part2(moon1, moon2, moon3, moon4);

    }

    private static void part2(Moon moon1, Moon moon2, Moon moon3, Moon moon4) {
        Set<String> positions = new HashSet<>();

        int stepsX = 0;
        for (int x = 0; true; x++) {
            if (x % 100000 == 0) {
                System.out.println("Executing step " + x);
            }
            calculateNewVelocityX(moon1, moon2, moon3, moon4);
            calculateNewVelocityX(moon2, moon1, moon3, moon4);
            calculateNewVelocityX(moon3, moon1, moon2, moon4);
            calculateNewVelocityX(moon4, moon1, moon2, moon3);

            moveMoonX(moon1);
            moveMoonX(moon2);
            moveMoonX(moon3);
            moveMoonX(moon4);

            String state = moon1.toString() + moon2.toString() + moon3.toString() + moon4.toString();
            if (!positions.contains(state)) {
                positions.add(state);
            } else {
                System.out.println("Match found in step " + x);
                stepsX = x;
                break;
            }
        }

        int stepsY = 0;
        positions.clear();
        for (int y = 0; true; y++) {
            if (y % 100000 == 0) {
                System.out.println("Executing step " + y);
            }
            calculateNewVelocityY(moon1, moon2, moon3, moon4);
            calculateNewVelocityY(moon2, moon1, moon3, moon4);
            calculateNewVelocityY(moon3, moon1, moon2, moon4);
            calculateNewVelocityY(moon4, moon1, moon2, moon3);

            moveMoonY(moon1);
            moveMoonY(moon2);
            moveMoonY(moon3);
            moveMoonY(moon4);

            String state = moon1.toString() + moon2.toString() + moon3.toString() + moon4.toString();
            if (!positions.contains(state)) {
                positions.add(state);
            } else {
                System.out.println("Match found in step " + y);
                stepsY = y;
                break;
            }
        }

        int stepsZ = 0;
        positions.clear();
        for (int z = 0; true; z++) {
            if (z % 100000 == 0) {
                System.out.println("Executing step " + z);
            }
            calculateNewVelocityZ(moon1, moon2, moon3, moon4);
            calculateNewVelocityZ(moon2, moon1, moon3, moon4);
            calculateNewVelocityZ(moon3, moon1, moon2, moon4);
            calculateNewVelocityZ(moon4, moon1, moon2, moon3);

            moveMoonZ(moon1);
            moveMoonZ(moon2);
            moveMoonZ(moon3);
            moveMoonZ(moon4);

            String state = moon1.toString() + moon2.toString() + moon3.toString() + moon4.toString();
            if (!positions.contains(state)) {
                positions.add(state);
            } else {
                System.out.println("Match found in step " + z);
                stepsZ = z;
                break;
            }
        }

        BigInteger xyLcm = lcm(BigInteger.valueOf(stepsX), BigInteger.valueOf(stepsY));
        BigInteger lcm = lcm(xyLcm, BigInteger.valueOf(stepsZ));

        System.out.println(lcm);
    }

    private static BigInteger lcm(BigInteger a, BigInteger b) {
        return a.multiply(b).divide(a.gcd(b));
    }

    private static void calculateNewVelocity(Moon moon, Moon moon1, Moon moon2, Moon moon3) {
        calculateNewVelocityX(moon, moon1, moon2, moon3);
        calculateNewVelocityY(moon, moon1, moon2, moon3);
        calculateNewVelocityZ(moon, moon1, moon2, moon3);
    }

    private static void calculateNewVelocityZ(Moon moon, Moon moon1, Moon moon2, Moon moon3) {
        moon.setVz(moon.getVz()
                + (moon.getZ() > moon1.getZ() ? -1 : 0)
                + (moon.getZ() > moon2.getZ() ? -1 : 0)
                + (moon.getZ() > moon3.getZ() ? -1 : 0)
                + (moon.getZ() < moon1.getZ() ? 1 : 0)
                + (moon.getZ() < moon2.getZ() ? 1 : 0)
                + (moon.getZ() < moon3.getZ() ? 1 : 0)
        );
    }

    private static void calculateNewVelocityY(Moon moon, Moon moon1, Moon moon2, Moon moon3) {
        moon.setVy(moon.getVy()
                + (moon.getY() > moon1.getY() ? -1 : 0)
                + (moon.getY() > moon2.getY() ? -1 : 0)
                + (moon.getY() > moon3.getY() ? -1 : 0)
                + (moon.getY() < moon1.getY() ? 1 : 0)
                + (moon.getY() < moon2.getY() ? 1 : 0)
                + (moon.getY() < moon3.getY() ? 1 : 0)
        );
    }

    private static void calculateNewVelocityX(Moon moon, Moon moon1, Moon moon2, Moon moon3) {
        moon.setVx(moon.getVx()
                + (moon.getX() > moon1.getX() ? -1 : 0)
                + (moon.getX() > moon2.getX() ? -1 : 0)
                + (moon.getX() > moon3.getX() ? -1 : 0)
                + (moon.getX() < moon1.getX() ? 1 : 0)
                + (moon.getX() < moon2.getX() ? 1 : 0)
                + (moon.getX() < moon3.getX() ? 1 : 0)
        );
    }

    private static void part1(Moon moon1, Moon moon2, Moon moon3, Moon moon4) {
        for (int i = 0; i < 1000; i++) {
            calculateNewVelocity(moon1, moon2, moon3, moon4);
            calculateNewVelocity(moon2, moon1, moon3, moon4);
            calculateNewVelocity(moon3, moon1, moon2, moon4);
            calculateNewVelocity(moon4, moon1, moon2, moon3);

            moveMoon(moon1);
            moveMoon(moon2);
            moveMoon(moon3);
            moveMoon(moon4);

            if (i % 10 == 9) {
                System.out.println("\n===== Step " + i + " =====");
                System.out.println(moon1);
                System.out.println(moon2);
                System.out.println(moon3);
                System.out.println(moon4);
                System.out.println("energy = " + (moon1.getEnergy() + moon2.getEnergy() + moon3.getEnergy() + moon4.getEnergy()));
            }
        }
    }

    private static void moveMoon(Moon moon) {
        moveMoonX(moon);
        moveMoonY(moon);
        moveMoonZ(moon);
    }

    private static void moveMoonZ(Moon moon) {
        moon.setZ(moon.getZ() + moon.getVz());
    }

    private static void moveMoonY(Moon moon) {
        moon.setY(moon.getY() + moon.getVy());
    }

    private static void moveMoonX(Moon moon) {
        moon.setX(moon.getX() + moon.getVx());
    }

    private static void calculateNewVelocities(Moon moon1, Moon moon2) {
        calculateNewVelocitiesX(moon1, moon2);
        calculateNewVelocitiesY(moon1, moon2);
        calculateNewVelocitiesZ(moon1, moon2);
    }

    private static void calculateNewVelocitiesZ(Moon moon1, Moon moon2) {
        if (moon1.getZ() > moon2.getZ()) {
            moon1.setVz(moon1.getVz() - 1);
            moon2.setVz(moon2.getVz() + 1);
        } else if (moon1.getZ() < moon2.getZ()) {
            moon1.setVz(moon1.getVz() + 1);
            moon2.setVz(moon2.getVz() - 1);
        }
    }

    private static void calculateNewVelocitiesY(Moon moon1, Moon moon2) {
        if (moon1.getY() > moon2.getY()) {
            moon1.setVy(moon1.getVy() - 1);
            moon2.setVy(moon2.getVy() + 1);
        } else if (moon1.getY() < moon2.getY()) {
            moon1.setVy(moon1.getVy() + 1);
            moon2.setVy(moon2.getVy() - 1);
        }
    }

    private static void calculateNewVelocitiesX(Moon moon1, Moon moon2) {
        if (moon1.getX() > moon2.getX()) {
            moon1.setVx(moon1.getVx() - 1);
            moon2.setVx(moon2.getVx() + 1);
        } else if (moon1.getX() < moon2.getX()) {
            moon1.setVx(moon1.getVx() + 1);
            moon2.setVx(moon2.getVx() - 1);
        }
    }

    private static Moon getMoon(String s) {
        String[] coords = s.substring(1, s.length() - 1).split(", ");
        return new Moon(Integer.parseInt(coords[0].split("=")[1]),
                Integer.parseInt(coords[1].split("=")[1]),
                Integer.parseInt(coords[2].split("=")[1]));
    }

    private static class Moon {
        private int x;
        private int y;
        private int z;
        private int vx = 0;
        private int vy = 0;
        private int vz = 0;

        public Moon(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public int getVx() {
            return vx;
        }

        public void setVx(int vx) {
            this.vx = vx;
        }

        public int getVy() {
            return vy;
        }

        public void setVy(int vy) {
            this.vy = vy;
        }

        public int getVz() {
            return vz;
        }

        public void setVz(int vz) {
            this.vz = vz;
        }

        public int getEnergy() {
            return (abs(x) + abs(y) + abs(z)) * (abs(vx) + abs(vy) + abs(vz));
        }
        @Override
        public String toString() {
            return String.format("pos = [%d, %d, %d], vel = [%d, %d, %d]", x, y, z, vx, vy, vz);
        }
    }
    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d12/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
