package com.oafijev.advent.y2019.d04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D04 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        //145852-616942

        int results = 0;
//        for (int i = 145852; i < 145854; i++) {
        for (int i = 145852; i < 616943; i++) {
            if (validNumber(i)) {
                results++;
            }
        }

        System.out.println(results);
    }

    private static boolean validNumber(int i) {
        int f = i % 10;
        int e = (i % 100) / 10;
        int d = (i % 1000) / 100;
        int c = (i % 10000) / 1000;
        int b = (i % 100000) / 10000;
        int a = i / 100000;

        if (a > b || b > c || c > d || d > e || e > f) {
            return false;
        }

        // Part 1
//        if (a != b && b != c && c != d && d != e && e != f) {
//            return false;
//        }

        // Part 2
        if (a == b) {
            if (b == c) {
                if (c == d) {
                    if (d == e) {
                        return false;
                    } else { // d != e
                        return e == f;
                    }
                } else { // c != d
                    if (d == e) {
                        return e != f;
                    } else {
                        return e == f;
                    }
                }
            } else { // b != c;
                return true;
            }
        } else { // a != b
            if (b == c) {
                if (c == d) {
                    if (d == e) {
                        return false;
                    } else { // d != e
                        return e == f;
                    }
                } else { // c != d
                    return true;
                }
            } else { // b != c
                if (c == d) {
                    if (d == e) {
                        return false;
                    } else { // d != e
                        return true;
                    }
                } else { // c != d
                    if (d == e) {
                        return e != f;
                    } else { // d != e
                        return e == f;
                    }
                }
            }
        }

//        System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);
//        System.out.println(i);
//        return true;
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d04/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
