package com.oafijev.advent.y2019.d01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class D01 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Long result = 0l;

        for (String line : input) {
            Long mass = Long.parseLong(line);

            long fuel = mass;
            while (fuel > 0) {
                System.out.println(result + " " + fuel);
                fuel = (fuel / 3) - 2;
                if (fuel > 0) {
                    result += fuel;
                }
            }
        }

        System.out.println("fuel = " + result);

    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d01/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
