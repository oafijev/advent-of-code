package com.oafijev.advent.y2019.d09;

import com.oafijev.advent.y2019.Intcode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D09 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Intcode computer = new Intcode(input);
        while(true) {
//            long result = computer.doInput(1);  // Part1
            long result = computer.doInput(2);  // Part2
            System.out.print(result + ",");
        }

    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d09/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
