package com.oafijev.advent.y2019.d02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D02 {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

//        int[] ops = tryInput(input, 12, 2);
//        System.out.println(ops[0]);

        for (int noun = 0; noun <= 99; noun++) {
            for (int verb = 0; verb <= 99; verb++) {
                int result = tryInput(input, noun, verb);
//                System.out.println(noun + ", " + verb + ", " + result);
                if (result == 19690720) {
                    System.out.println(noun + ", " + verb);
                    System.out.println(100 * noun + verb);
                }
            }
        }

    }

    private static int tryInput(List<String> input, int noun, int verb) {
        int[] ops = Arrays.stream(input.get(0).split(",")).mapToInt(e -> Integer.parseInt(e)).toArray();
        int position = 0;

        ops[1] = noun;
        ops[2] = verb;

        while (ops[position] != 99) {
            if (ops[position] == 1) {
                ops[ops[position + 3]] = ops[ops[position + 1]] + ops[ops[position + 2]];
            } else if (ops[position] == 2) {
                ops[ops[position + 3]] = ops[ops[position + 1]] * ops[ops[position + 2]];
            }
            position += 4;
        }

        return ops[0];
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d02/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
