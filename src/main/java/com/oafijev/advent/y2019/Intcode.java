package com.oafijev.advent.y2019;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Intcode {
//    private int[] ops;
    private Map<Long, Long> ops = new HashMap<Long, Long>() {
        @Override
        public Long get(Object key) {
            if (super.get(key) == null) {
                super.put((Long) key, 0L);
            }
            return super.get(key);
        }
    };
    long position = 0;
    private long relativeBase = 0;

    public Intcode(List<String> input) {
        long index = 0;
        String[] split = input.get(0).split(",");
        for (String data : split) {
            this.ops.put(index++, Long.parseLong(data));
        }
    }

    public long doInput(long inputValue) {

        while ((ops.get(position) % 100) != 99) {
            long instruction = ops.get(position);
            long opcode = instruction % 100;
            long modeA = (instruction / 100) % 10;
            long modeB = (instruction / 1000) % 10;
            long modeC = (instruction / 10000) % 10;

            if (opcode == 1) {
                ops.put(getOutputParameter(ops, position + 3, modeC), getParameter(ops, position + 1, modeA) + getParameter(ops, position + 2, modeB));
                position += 4;
            } else if (opcode == 2) {
                ops.put(getOutputParameter(ops, position + 3, modeC), getParameter(ops, position + 1, modeA) * getParameter(ops, position + 2, modeB));
                position += 4;
            } else if (opcode == 3) {
                ops.put(getOutputParameter(ops, position + 1, modeA), inputValue);
                position += 2;
            } else if (opcode == 4) {
                long result = getParameter(ops, position + 1, modeA);
                position += 2;
                return result;
            } else if (opcode == 5) {
                if (getParameter(ops, position + 1, modeA) != 0) {
                    position = getParameter(ops, position + 2, modeB);
                } else {
                    position += 3;
                }
            } else if (opcode == 6) {
                if (getParameter(ops, position + 1, modeA) == 0) {
                    position = getParameter(ops, position + 2, modeB);
                } else {
                    position += 3;
                }
            } else if (opcode == 7) {
                if (getParameter(ops, position + 1, modeA) < getParameter(ops, position + 2, modeB)) {
                    ops.put(getOutputParameter(ops, position + 3, modeC), 1l);
                } else {
                    ops.put(getOutputParameter(ops, position + 3, modeC), 0l);
                }
                position += 4;
            } else if (opcode == 8) {
                if (getParameter(ops, position + 1, modeA) == getParameter(ops, position + 2, modeB)) {
                    ops.put(getOutputParameter(ops, position + 3, modeC), 1l);
                } else {
                    ops.put(getOutputParameter(ops, position + 3, modeC), 0l);
                }
                position += 4;
            } else if (opcode == 9) {
                this.relativeBase += getParameter(ops, position + 1, modeA);
                position += 2;
            }
        }

        throw new IllegalStateException("should not have finished.");
    }

    private long getParameter(Map<Long, Long> ops, long i, long mode) {
        if (mode == 0) {
            return ops.get(ops.get(i));
        } else if (mode == 1) {
            return ops.get(i);
        } else if (mode == 2) {
            return ops.get(ops.get(i) + this.relativeBase);
        } else {
            throw new IllegalArgumentException("mode = " + mode);
        }
    }

    private long getOutputParameter(Map<Long, Long> ops, long i, long mode) {
        if (mode == 0) {
            return ops.get(i);
        } else if (mode == 2) {
            return ops.get(i) + this.relativeBase;
        } else {
            throw new IllegalArgumentException("Illegal output mode mode = " + mode);
        }
    }
}
