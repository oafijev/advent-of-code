package com.oafijev.advent.y2019.d07;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D07 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

//        part1(input);
        part2(input);

    }

    private static void part1(List<String> input) {
        int best = 0;
        for (int a = 0; a < 5; a++) {
            for (int b = 0; b < 5; b++) {
                if (b == a) {
                    continue;
                }
                for (int c = 0; c < 5; c++) {
                    if (c == a || c == b) {
                        continue;
                    }
                    for (int d = 0; d < 5; d++) {
                        if (d == a || d == b || d == c) {
                            continue;
                        }
                        for (int e = 0; e < 5; e++) {
                            if (e == a || e == b || e == c || e == d) {
                                continue;
                            }
                            int o1 = new Amplifier(input, a).doInput(0);
                            int o2 = new Amplifier(input, b).doInput(o1);
                            int o3 = new Amplifier(input, c).doInput(o2);
                            int o4 = new Amplifier(input, d).doInput(o3);
                            int o5 = new Amplifier(input, e).doInput(o4);
                            if (o5 > best) {
                                best = o5;
                                System.out.println("found best: " + best);
                                System.out.println("" + a + b + c + d + e);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void part2(List<String> input) {
        int best = 0;
        for (int a = 5; a < 10; a++) {
            for (int b = 5; b < 10; b++) {
                if (b == a) {
                    continue;
                }
                for (int c = 5; c < 10; c++) {
                    if (c == a || c == b) {
                        continue;
                    }
                    for (int d = 5; d < 10; d++) {
                        if (d == a || d == b || d == c) {
                            continue;
                        }
                        for (int e = 5; e < 10; e++) {
                            if (e == a || e == b || e == c || e == d) {
                                continue;
                            }
//                            System.out.println("Trying " + a + b + c + d + e);
                            Amplifier a1 = new Amplifier(input, a);
                            Amplifier a2 = new Amplifier(input, b);
                            Amplifier a3 = new Amplifier(input, c);
                            Amplifier a4 = new Amplifier(input, d);
                            Amplifier a5 = new Amplifier(input, e);
                            int lastResult = 0;
                            try {
                                while (true) {
                                    int o1 = a1.doInput(lastResult);
                                    int o2 = a2.doInput(o1);
                                    int o3 = a3.doInput(o2);
                                    int o4 = a4.doInput(o3);
                                    lastResult = a5.doInput(o4);
                                }
                            } catch (IllegalStateException ex) {
//                                System.out.println("halting");
                            }
                            if (lastResult > best) {
                                best = lastResult;
                                System.out.println("found best: " + best);
                                System.out.println("" + a + b + c + d + e);
                            }
                        }
                    }
                }
            }
        }
    }

    private static class Amplifier {
        private int[] ops;
        private int phase;
        private int inputValue;
        int position = 0;
        boolean phaseUsed = false;

        public Amplifier(List<String> input, int phase) {
            this.phase = phase;
            this.ops = Arrays.stream(input.get(0).split(",")).mapToInt(e -> Integer.parseInt(e)).toArray();
        }

        public int doInput(int inputValue) {

            while ((ops[position] % 100) != 99) {
                int instruction = ops[position];
                int opcode = instruction % 100;
                int modeA = (instruction / 100) % 10;
                int modeB = (instruction / 1000) % 10;
                int modeC = (instruction / 10000) % 10;

                if (opcode == 1) {
                    ops[ops[position + 3]] = getParameter(ops, position + 1, modeA) + getParameter(ops, position + 2, modeB);
                    position += 4;
                } else if (opcode == 2) {
                    ops[ops[position + 3]] = getParameter(ops, position + 1, modeA) * getParameter(ops, position + 2, modeB);
                    position += 4;
                } else if (opcode == 3) {
                    if (!phaseUsed) {
                        ops[ops[position + 1]] = phase;
                        phaseUsed = true;
                    } else {
                        ops[ops[position + 1]] = inputValue;
                    }
                    position += 2;
                } else if (opcode == 4) {
                    int result = getParameter(ops, position + 1, modeA);
                    position += 2;
                    return result;
                } else if (opcode == 5) {
                    if (getParameter(ops, position + 1, modeA) != 0) {
                        position = getParameter(ops, position + 2, modeB);
                    } else {
                        position += 3;
                    }
                } else if (opcode == 6) {
                    if (getParameter(ops, position + 1, modeA) == 0) {
                        position = getParameter(ops, position + 2, modeB);
                    } else {
                        position += 3;
                    }
                } else if (opcode == 7) {
                    if (getParameter(ops, position + 1, modeA) < getParameter(ops, position + 2, modeB)) {
                        ops[ops[position + 3]] = 1;
                    } else {
                        ops[ops[position + 3]] = 0;
                    }
                    position += 4;
                } else if (opcode == 8) {
                    if (getParameter(ops, position + 1, modeA) == getParameter(ops, position + 2, modeB)) {
                        ops[ops[position + 3]] = 1;
                    } else {
                        ops[ops[position + 3]] = 0;
                    }
                    position += 4;
                }
            }

            throw new IllegalStateException("should not have finished.");
        }

    }

    private static int getParameter(int[] ops, int i, int mode) {
        if (mode == 0) {
            return ops[ops[i]];
        } else if (mode == 1) {
            return ops[i];
        } else {
            throw new IllegalArgumentException("mode = " + mode);
        }
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d07/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
