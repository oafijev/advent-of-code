package com.oafijev.advent.y2019.d05;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class D05 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

//        int[] ops = tryInput(input, 12, 2);
//        System.out.println(ops[0]);

//        for (int noun = 0; noun <= 99; noun++) {
//            for (int verb = 0; verb <= 99; verb++) {
                int result = doInput(input);
//                System.out.println(noun + ", " + verb + ", " + result);
//                if (result == 19690720) {
//                    System.out.println(noun + ", " + verb);
//                    System.out.println(100 * noun + verb);
//                }
//            }
//        }

    }

    private static int doInput(List<String> input) {
        int[] ops = Arrays.stream(input.get(0).split(",")).mapToInt(e -> Integer.parseInt(e)).toArray();
        int position = 0;

        while ((ops[position] % 100) != 99) {
            int instruction = ops[position];
            int opcode = instruction % 100;
            int modeA = (instruction / 100) % 10;
            int modeB = (instruction / 1000) % 10;
            int modeC = (instruction / 10000) % 10;

            if (opcode == 1) {
                ops[ops[position + 3]] = getParameter(ops, position + 1, modeA) + getParameter(ops, position + 2, modeB);
                position += 4;
            } else if (opcode == 2) {
                ops[ops[position + 3]] = getParameter(ops, position + 1, modeA) * getParameter(ops, position + 2, modeB);
                position += 4;
            } else if (opcode == 3) {
                ops[ops[position + 1]] = PART2_INPUT;
                position += 2;
            } else if (opcode == 4) {
                System.out.println("output: " + getParameter(ops, position + 1, modeA));
                position += 2;
            } else if (opcode == 5) {
                if (getParameter(ops, position + 1, modeA) != 0) {
                    position = getParameter(ops, position + 2, modeB);
                } else {
                    position += 3;
                }
            } else if (opcode == 6) {
                if (getParameter(ops, position + 1, modeA) == 0) {
                    position = getParameter(ops, position + 2, modeB);
                } else {
                    position += 3;
                }
            } else if (opcode == 7) {
                if (getParameter(ops, position + 1, modeA) < getParameter(ops, position + 2, modeB)) {
                    ops[ops[position + 3]] = 1;
                } else {
                    ops[ops[position + 3]] = 0;
                }
                position += 4;
            } else if (opcode == 8) {
                if (getParameter(ops, position + 1, modeA) == getParameter(ops, position + 2, modeB)) {
                    ops[ops[position + 3]] = 1;
                } else {
                    ops[ops[position + 3]] = 0;
                }
                position += 4;
            }
        }

        return ops[0];
    }

    private static int getParameter(int[] ops, int i, int mode) {
        if (mode == 0) {
            return ops[ops[i]];
        } else if (mode == 1) {
            return ops[i];
        } else {
            throw new IllegalArgumentException("mode = " + mode);
        }
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d05/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
