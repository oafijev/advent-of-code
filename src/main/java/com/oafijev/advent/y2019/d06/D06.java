package com.oafijev.advent.y2019.d06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class D06 {
    private static final int PART1_INPUT = 1;
    private static final int PART2_INPUT = 5;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Map<String,Planet> planetMap = new HashMap<>();

        for (String line : input) {
            String[] planets = line.split("\\)");
            String parentName = planets[0];
            String childName = planets[1];
            System.out.println(parentName + " -- " + childName);
            if (!planetMap.containsKey(parentName)) {
                planetMap.put(parentName, new Planet(parentName));
            }
            Planet parent = planetMap.get(parentName);

            if (!planetMap.containsKey(childName)) {
                planetMap.put(childName, new Planet(childName));
            }
            Planet child = planetMap.get(childName);

            parent.addOrbiter(child);
            child.setParent(parent);
        }

        int totalOrbits = 0;
        for (Planet planet : planetMap.values()) {
            totalOrbits += planet.getNumberOfOrbits();
        }

        System.out.println("part 1: " + totalOrbits);

        List<String> chainYou = planetMap.get("YOU").getOrbitChain();
        List<String> chainSan = planetMap.get("SAN").getOrbitChain();

        while(chainYou.get(0).compareTo(chainSan.get(0)) == 0) {
            chainYou.remove(0);
            chainSan.remove(0);
        }
        System.out.println("Part 2: " + (chainYou.size() + chainSan.size()));
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/main/java/com/oafijev/advent/y2019/d06/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }

    private static class Planet {
        private String name;
        private Planet parent;
        private Set<Planet> orbiters = new HashSet<>();

        public Planet(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Planet getParent() {
            return parent;
        }

        public void setParent(Planet parent) {
            this.parent = parent;
        }

        public Set<Planet> getOrbiters() {
            return orbiters;
        }

        public void addOrbiter(Planet orbiter) {
            this.orbiters.add(orbiter);
        }

        public int getNumberOfOrbits() {
            int orbits = 0;
            Planet current = this.parent;
            while (current != null) {
                orbits++;
                current = current.parent;
            }
            return orbits;
        }

        public List<String> getOrbitChain() {
            List<String> orbitChain = new ArrayList<>();
            Planet current = this.parent;
            while (current != null) {
                orbitChain.add(0, current.getName());
                current = current.getParent();
            }
            return orbitChain;
        }
    }
}
