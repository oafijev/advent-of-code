package com.oafijev.advent.y2017.d09;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NineOne {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        char[] chars = input.get(0).toCharArray();
        boolean ignoreNextChar = false;
        boolean inGarbage = false;
        StringBuffer output = new StringBuffer();
        int garbageCount = 0;

        for (char c : chars) {
            if (ignoreNextChar) {
                ignoreNextChar = false;
                continue;
            }
            if (inGarbage && c != '>' && c != '!') {
                garbageCount++;
                continue;
            }
            if (c == '{') {
                output.append("{");
            } else if (c == '<') {
                if (inGarbage) {
                    throw new IllegalStateException("Already in garbage!");
                }
                inGarbage = true;
            } else if (c == '!') {
                if (!inGarbage) {
                    throw new IllegalStateException("Ignoring while not in garbage!");
                }
                ignoreNextChar = true;
            } else if (c == '>') {
                if (!inGarbage) {
                    throw new IllegalStateException("Trying to close garbage when not in garbage!");
                } else {
                    inGarbage = false;
                }
            } else if (c == '}') {
                output.append("}");
            }
        }
        System.out.println(chars);
        System.out.println(output.toString());

        int depth = 0;
        int score = 0;
        for (char c : output.toString().toCharArray()) {
            if (c == '{') {
                depth++;
                score += depth;
            } else if (c == '}') {
                depth--;
            }
        }
        System.out.println(score);
        System.out.println(garbageCount);
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d09/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
