package com.oafijev.advent.y2017.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EightOne {
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Map<String, Long> registers = new HashMap<String, Long>();

        Long max = Long.MIN_VALUE;
        int count = 0;
        for (String line : input) {
            count++;
            String[] split = line.split(" ");
            String name = split[0];
            String operation = split[1];
            Long value = Long.parseLong(split[2]);
            String checkName = split[4];
            String operator = split[5];
            Long checkValue = Long.parseLong(split[6]);

            if (!registers.containsKey(checkName)) {
                registers.put(checkName, 0l);
            }
            if (!registers.containsKey(name)) {
                registers.put(name, 0l);
            }
            if (checkCondition(registers.get(checkName), operator, checkValue)) {
                System.out.println(String.format("executed conditions for %s: %s %s %s", line, registers.get(checkName), operator, checkValue));
                Long oldValue = registers.get(name);
                if (operation.equals("dec")) {
                    registers.put(name, oldValue - value);
                } else if (operation.equals("inc")) {
                    registers.put(name, oldValue + value);
                } else {
                    throw new IllegalStateException("Unknown operation " + operation);
                }
            } else {
                System.out.println(String.format("skip conditions for %s: %s %s %s", line, registers.get(checkName), operator, checkValue));
            }
        }

        for (Long value : registers.values()) {
            if (value > max) {
                max = value;
            }
        }
        System.out.println("Maximum value = " + max);
        System.out.println(registers);
        System.out.println(count);
    }

    private static boolean checkCondition(Long value, String operator, Long checkValue) {
        if (operator.equals("<")) {
            return value < checkValue;
        } else if (operator.equals("<=")) {
            return value <= checkValue;
        } else if (operator.equals("==")) {
            return value.equals(checkValue);
        } else if (operator.equals(">=")) {
            return value >= checkValue;
        } else if (operator.equals(">")) {
            return value > checkValue;
        } else if (operator.equals("!=")) {
            return !value.equals(checkValue);
        }
        throw new IllegalStateException("Unknown operator " + operator);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }
}
