package com.oafijev.advent.y2017.d08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.ToIntBiFunction;

public class Day8 {

    private static Map<String, BiPredicate<Integer, Integer>> comparisons = new HashMap<>();

    static {
        comparisons.put("==", (x, y) -> x.equals(y));
        comparisons.put("!=", (x, y) -> !x.equals(y));
        comparisons.put("<", (x, y) -> x < y);
        comparisons.put(">", (x, y) -> x > y);
        comparisons.put("<=", (x, y) -> x <= y);
        comparisons.put(">=", (x, y) -> x >= y);

    }

    private static Map<String, ToIntBiFunction<Integer, Integer>> commands = new HashMap<>();

    static {
        commands.put("inc", (x, y) -> x + y);
        commands.put("dec", (x, y) -> x - y);
    }

    public static void main(String[] args) throws IOException {
        List<String> inputList = readInput();
        Map<String, Integer> registers = new HashMap<>();
        int highest = 0;

        for (String line : inputList) {
            String[] each = line.split(" ");
            String name = each[0];
            String command = each[1];
            int amount = Integer.parseInt(each[2]);
            String testReg = each[4];
            String comp = each[5];
            int testAmt = Integer.parseInt(each[6]);

            registers.putIfAbsent(name, 0);

            if (comparisons.get(comp).test(registers.getOrDefault(testReg, 0), testAmt)) {

                int current = registers.get(name);
                registers.put(name, commands.get(command).applyAsInt(current, amount));

                if (registers.get(name) > highest) {
                    highest = registers.get(name);
                }
            }
        }

        System.out.println("Part 1: " + Collections.max(registers.values()));
        System.out.println("Part 2: " + highest);


    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d08/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }
}
