package com.oafijev.advent.y2017.d22;

public enum Direction {
    NORTH(-1, 0),
    EAST(0, 1),
    SOUTH(1, 0),
    WEST(0, -1);

    private int rowOffset;
    private int colOffset;

    private Direction(int rowOffset, int colOffset) {
        this.rowOffset = rowOffset;
        this.colOffset = colOffset;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public int getColOffset() {
        return colOffset;
    }

    public Direction getInfectedDirection() {
        if (this == NORTH) {
            return EAST;
        } else if (this == EAST) {
            return SOUTH;
        } else if (this == SOUTH) {
            return WEST;
        } else if (this == WEST) {
            return NORTH;
        }
        throw new IllegalStateException();
    }

    public Direction getCleanDirection() {
        if (this == NORTH) {
            return WEST;
        } else if (this == EAST) {
            return NORTH;
        } else if (this == SOUTH) {
            return EAST;
        } else if (this == WEST) {
            return SOUTH;
        }
        throw new IllegalStateException();
    }

    public Direction getReverseDirection() {
        if (this == NORTH) {
            return SOUTH;
        } else if (this == EAST) {
            return WEST;
        } else if (this == SOUTH) {
            return NORTH;
        } else if (this == WEST) {
            return EAST;
        }
        throw new IllegalStateException();
    }
}
