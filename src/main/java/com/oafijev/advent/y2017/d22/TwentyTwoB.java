package com.oafijev.advent.y2017.d22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TwentyTwoB {

    private static int PADDING = 50;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int size = input.size();

        char[][] map = new char[size * (1 + 2 * PADDING)][size * (1 + 2 * PADDING)];

        for (int i = PADDING * size; i < size + PADDING * size; i++) {
            for (int j = PADDING * size; j < size + PADDING * size; j++) {
                map[i][j] = input.get(i - PADDING * size).charAt(j - PADDING * size);
            }
        }

        int y = PADDING * size + size / 2;
        int x = PADDING * size + size / 2;
        Direction direction = Direction.NORTH;
        int bursts = 0;
        int infections = 0;

        do {
            if (map[y][x] == '#') {
                direction = direction.getInfectedDirection();
                map[y][x] = 'F';
            } else if (map[y][x] == 'F') {
                direction = direction.getReverseDirection();
                map[y][x] = '.';
            } else if (map[y][x] == '.' || map[y][x] == 0) {
                direction = direction.getCleanDirection();
                map[y][x] = 'W';
            } else if (map[y][x] == 'W') {
                map[y][x] = '#';
                infections++;
            }

            x += direction.getColOffset();
            y += direction.getRowOffset();
            bursts++;
        } while (bursts < 10000000);

        System.out.println(x);
        System.out.println(y);
        System.out.println(direction);
        System.out.println(bursts);
        System.out.println(infections);
        System.out.println(map);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d22/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
