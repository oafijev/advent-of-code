package com.oafijev.advent.y2017.d21;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwentyOneA {

    private static final int ROUNDS = 18;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Map<String, String> rules = new HashMap<String, String>();
        char[][] state = new char[][]{".#.".toCharArray(), "..#".toCharArray(), "###".toCharArray()};

        for (String line : input) {
            String[] split = line.split(" => ");
            if (split[0].length() == 5) {
                rules.put(split[0], split[1]);
                // 2x2 to 3x3
                char[] rule = split[0].toCharArray();
                char[] chars = ("" + rule[0] + rule[1] + rule[3] + rule[4]).toCharArray();
                rules.put("" + chars[2] + chars[0] + "/" + chars[3] + chars[1], split[1]);
                rules.put("" + chars[3] + chars[2] + "/" + chars[1] + chars[0], split[1]);
                rules.put("" + chars[1] + chars[3] + "/" + chars[0] + chars[2], split[1]);
                rules.put("" + chars[1] + chars[0] + "/" + chars[3] + chars[2], split[1]);
                rules.put("" + chars[2] + chars[3] + "/" + chars[0] + chars[1], split[1]);
                rules.put("" + chars[2] + chars[0] + "/" + chars[3] + chars[1], split[1]);
                rules.put("" + chars[3] + chars[1] + "/" + chars[2] + chars[0], split[1]);
                rules.put("" + chars[2] + chars[3] + "/" + chars[0] + chars[1], split[1]);
                rules.put("" + chars[1] + chars[0] + "/" + chars[3] + chars[2], split[1]);
                rules.put("" + chars[3] + chars[1] + "/" + chars[2] + chars[0], split[1]);
                rules.put("" + chars[0] + chars[2] + "/" + chars[1] + chars[3], split[1]);
            } else if (split[0].length() == 11) {
                char[] rule = split[0].toCharArray();
                char[] c = ("" + rule[0] + rule[1] + rule[2] + rule[4] + rule[5] + rule[6] + rule[8] + rule[9] + rule[10]).toCharArray();
                rules.put(split[0], split[1]);
                rules.put("" + c[6] + c[7] + c[8] + "/" + c[3] + c[4] + c[5] + "/" + c[0] + c[1] + c[2], split[1]);
                rules.put("" + c[2] + c[1] + c[0] + "/" + c[5] + c[4] + c[3] + "/" + c[8] + c[7] + c[6], split[1]);
                rules.put("" + c[6] + c[3] + c[0] + "/" + c[7] + c[4] + c[1] + "/" + c[8] + c[5] + c[2], split[1]);
                rules.put("" + c[8] + c[5] + c[2] + "/" + c[7] + c[4] + c[1] + "/" + c[6] + c[3] + c[0], split[1]);
                rules.put("" + c[0] + c[3] + c[6] + "/" + c[1] + c[4] + c[7] + "/" + c[2] + c[5] + c[8], split[1]);
                rules.put("" + c[8] + c[7] + c[6] + "/" + c[5] + c[4] + c[3] + "/" + c[2] + c[1] + c[0], split[1]);
                rules.put("" + c[2] + c[1] + c[0] + "/" + c[5] + c[4] + c[3] + "/" + c[8] + c[7] + c[6], split[1]);
                rules.put("" + c[6] + c[7] + c[8] + "/" + c[3] + c[4] + c[5] + "/" + c[0] + c[1] + c[2], split[1]);
                rules.put("" + c[2] + c[5] + c[8] + "/" + c[1] + c[4] + c[7] + "/" + c[0] + c[3] + c[6], split[1]);
                rules.put("" + c[0] + c[3] + c[6] + "/" + c[1] + c[4] + c[7] + "/" + c[2] + c[5] + c[8], split[1]);
                rules.put("" + c[8] + c[5] + c[2] + "/" + c[7] + c[4] + c[1] + "/" + c[6] + c[3] + c[0], split[1]);
            } else {
                throw new IllegalStateException();
            }
        }

        for (int round = 0; round < ROUNDS; round++) {
            if (state.length % 2 == 0) {
                char[][] newState = new char[3 * state.length / 2][3 * state.length / 2];
                for (int i = 0; i < state.length; i = i + 2) {
                    for (int j = 0; j < state.length; j = j + 2) {
                        String rule = "" + state[i][j] + state[i][j + 1] + "/" + state[i + 1][j] + state[i + 1][j + 1];
                        char[] result = rules.get(rule).toCharArray();
                        int newI = 3 * i / 2;
                        int newJ = 3 * j / 2;
                        newState[newI][newJ] = result[0];
                        newState[newI][newJ + 1] = result[1];
                        newState[newI][newJ + 2] = result[2];
                        newState[newI + 1][newJ] = result[4];
                        newState[newI + 1][newJ + 1] = result[5];
                        newState[newI + 1][newJ + 2] = result[6];
                        newState[newI + 2][newJ] = result[8];
                        newState[newI + 2][newJ + 1] = result[9];
                        newState[newI + 2][newJ + 2] = result[10];
                    }
                }
                state = newState;
            } else { // state.length % 3 == 0
                char[][] newState = new char[4 * state.length / 3][4 * state.length / 3];
                for (int i = 0; i < state.length; i = i + 3) {
                    for (int j = 0; j < state.length; j = j + 3) {
                        String rule = "" + state[i][j] + state[i][j + 1] + state[i][j + 2] + "/" + state[i + 1][j] + state[i + 1][j + 1] + state[i + 1][j + 2] + "/" + state[i + 2][j] + state[i + 2][j + 1] + state[i + 2][j + 2];
                        char[] result = rules.get(rule).toCharArray();
                        int newI = 4 * i / 3;
                        int newJ = 4 * j / 3;
                        newState[newI][newJ] = result[0];
                        newState[newI][newJ + 1] = result[1];
                        newState[newI][newJ + 2] = result[2];
                        newState[newI][newJ + 3] = result[3];
                        newState[newI + 1][newJ] = result[5];
                        newState[newI + 1][newJ + 1] = result[6];
                        newState[newI + 1][newJ + 2] = result[7];
                        newState[newI + 1][newJ + 3] = result[8];
                        newState[newI + 2][newJ] = result[10];
                        newState[newI + 2][newJ + 1] = result[11];
                        newState[newI + 2][newJ + 2] = result[12];
                        newState[newI + 2][newJ + 3] = result[13];
                        newState[newI + 3][newJ] = result[15];
                        newState[newI + 3][newJ + 1] = result[16];
                        newState[newI + 3][newJ + 2] = result[17];
                        newState[newI + 3][newJ + 3] = result[18];
                    }
                }
                state = newState;
            }
        }

        int count = 0;
        for (char[] row : state) {
            for (char element : row) {
                if (element == '#') {
                    count++;
                }
            }
        }

        System.out.println(count);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d21/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
