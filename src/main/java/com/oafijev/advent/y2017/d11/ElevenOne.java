package com.oafijev.advent.y2017.d11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ElevenOne {

    private static final int COUNT = 256;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String[] split = input.get(0).split(",");
        Integer[] totals = new Integer[]{0, 0, 0, 0, 0, 0}; // ne n nw sw s se
        Integer x = 0; // current coords in sqrt(3)'s
        Integer y = 0; // this is in number of 0.5's
        Integer maxX = 0;
        Integer maxY = 0;
        Integer maxSteps = 0;

        for (String direction : split) {
            if ("ne".equals(direction)) {
                x += 1;
                y += 1;
                totals[0]++;
            } else if ("n".equals(direction)) {
                y += 2;
                totals[1]++;
            } else if ("nw".equals(direction)) {
                x -= 1;
                y += 1;
                totals[2]++;
            } else if ("sw".equals(direction)) {
                x -= 1;
                y -= 1;
                totals[3]++;
            } else if ("s".equals(direction)) {
                y -= 2;
                totals[4]++;
            } else if ("se".equals(direction)) {
                x += 1;
                y -= 1;
                totals[5]++;
            }

            Integer distance = (y - x) / 2 + x;
            if (distance > maxSteps) {
                maxX = x;
                maxY = y;
                maxSteps = distance;
            }
        }

        System.out.println(x + " " + y);
        System.out.println((y - x) / 2 + x);
        System.out.println(totals);
        System.out.println(maxSteps);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d11/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
