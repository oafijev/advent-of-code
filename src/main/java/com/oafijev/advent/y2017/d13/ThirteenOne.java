package com.oafijev.advent.y2017.d13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThirteenOne {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Integer severity = 0;

        for (String line : input) {
            String[] split = line.split(": ");
            Integer depth = Integer.parseInt(split[0]);
            Integer range = Integer.parseInt(split[1]);

            if ((depth % ((range - 1) * 2)) == 0) {
                severity += depth * range;
            }
        }

        System.out.println("severity: " + severity);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d13/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
