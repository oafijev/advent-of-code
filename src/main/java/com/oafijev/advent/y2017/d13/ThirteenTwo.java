package com.oafijev.advent.y2017.d13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThirteenTwo {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Integer delay = 0;

        while (true) {
            boolean failed = false;
            for (String line : input) {
                String[] split = line.split(": ");
                Integer depth = Integer.parseInt(split[0]);
                Integer range = Integer.parseInt(split[1]);

                if (((delay + depth) % ((range - 1) * 2)) == 0) {
                    failed = true;
                    break;
                }
            }

            if (failed) {
                delay++;
                continue;
            } else {
                System.out.println("delay: " + delay);
                break;
            }
        }


    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d13/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
