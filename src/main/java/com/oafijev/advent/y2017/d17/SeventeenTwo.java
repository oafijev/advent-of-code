package com.oafijev.advent.y2017.d17;

public class SeventeenTwo {

    private static final Integer STEPS = 304;

    public static void main(String[] args) throws Exception {
        Node current = new Node(0);
        current.setNext(current);
        current.setPrevious(current);
        Integer count = 1;

        do {
            for (int i = 0; i < STEPS; i++) {
                current = current.getNext();
            }
            if (current.getValue().equals(0)) {
                System.out.println(count);
            }
            Node newNode = new Node(count);
            newNode.setNext(current.getNext());
            current.getNext().setPrevious(newNode);
            current.setNext(newNode);
            newNode.setPrevious(current);
            current = newNode;
            count++;
        } while (count <= 50000000);

        while (current.getValue() != 0) {
            current = current.getNext();
        }
        System.out.println(current.getNext());
    }
}
