package com.oafijev.advent.y2017.d19;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.oafijev.advent.y2017.d19.Direction.SOUTH;

public class NineteenOne {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int col = 0;
        int row = 0;
        Direction direction = SOUTH;
        int steps = 0;

        // find start, assume it's on first row and is a "|"
        for (; col < input.get(0).length(); col++) {
            if (input.get(0).charAt(col) == '|') {
                break;
            }
        }

        // go
        do {
//			System.out.println("row, col = " + row + ", " + col);
            char spot = input.get(row).charAt(col);
            if (spot == direction.getDefaultChar()) {
                row += direction.getRowOffset();
                col += direction.getColOffset();
                continue;
            } else if (spot == '+') {
                Direction[] turnDirections = direction.getTurnDirections();
                int checkTurnRow = row + turnDirections[0].getRowOffset();
                int checkTurnCol = col + turnDirections[0].getColOffset();
                if (input.get(checkTurnRow).charAt(checkTurnCol) != ' ') {
                    direction = turnDirections[0];
                } else if (input.get(checkTurnRow).charAt(checkTurnCol) == ' ') {
                    direction = turnDirections[1];
                }
            } else if (spot >= 'A' && spot <= 'z') {
                System.out.print(spot);
            } else if (spot != ' ') {
                //skip - the other direction crossing over
            } else if (spot == ' ') {
                break;
            }
            row += direction.getRowOffset();
            col += direction.getColOffset();
            steps++;
        } while (true);

        System.out.println();
        System.out.println(steps);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d19/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
