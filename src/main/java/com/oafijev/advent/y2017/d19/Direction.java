package com.oafijev.advent.y2017.d19;

public enum Direction {
    NORTH('|', -1, 0),
    EAST('-', 0, 1),
    SOUTH('|', 1, 0),
    WEST('-', 0, -1);

    private char defaultChar;
    private int rowOffset;
    private int colOffset;

    private Direction(char defaultChar, int rowOffset, int colOffset) {
        this.defaultChar = defaultChar;
        this.rowOffset = rowOffset;
        this.colOffset = colOffset;
    }

    public char getDefaultChar() {
        return defaultChar;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public int getColOffset() {
        return colOffset;
    }

    public Direction[] getTurnDirections() {
        if (this == NORTH || this == SOUTH) {
            return new Direction[]{EAST, WEST};
        } else if (this == EAST || this == WEST) {
            return new Direction[]{NORTH, SOUTH};
        }
        throw new IllegalStateException();
    }
}
