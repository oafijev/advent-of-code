package com.oafijev.advent.y2017.d23;

public class TwentyThreeB {
    public static void main(String[] args) {
        int count = 0;

        for (long i = 106500l; i <= 123500l; i += 17) {
            count += isPrime(i) ? 0 : 1;
        }

        System.out.println(count);
    }

    static boolean isPrime(Long n) {
        // check if n is a multiple of 2
        if (n % 2 == 0) {
            return false;
        }

        // if not, then just check the odds
        for (long i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
