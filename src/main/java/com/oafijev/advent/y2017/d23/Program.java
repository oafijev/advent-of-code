package com.oafijev.advent.y2017.d23;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Program {

    private List<String> input;
    private Integer pointer = 0;
    private Map<String, Long> registers = new HashMap<String, Long>();
    private List<Long> queue = new ArrayList<Long>();
    boolean waiting = false;
    private Long programId;
    private int sends = 0;
    private int muls = 0;

    public Program(Long programId, List<String> input) {
        this.programId = programId;
        registers.put("p", programId);
        this.input = input;
    }

    public void run() {
        String instruction = input.get(pointer);
        pointer++;
//		System.out.println("Executing on " + programId + ": " + instruction);

        String[] split = instruction.split(" ");
        if (split[0].equals("set")) {
            setRegister(split[1], getValue(split[2]));
        } else if (split[0].equals("sub")) {
            setRegister(split[1], getValue(split[1]) - getValue(split[2]));
        } else if (split[0].equals("mul")) {
            setRegister(split[1], getValue(split[1]) * getValue(split[2]));
            muls++;
        } else if (split[0].equals("nop")) {

        } else if (split[0].equals("jnz")) {
            Long offset = getValue(split[2]);
            Long base = getValue(split[1]);
            if (offset == -13) {
                System.out.println(registers);
            } else if (offset == -23) {
                System.out.println(registers);
            }
            if (base.intValue() != 0) {
                pointer = pointer.intValue() + offset.intValue() - 1;
            }
        } else {
            throw new IllegalArgumentException("Command not implemented: " + split[0]);
        }
    }

    public boolean isTerminated() {
        return pointer < 0 || pointer >= input.size();
    }

    public void setRegister(String input, Long value) {
        registers.put(input, value);
//		System.out.println(registers);
    }

    public Long getRegister(String input) {
        return registers.get(input);
    }

    private Long getValue(String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
        }
        if (!registers.containsKey(input)) {
            registers.put(input, 0l);
        }
        return registers.get(input);
    }

    public void queue(Long value) {
        queue.add(0, value);
    }

    public boolean isWaiting() {
        return this.waiting;
    }

    public Integer getSummary() {
        return sends;
    }

    public int getMuls() {
        return muls;
    }

}
