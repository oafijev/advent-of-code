package com.oafijev.advent.y2017.d23;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TwentyThreeA {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Program program = new Program(0l, input);

        program.setRegister("a", 1l);

        while (!program.isTerminated()) {
            program.run();
        }

        System.out.println(program.getMuls());
        System.out.println(program.getRegister("h"));
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d23/input.orig.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
