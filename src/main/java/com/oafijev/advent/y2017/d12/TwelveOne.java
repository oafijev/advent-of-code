package com.oafijev.advent.y2017.d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TwelveOne {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Map<Integer, Node> nodes = new HashMap<Integer, Node>();

        for (String line : input) {
            String[] split = line.split(" <-> ");

            Integer value = Integer.parseInt(split[0]);
            if (!nodes.containsKey(value)) {
                nodes.put(value, new Node(value));
            }

            String[] children = split[1].split(", ");
            for (String child : children) {
                Integer childValue = Integer.parseInt(child);

                if (!nodes.containsKey(childValue)) {
                    nodes.put(childValue, new Node(childValue));
                }

                nodes.get(value).addChild(nodes.get(childValue));
                nodes.get(childValue).addChild(nodes.get(value));
            }
        }

        int count = 0;

        Set<Node> containingZero = visitSetContaining(0, nodes, new HashSet<Node>());
        System.out.println("this many items are in the group containing zero: " + containingZero.size());

        while (!nodes.isEmpty()) {
            Integer value = nodes.values().iterator().next().getValue();
            Set<Node> groupContainingValue = visitSetContaining(value, nodes, new HashSet<Node>());
            for (Node node : groupContainingValue) {
                nodes.remove(node.getValue());
            }
            count++;
        }

        System.out.println("here are the number of groups: " + count);
    }

    private static Set<Node> visitSetContaining(Integer value, Map<Integer, Node> nodes, HashSet<Node> visited) {
        for (Node child : nodes.get(value).getChildren()) {
            if (!visited.contains(child)) {
                visited.add(child);
                visitSetContaining(child.getValue(), nodes, visited);
            }
        }
        return visited;
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d12/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
