package com.oafijev.advent.y2017.d12;

import java.util.HashSet;
import java.util.Set;

public class Node {
    private Set<Node> children = new HashSet<Node>();
    private Integer value;

    public Node(Integer value) {
        this.value = value;
    }

    public Set<Node> getChildren() {
        return children;
    }

    public void addChild(Node child) {
        this.children.add(child);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}

