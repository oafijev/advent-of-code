package com.oafijev.advent.y2017.d16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SixteenOne {

    private static final long ROUNDS = 10000000000l;
    private static Integer SIZE = 16;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        String[] split = line.split(",");
        char[] programs = new char[SIZE];
        Set<char[]> answers = new HashSet<char[]>();

        for (int i = 0; i < SIZE; i++) {
            programs[i] = ((char) (((int) 'a') + i));
        }

        for (long i = 0; i < 10000000000l; i++) {
            for (String command : split) {
                char operation = command.charAt(0);
                String arguments = command.substring(1);
                switch (operation) {
                    case 's':
                        programs = rotate(programs, arguments);
                        break;
                    case 'x':
                        programs = swap(programs, arguments);
                        break;
                    case 'p':
                        programs = swapPartners(programs, arguments);
                        break;
                }
            }
            answers.add(programs);
            System.out.print(i + ": ");
            System.out.println(programs);
            if (isOrderAlphabetical(programs)) {
                System.out.println("cycle after " + i);
                System.out.println("Find row " + ((ROUNDS - 1) % (i + 1)));
                break;
            }
        }

        System.out.println(programs);
    }

    private static boolean isOrderAlphabetical(char[] programs) {
        for (int i = 0; i < programs.length - 1; i++) {
            if (programs[i] > programs[i + 1]) {
                return false;
            }
        }
        return true;
    }

    private static char[] swapPartners(char[] programs, String arguments) {
        String[] partners = arguments.split("/");
        Integer a = -1;
        Integer b = -1;
        int count = 0;
        for (char program : programs) {
            if (program == partners[0].toCharArray()[0]) {
                a = count;
            }
            if (program == partners[1].toCharArray()[0]) {
                b = count;
            }
            count++;
        }

        char tmp = programs[a];
        programs[a] = programs[b];
        programs[b] = tmp;

        return programs;
    }

    private static char[] swap(char[] programs, String arguments) {
        String[] partners = arguments.split("/");
        Integer a = Integer.parseInt(partners[0]);
        Integer b = Integer.parseInt(partners[1]);

        char tmp = programs[a];
        programs[a] = programs[b];
        programs[b] = tmp;

        return programs;
    }

    private static char[] rotate(char[] programs, String arguments) {
        Integer amountToRotate = Integer.parseInt(arguments);

        char[] result = new char[programs.length];
        System.arraycopy(programs, programs.length - amountToRotate, result, 0, amountToRotate);
        System.arraycopy(programs, 0, result, amountToRotate, programs.length - amountToRotate);

        programs = result;

        return result; // Case 2: method returns the rotated array
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d16/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
