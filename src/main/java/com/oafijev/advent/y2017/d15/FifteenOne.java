package com.oafijev.advent.y2017.d15;

public class FifteenOne {

    private static Long FACTOR_A = 16807L;
    private static Long FACTOR_B = 48271L;
    private static Long MOD = 2147483647L;

    public static void main(String[] args) throws Exception {
//		List<String> input = readInput();
        Integer severity = 0;

//		Long a = 65l;
//		Long b = 8921l;
        Long a = 591l;
        Long b = 393l;

        Long rounds = 1l;
        Long count = 0l;
        do {
            a = a * FACTOR_A % MOD;
            b = b * FACTOR_B % MOD;

//			System.out.println(a + " " + Long.toBinaryString(a));
//			System.out.println(b + " " + Long.toBinaryString(b));
//			System.out.println("======");
            if ((a & 0xffff) == (b & 0xffff)) {
                count++;
            }

        } while (rounds++ < 40000000l);

        System.out.println("severity: " + count);
    }

}
