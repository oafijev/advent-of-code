package com.oafijev.advent.y2017.d18;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProgramTwo {

    private List<String> input;
    private Integer pointer = 0;
    private Map<String, Long> registers = new HashMap<String, Long>();
    private List<Long> queue = new ArrayList<Long>();
    boolean waiting = false;
    private Long programId;
    private int sends = 0;

    public ProgramTwo(Long programId, List<String> input) {
        this.programId = programId;
        registers.put("p", programId);
        this.input = input;
    }

    public Long run() {
        String instruction = input.get(pointer);
        pointer++;
//		System.out.println("Executing on " + programId + ": " + instruction);

        String[] split = instruction.split(" ");
        if (split[0].equals("snd")) {
            System.out.println(programId + " sending " + getValue(split[1]));
            sends++;
            return getValue(split[1]);
        } else if (split[0].equals("set")) {
            setRegister(split[1], getValue(split[2]));
        } else if (split[0].equals("add")) {
            setRegister(split[1], getValue(split[1]) + getValue(split[2]));
        } else if (split[0].equals("mul")) {
            setRegister(split[1], getValue(split[1]) * getValue(split[2]));
        } else if (split[0].equals("mod")) {
            setRegister(split[1], getValue(split[1]) % getValue(split[2]));
        } else if (split[0].equals("rcv")) {
            if (!queue.isEmpty()) {
                waiting = false;
                Long value = queue.remove(queue.size() - 1);
                setRegister(split[1], value);
                System.out.println(programId + " receiving " + value);
            } else {
//				System.out.println(programId + " waiting");
                pointer--;
                waiting = true;
            }
        } else if (split[0].equals("jgz")) {
            Long offset = getValue(split[2]);
            Long base = getValue(split[1]);
            if (base.intValue() > 0) {
                pointer = pointer.intValue() + offset.intValue() - 1;
            }
        }
        return null;
    }

    public boolean isTerminated() {
        return pointer < 0 || pointer >= input.size();
    }

    private void setRegister(String input, Long value) {
        registers.put(input, value);
    }

    private Long getValue(String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
        }
        if (!registers.containsKey(input)) {
            registers.put(input, 0l);
        }
        return registers.get(input);
    }

    public void queue(Long value) {
        queue.add(0, value);
    }

    public boolean isWaiting() {
        return this.waiting;
    }

    public Integer getSummary() {
        return sends;
    }
}
