package com.oafijev.advent.y2017.d18;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EighteenTwo {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        ProgramTwo program1 = new ProgramTwo(0l, input);
        ProgramTwo program2 = new ProgramTwo(1l, input);

        while ((!program1.isTerminated() || !program2.isTerminated())) {
            Long program1Send = program1.run();
            if (program1Send != null) {
                program2.queue(program1Send);
            }
            Long program2Send = program2.run();
            if (program2Send != null) {
                program1.queue(program2Send);
            }

            if (program1.isWaiting() && program2.isWaiting()) {
                System.out.println("Deadlocked!");
                break;
            }
        }

        System.out.println("1: " + program2.getSummary());
        System.out.println("0: " + program1.getSummary());
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d18/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
