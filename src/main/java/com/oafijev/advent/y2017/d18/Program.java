package com.oafijev.advent.y2017.d18;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Program {

    private List<String> input;
    private Integer pointer = 0;
    private Map<String, Long> registers = new HashMap<String, Long>();
    private Long lastFrequency = null;

    public Program(List<String> input) {
        this.input = input;
    }

    public void run() {
        String instruction = input.get(pointer);
        System.out.println(instruction);

        String[] split = instruction.split(" ");
        if (split[0].equals("snd")) {
            lastFrequency = getValue(split[1]);
        } else if (split[0].equals("set")) {
            setRegister(split[1], getValue(split[2]));
        } else if (split[0].equals("add")) {
            setRegister(split[1], getValue(split[1]) + getValue(split[2]));
        } else if (split[0].equals("mul")) {
            setRegister(split[1], getValue(split[1]) * getValue(split[2]));
        } else if (split[0].equals("mod")) {
            setRegister(split[1], getValue(split[1]) % getValue(split[2]));
        } else if (split[0].equals("rcv")) {
            if (!getValue(split[1]).equals(0l)) {
                System.out.println(getValue(split[1]) + ": " + lastFrequency);
                return;
            }
        } else if (split[0].equals("jgz")) {
            Long offset = getValue(split[2]);
            Long base = getValue(split[1]);
            if (base.intValue() > 0) {
                pointer = pointer.intValue() + offset.intValue() - 1;
            }
        }
        pointer++;
    }

    public boolean isTerminated() {
        return pointer < 0 || pointer >= input.size();
    }

    private void setRegister(String input, Long value) {
        registers.put(input, value);
    }

    private Long getValue(String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
        }
        if (!registers.containsKey(input)) {
            registers.put(input, 0l);
        }
        return registers.get(input);
    }

}
