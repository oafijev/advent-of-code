package com.oafijev.advent.y2017.d18;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EighteenOne {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Program program = new Program(input);

        while (!program.isTerminated()) {
            program.run();
        }

        System.out.println(input);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d18/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
