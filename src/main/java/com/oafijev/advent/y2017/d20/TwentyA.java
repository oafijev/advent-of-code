package com.oafijev.advent.y2017.d20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TwentyA {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Integer count = 0;
        Integer closest = 0;
        Long closestAcceleration = Long.MAX_VALUE;

        for (String line : input) {
            String[] split = line.substring(3, line.length() - 1).split(">, v=<");
            String[] position = split[0].split(",");
            String[] velocity = split[1].split(">, a=<")[0].split(",");
            String[] acceleration = split[1].split(">, a=<")[1].split(",");

            Long manhattanAcceleration = Math.abs(Long.parseLong(acceleration[0])) + Math.abs(Long.parseLong(acceleration[1])) + Math.abs(Long.parseLong(acceleration[2]));
            if (Math.abs(manhattanAcceleration) < Math.abs(closestAcceleration)) {
                closest = count;
                closestAcceleration = manhattanAcceleration;
            }
            count++;
        }

        System.out.println(closest);
        System.out.println(closestAcceleration);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d20/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
