package com.oafijev.advent.y2017.d20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TwentyB {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        Integer count = 0;
        Set<Particle> particles = new HashSet<Particle>();
        Integer iterations = 0;
        Integer particleCount = 0;

        for (String line : input) {
            String[] split = line.substring(3, line.length() - 1).split(">, v=<");
            String[] position = split[0].split(",");
            String[] velocity = split[1].split(">, a=<")[0].split(",");
            String[] acceleration = split[1].split(">, a=<")[1].split(",");

            Particle particle = new Particle(count, position, velocity, acceleration);
            particles.add(particle);
        }

        particleCount = particles.size();

        while (true) {
            for (Particle particle : particles) {
                particle.tick();
            }

            // find duplicates
            Set<String> positionsToDelete = new HashSet<String>();
            Map<String, Particle> seenPositions = new HashMap<String, Particle>();
            Set<Particle> particlesToDelete = new HashSet<Particle>();
            for (Particle particle : particles) {
                String position = particle.getPosition();
                if (positionsToDelete.contains(position)) {
                    particlesToDelete.add(particle);
                } else if (seenPositions.containsKey(position)) {
                    positionsToDelete.add(position);
                    particlesToDelete.add(particle);
                    particlesToDelete.add(seenPositions.get(position));
                } else {
                    seenPositions.put(position, particle);
                }
            }

            particles.removeAll(particlesToDelete);

            particleCount = particles.size();
            if (count++ % 10 == 0) {
                System.out.println(particleCount);
            }
        }
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d20/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
