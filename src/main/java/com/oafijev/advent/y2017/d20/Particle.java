package com.oafijev.advent.y2017.d20;

public class Particle {
    private Integer id;
    private Long[] position;
    private Long[] velocity;
    private Long[] acceleration;

    public Particle(Integer id, String[] position, String[] velocity, String[] acceleration) {
        this.id = id;
        this.acceleration = new Long[]{Long.parseLong(acceleration[0]), Long.parseLong(acceleration[1]), Long.parseLong(acceleration[2])};
        this.velocity = new Long[]{Long.parseLong(velocity[0]), Long.parseLong(velocity[1]), Long.parseLong(velocity[2])};
        this.position = new Long[]{Long.parseLong(position[0]), Long.parseLong(position[1]), Long.parseLong(position[2])};
    }

    public void tick() {
        velocity[0] += acceleration[0];
        velocity[1] += acceleration[1];
        velocity[2] += acceleration[2];
        position[0] += velocity[0];
        position[1] += velocity[1];
        position[2] += velocity[2];
    }

    public String getPosition() {
        return position[0] + "," + position[1] + "," + position[2];
    }
}
