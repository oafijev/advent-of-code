package com.oafijev.advent.y2017.d14;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class FourteenOne {

    private static final int ROUNDS = 64;
    private static final int COUNT = 256;
    private static final int ROWS = 128;

    public static void main(String[] args) throws Exception {
        String key = "xlqgujun";
        Integer[][] rows = new Integer[ROWS][128];

        int bitcount = 0;
        for (int i = 0; i < ROWS; i++) {
            Integer[] knothash = getKnothash(key + "-" + i);
            for (int j = 0; j < knothash.length; j++) {
                bitcount += Integer.bitCount(knothash[j]);
                rows[i][8 * j] = Integer.bitCount(knothash[j] & 128);
                rows[i][8 * j + 1] = Integer.bitCount(knothash[j] & 64);
                rows[i][8 * j + 2] = Integer.bitCount(knothash[j] & 32);
                rows[i][8 * j + 3] = Integer.bitCount(knothash[j] & 16);
                rows[i][8 * j + 4] = Integer.bitCount(knothash[j] & 8);
                rows[i][8 * j + 5] = Integer.bitCount(knothash[j] & 4);
                rows[i][8 * j + 6] = Integer.bitCount(knothash[j] & 2);
                rows[i][8 * j + 7] = Integer.bitCount(knothash[j] & 1);
            }
        }

        System.out.println("count: " + bitcount);

        int regionCount = 0;
        while (containsRegions(rows)) {
            findSomeRegionAndRemoveIt(rows);
            regionCount++;
        }

        System.out.println("regions: " + regionCount);
    }

    private static void findSomeRegionAndRemoveIt(Integer[][] rows) {
        for (int i = 0; i < rows.length; i++) {
            for (int j = 0; j < rows[i].length; j++) {
                if (rows[i][j] != 0) {
                    removeRegionAt(rows, i, j);
                    return;
                }
            }
        }
    }

    private static void removeRegionAt(Integer[][] rows, int i, int j) {
        if (i < 0 || i >= ROWS || j < 0 || j >= ROWS) {
            return;
        }
        if (rows[i][j] == 0) {
            return;
        }

        rows[i][j] = 0;
        removeRegionAt(rows, i - 1, j);
        removeRegionAt(rows, i + 1, j);
        removeRegionAt(rows, i, j - 1);
        removeRegionAt(rows, i, j + 1);
    }

    private static boolean containsRegions(Integer[][] rows) {
        for (Integer[] row : rows) {
            for (Integer element : row) {
                if (element != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Integer[] getKnothash(String input) {
        int length = input.length();
        byte[] bytes = new byte[length + 5];
//		byte[] bytes = new byte[length];
        System.arraycopy(input.getBytes(), 0, bytes, 0, length);
        bytes[length] = (byte) 17;
        bytes[length + 1] = (byte) 31;
        bytes[length + 2] = (byte) 73;
        bytes[length + 3] = (byte) 47;
        bytes[length + 4] = (byte) 23;
        int skip = 0;
        int index = 0;
        Integer[] list = new Integer[COUNT];

        for (int i = 0; i < COUNT; i++) {
            list[i] = i;
        }

        for (int i = 0; i < ROUNDS; i++) {
            for (byte oneByte : bytes) {
                Integer numberToReverse = Byte.toUnsignedInt(oneByte);
                Integer[] doubleArray = Stream.concat(Arrays.stream(list), Arrays.stream(list)).toArray(Integer[]::new);

                List<Integer> subarrayList = Arrays.asList(Arrays.copyOfRange(doubleArray, index, index + numberToReverse));
                Collections.reverse(subarrayList);
                Integer[] subarray = subarrayList.toArray(new Integer[subarrayList.size()]);

                System.arraycopy(subarray, 0, doubleArray, index, numberToReverse);
                if (index + numberToReverse > COUNT) {
                    System.arraycopy(doubleArray, COUNT, doubleArray, 0, index + numberToReverse - COUNT);
                }
                System.arraycopy(doubleArray, 0, list, 0, COUNT);
                index = (index + numberToReverse + skip) % COUNT;
                skip = skip + 1;
            }
        }

        Integer[] denseHash = new Integer[16];
        for (int i = 0; i < 16; i++) {
            denseHash[i] = 0;
            for (int j = 0; j < 16; j++) {
                denseHash[i] = denseHash[i] ^ list[16 * i + j];
            }
//			System.out.println(Integer.toHexString(denseHash[i]));
        }
        return denseHash;
    }

}
