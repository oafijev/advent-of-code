package com.oafijev.advent.y2017.d10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TenOne {

    private static final int COUNT = 256;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String[] split = input.get(0).split(",");
        int skip = 0;
        int index = 0;
        Integer[] list = new Integer[COUNT];

        for (int i = 0; i < COUNT; i++) {
            list[i] = i;
        }

        for (String splitItem : split) {
            Integer numberToReverse = Integer.parseInt(splitItem);
            Integer[] doubleArray = Stream.concat(Arrays.stream(list), Arrays.stream(list)).toArray(Integer[]::new);

            List<Integer> subarrayList = Arrays.asList(Arrays.copyOfRange(doubleArray, index, index + numberToReverse));
            Collections.reverse(subarrayList);
            Integer[] subarray = subarrayList.toArray(new Integer[subarrayList.size()]);

            System.arraycopy(subarray, 0, doubleArray, index, numberToReverse);
            if (index + numberToReverse > COUNT) {
                System.arraycopy(doubleArray, COUNT, doubleArray, 0, index + numberToReverse - COUNT);
            }
            System.arraycopy(doubleArray, 0, list, 0, COUNT);
            index = (index + numberToReverse + skip) % COUNT;
            skip = skip + 1;
        }

        System.out.println(list[0] * list[1]);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
