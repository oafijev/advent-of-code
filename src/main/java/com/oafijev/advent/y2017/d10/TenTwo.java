package com.oafijev.advent.y2017.d10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TenTwo {

    private static final int COUNT = 256;
    private static final int ROUNDS = 64;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int length = input.get(0).length();
        byte[] bytes = new byte[length + 5];
        System.arraycopy(input.get(0).getBytes(), 0, bytes, 0, length);
        bytes[length] = (byte) 17;
        bytes[length + 1] = (byte) 31;
        bytes[length + 2] = (byte) 73;
        bytes[length + 3] = (byte) 47;
        bytes[length + 4] = (byte) 23;
        int skip = 0;
        int index = 0;
        Integer[] list = new Integer[COUNT];

        for (int i = 0; i < COUNT; i++) {
            list[i] = i;
        }

        for (int i = 0; i < ROUNDS; i++) {
            for (byte oneByte : bytes) {
                Integer numberToReverse = Byte.toUnsignedInt(oneByte);
                Integer[] doubleArray = Stream.concat(Arrays.stream(list), Arrays.stream(list)).toArray(Integer[]::new);

                List<Integer> subarrayList = Arrays.asList(Arrays.copyOfRange(doubleArray, index, index + numberToReverse));
                Collections.reverse(subarrayList);
                Integer[] subarray = subarrayList.toArray(new Integer[subarrayList.size()]);

                System.arraycopy(subarray, 0, doubleArray, index, numberToReverse);
                if (index + numberToReverse > COUNT) {
                    System.arraycopy(doubleArray, COUNT, doubleArray, 0, index + numberToReverse - COUNT);
                }
                System.arraycopy(doubleArray, 0, list, 0, COUNT);
                index = (index + numberToReverse + skip) % COUNT;
                skip = skip + 1;
            }
        }

        for (int i = 0; i < 16; i++) {
            Integer denseHash = 0;
            for (int j = 0; j < 16; j++) {
                denseHash = denseHash ^ list[16 * i + j];
            }
            System.out.print(Integer.toHexString(denseHash));
        }
        System.out.println();

        for (int i = 0; i < 16; i++) ;

        System.out.println(list[0] * list[1]);
    }

    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
