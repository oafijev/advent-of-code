package com.oafijev.advent.y2017.d10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TenOneBadAttempt {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String[] split = input.get(0).split(",");
        int skip = 0;

        Node first = new Node(0);
        Node previous = first;

        for (int i = 1; i < 5; i++) {
            Node current = new Node(i);
            current.setPrevious(previous);
            previous.setNext(current);
            previous = current;
        }
        first.setPrevious(previous);
        previous.setNext(first);

        Node current = first;

        for (String splitItem : split) {
            Integer numberToReverse = Integer.parseInt(splitItem);
            Node endOfReverse = getNthNodeFromCurrent(current, numberToReverse - 1);
            if (numberToReverse != 1) {
                reverseLinks(current, endOfReverse);
            }
            current = getNthNodeFromCurrent(current, 1 + skip);
            skip = skip + 1;
            printNodes(first);
            System.out.println("skipping " + (1 + skip));
        }

        System.out.println(split);
    }

    private static void reverseLinks(Node first, Node last) {
        Node beforeFirst = first.getPrevious();
        Node afterLast = last.getNext();
        Node current = first;
        while (current != afterLast) {
            Node temp = current.getNext();
            current.setNext(current.getPrevious());
            current.setPrevious(temp);
            current = temp;
        }
        beforeFirst.setNext(last);
        last.setPrevious(beforeFirst);
        afterLast.setPrevious(first);
        first.setNext(afterLast);
    }

    private static void printNodes(Node node) {
        Node next = node.getNext();
        System.out.print(node.getValue());

        while (node != next) {
            System.out.print(", " + next.getValue());
            next = next.getNext();
        }

        System.out.println();
    }

    private static Node getNthNodeFromCurrent(Node current, Integer number) {
        for (int i = 0; i < number; i++) {
            current = current.getNext();
        }
        return current;
    }


    private static List<String> readInput() throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(new File("src/d10/input.txt")));

        String line = "";
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        reader.close();

        return result;
    }
}
