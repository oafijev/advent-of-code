package com.oafijev.advent.y2024.d02;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Day02 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int safeCount = 0;

        for (String line : input) {
            if (isSafe(line.split(" "))) {
                safeCount++;
            }
        }

        System.out.println("Part A: " + safeCount);

        safeCount = 0;

        for (String line : input) {
            String[] allNumbersForLine = line.split(" ");

            boolean safe = false;
            for (int i = 0; i < allNumbersForLine.length; i++) {
                if (isSafe(copyArrayExcluding(allNumbersForLine, i))) {
                    safe = true;
                }
            }

            if (safe) {
                safeCount++;
            }
        }


        System.out.println("Part B: " + safeCount);
    }

    private static String[] copyArrayExcluding(String[] array, int index) {
        String[] result = new String[array.length - 1];
        int i = 0;
        for (int j = 0; j < array.length; j++) {
            if (j != index) {
                result[i] = array[j];
                i++;
            }
        }
        return result;
    }

    private static boolean isSafe(String[] numbers) {
        boolean increasingSafe = true;
        boolean decreasingSafe = true;

        Integer currentNumber = null;
        for (String number : numbers) {
            Integer nextNumber = Integer.parseInt(number);
            if (currentNumber == null) {
                currentNumber = nextNumber;
            } else {
                if (increasingSafe && (nextNumber > currentNumber) && (nextNumber < currentNumber + 4)) {
                    currentNumber = nextNumber;
                    decreasingSafe = false;
                    continue;
                } else if (decreasingSafe && (nextNumber < currentNumber) && (nextNumber > currentNumber - 4)) {
                    currentNumber = nextNumber;
                    increasingSafe = false;
                    continue;
                } else {
                    increasingSafe = false;
                    decreasingSafe = false;
                    break;
                }
            }
        }
        return increasingSafe || decreasingSafe;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 2);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 2);
        return InputUtil.readCachedInput(2024, 2);
    }

}
