package com.oafijev.advent.y2024.d06;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;
import com.oafijev.advent.y2017.d19.Direction;
import com.oafijev.advent.y2024.d05.Day05;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day06 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int guardx = -1;
        int guardy = -1;
        int maxy = input.size() - 1;
        int maxx = input.get(0).length() - 1;

        char[][] grid = new char[input.size()][input.get(0).length()];
        for (int i = 0; i < input.size(); i++) {
            String line = input.get(i);
            for (int j = 0; j < line.length(); j++) {
                grid[i][j] = line.charAt(j);
                if ('^' == grid[i][j]) {
                    guardx = j;
                    guardy = i;
                }
            }
        }

        String direction = "up";
        Set<Point> visitedPoints = new HashSet<>();

        while (guardMoveInGrid(guardy, guardx, maxy, maxx)) {
            visitedPoints.add(new Point(guardy, guardx));

            if ("up".equals(direction)) {
                guardy = guardy - 1;
            } else if ("right".equals(direction)) {
                guardx = guardx + 1;
            } else if ("down".equals(direction)) {
                guardy = guardy + 1;
            } else if ("left".equals(direction)) {
                guardx = guardx - 1;
            }

            if (guardMoveInGrid(guardy, guardx, maxy, maxx)) {
                if (grid[guardy][guardx] == '#') {
                    System.out.println("collision at " + guardy + ", " + guardx);
                    if ("up".equals(direction)) {
                        direction = "right";
                        guardy = guardy + 1;
                        guardx = guardx + 1;
                    } else if ("right".equals(direction)) {
                        direction = "down";
                        guardy = guardy + 1;
                        guardx = guardx - 1;
                    } else if ("down".equals(direction)) {
                        direction = "left";
                        guardy = guardy - 1;
                        guardx = guardx - 1;
                    } else if ("left".equals(direction)) {
                        direction = "up";
                        guardy = guardy - 1;
                        guardx = guardx + 1;
                    }
                }
            }
        }


        System.out.println(guardy + ", " + guardx);

        System.out.println("Part A: " + visitedPoints.size());
        System.out.println("Part B: " + 2);
    }

    private static boolean guardMoveInGrid(int guardy, int guardx, int maxy, int maxx) {
        return (!((guardy > maxy) || (guardx > maxx) || (guardx < 0) || (guardy < 0)));
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 6);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 6);
        return InputUtil.readCachedInput(2024, 6);
    }

}
