package com.oafijev.advent.y2024.d01;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day01 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        List<Integer> first = new ArrayList<>();
        List<Integer> second = new ArrayList<>();
        Map<Integer, Integer> secondFrequency = new HashMap<>();

        for (String line : input) {
            String[] arguments = line.split("   ");
            first.add(Integer.parseInt(arguments[0]));
            int secondNumber = Integer.parseInt(arguments[1]);
            second.add(secondNumber);

            if (!secondFrequency.containsKey(secondNumber)) {
                secondFrequency.put(secondNumber, 0);
            }
            secondFrequency.put(secondNumber, secondFrequency.get(secondNumber) + 1);
        }

        List<Integer> firstSorted = first.stream().sorted().toList();
        List<Integer> secondSorted = second.stream().sorted().toList();

        int totalDistance = 0;
        for (int i = 0; i < firstSorted.size(); i++) {
            totalDistance += Math.abs(firstSorted.get(i) - secondSorted.get(i));
        }

        int similarity = 0;
        for (Integer number : first) {
            if (secondFrequency.containsKey(number)) {
                similarity += number * secondFrequency.get(number);
            }
        }

        System.out.println("Part A: " + totalDistance);
        System.out.println("Part B: " + similarity);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 1);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 1);
        return InputUtil.readCachedInput(2024, 1);
    }

}
