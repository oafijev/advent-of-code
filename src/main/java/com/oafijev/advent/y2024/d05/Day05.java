package com.oafijev.advent.y2024.d05;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class Day05 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Pair> rules = new ArrayList<>();
        List<List<String>> updates = new ArrayList<>();
        List<List<String>> incorrectUpdates = new ArrayList<>();

        for (String line : input) {
            if (line.contains("|")) {
                String[] split = line.split("\\|");
                rules.add(new Pair(split[0], split[1]));
            } else if (line.contains(",")) {
                String[] split = line.split(",");
                updates.add(Arrays.asList(split));
            }
        }

        int accumulator = 0;
        for (List<String> update : updates) {
            boolean ordered = true;
            for (Pair rule : rules) {
                if (update.contains(rule.getLeft())
                        && update.contains(rule.getRight())
                        && (update.indexOf(rule.getLeft()) >= update.indexOf(rule.getRight()))) {
                    ordered = false;
                    break;
                }
            }

            if (ordered) {
                accumulator += Integer.parseInt(update.get(((update.size() + 1) / 2) - 1));
            } else {
                incorrectUpdates.add(update);
            }
        }

        System.out.println("Part A: " + accumulator);
        accumulator = 0;

        for (List<String> update : incorrectUpdates) {
            List<Pair> applicableRules = new ArrayList<>();
            for (Pair rule : rules) {
                if (update.contains(rule.getLeft()) && update.contains(rule.getRight())) {
                    applicableRules.add(rule);
                }
            }

            Map<String,Integer> pageToFrequency = new HashMap<>();
            for (Pair rule : applicableRules) {
                String page = rule.getLeft();
                if (!pageToFrequency.containsKey(page)) {
                    pageToFrequency.put(page, 0);
                }
                pageToFrequency.put(page, pageToFrequency.get(page) + 1);
            }

            int numberOFPages = update.size();
            int indexOfOfMiddleElement = (numberOFPages - 1) / 2;

            for (Entry<String,Integer> entry : pageToFrequency.entrySet()) {
                if (entry.getValue() == indexOfOfMiddleElement) {
                    accumulator += Integer.parseInt(entry.getKey());
                }
            }
        }

        System.out.println("Part B: " + accumulator);
    }

    public static class Pair {
        String left;
        String right;

        Pair(String left, String right) {
            this.left = left;
            this.right = right;
        }

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getRight() {
            return right;
        }

        public void setRight(String right) {
            this.right = right;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 5);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 5);
        return InputUtil.readCachedInput(2024, 5);
    }

}
