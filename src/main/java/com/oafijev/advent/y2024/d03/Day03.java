package com.oafijev.advent.y2024.d03;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day03 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        String regex = "mul\\([\\d]{1,3}\\,[\\d]{1,3}\\)";
        Pattern pattern = Pattern.compile(regex);
        long accumulator = 0;

        for (String line : input) {
            Matcher matcher = pattern.matcher(line);

            while (matcher.find()) {
                String group = matcher.group();
                System.out.println("args = " + group);
                String[] split = group.substring(4, group.length() - 1).split(",");

                accumulator += Integer.parseInt(split[0]) * Integer.parseInt(split[1]);
            }
        }

        System.out.println("Part A: " + accumulator);

        regex = "mul\\([\\d]{1,3}\\,[\\d]{1,3}\\)|do\\(\\)|don\\'t\\(\\)";
        pattern = Pattern.compile(regex);
        accumulator = 0;
        boolean doing = true;

        for (String line : input) {
            Matcher matcher = pattern.matcher(line);

            while (matcher.find()) {
                String group = matcher.group();

                if ("do()".equals(group)) {
                    doing = true;
                } else if ("don't()".equals(group)) {
                    doing = false;
                } else {
                    System.out.println("args = " + group);
                    String[] split = group.substring(4, group.length() - 1).split(",");
                    if (doing) {
                        accumulator += Integer.parseInt(split[0]) * Integer.parseInt(split[1]);
                    }
                }

            }
        }
        System.out.println("Part B: " + accumulator);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 3);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 3);
        return InputUtil.readCachedInput(2024, 3);
    }

}
