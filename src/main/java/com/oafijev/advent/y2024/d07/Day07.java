package com.oafijev.advent.y2024.d07;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day07 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        System.out.println(line);
        System.out.println("Part A: " + 1);
        System.out.println("Part B: " + 2);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 7);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 7);
        return InputUtil.readCachedInput(2024, 7);
    }

}
