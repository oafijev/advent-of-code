package com.oafijev.advent.y2024.d04;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Day04 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        int maxx = line.length();
        int maxy = input.size();
        String matchWord = "XMAS";
        int matchLength = matchWord.length();

        int matches = 0;
        for (int i = 0; i < maxy; i++) {
            for (int j = 0; j < maxx; j++) {
                if (match(input, i, j, 1, 0, matchWord)) {
                    matches++;
                };
                if (match(input, i, j, -1, 0, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, 0, 1, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, 0, -1, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, 1, 1, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, -1, -1, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, 1, -1, matchWord)) {
                    matches++;
                }
                if (match(input, i, j, -1, 1, matchWord)) {
                    matches++;
                }

            }
        }

        System.out.println("Part A: " + matches);
        System.out.println("Part B: " + 2);
    }

    private static boolean match(List<String> input, int i, int j, int istep, int jstep, String matchWord) {
        boolean result = true;
        for (int k = 0; k < matchWord.length(); k++) {
            int maxx = input.get(0).length();
            int maxy = input.size();

            if (i < 0 || i >= maxy ||  j < 0 || j >= maxx) {
                result = false;
                break;
            }
            if (input.get(i).charAt(j) != matchWord.charAt(k)) {
                result = false;
            }
            i += istep;
            j += jstep;
        }

        System.out.println(result);
        return result;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2024, 4);
        InputUtil.retrieveAndSaveProblemIfNecessary(2024, 4);
        return InputUtil.readCachedInput(2024, 4);
    }

}
