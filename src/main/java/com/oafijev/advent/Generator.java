package com.oafijev.advent;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static com.oafijev.advent.common.InputUtil.padDay;

public class Generator {

    public static void main(String ... args) throws IOException {
        int year = 2024;
        for (int day = 1; day <= 25; day++) {
            if (classExists(year, day)) {
                System.out.println("Class for [" + year + ", " + day + "] already exists, skipping.");
            } else {
                System.out.println("Class for [" + year + ", " + day + "] does not exist, creating it.");

                char[] buffer = new char[10000];
                String contents;
                try (FileReader templateReader = new FileReader("src/main/java/com/oafijev/advent/template/Template.txt")) {
                    int length = templateReader.read(buffer);
                    contents = new String(buffer).substring(0, length);
                    contents = contents.replace("RAWDAY", "" + day);
                    contents = contents.replace("YEAR", "" + year);
                    contents = contents.replace("DAY", padDay(day));
                }
                String targetClassFileName = ("src/main/java/com/oafijev/advent/y" + year + "/d" + padDay(day)) + "/Day" + padDay(day) + ".java";
                File file = new File(targetClassFileName);
                System.out.println(file.getParentFile().mkdirs());
                try (FileWriter writer = new FileWriter(file)) {
                    writer.write(contents);
                }
                break;
            }
        }
    }

    private static boolean classExists(int year, int day) {
        //        File file = new File("src/main/java/com/oafijev/advent/y2017/d08/Day8.java");
        String fullyQualifiedClassName = "com.oafijev.advent.y" + year + ".d" + padDay(day) + ".Day" + padDay(day);
        try {
            Generator.class.getClassLoader().loadClass(fullyQualifiedClassName);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }

    }

}
