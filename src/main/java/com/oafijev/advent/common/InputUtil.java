package com.oafijev.advent.common;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class InputUtil {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void retrieveAndSaveInputFileIfNecessary(int year, int day) throws IOException {
        String filename = getInputFilename(year, day);
        File file = new File(filename);
        if (!file.exists()) {
            System.out.println("Input not found at [" + filename + "], retrieving and saving it.");
            file.getParentFile().mkdirs();
            URL url = new URL("https://adventofcode.com/" + year + "/day/" + day + "/input");
            URLConnection urlConnection = url.openConnection();
            urlConnection.addRequestProperty("Cookie", "session=" + getSession());
            try (BufferedReader externalReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                 BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
                String line;
                while ((line = externalReader.readLine()) != null) {
                    writer.write(line + "\n");
                }
            }
        } else {
            System.out.println("Input found at [" + filename + "], using cached version.");
        }
    }

    private static String getInputFilename(int year, int day) {
        return "src/main/resources/com/oafijev/advent/y" + year + "/d" + padDay(day) + "/input.txt";
    }

    private static String getProblemFilename(int year, int day) {
        return "src/main/resources/com/oafijev/advent/y" + year + "/d" + padDay(day) + "/Day" + padDay(day) + ".html";
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static String getSession() throws IOException {
        File file = new File("src/main/resources/session.txt");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try (FileWriter fileWriter = new FileWriter(file)) {
                System.out.println("Created session file - please put google session cookie in src/main/resources/session.txt");
                fileWriter.write("REPLACE WITH GOOGLE SESSION");
            }
        }

        try (BufferedReader fileReader = new BufferedReader(new FileReader(file))) {
            return fileReader.readLine();
        }
    }

    public static String padDay(int day) {
        return String.format("%02d", day);
    }

    public static List<String> readCachedInput(int year, int day) throws IOException {
        List<String> result = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(getInputFilename(year, day)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
        }

        return result;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void retrieveAndSaveProblemIfNecessary(int year, int day) throws IOException {
        String filename = getProblemFilename(year, day);
        File file = new File(filename);
        if (!file.exists()) {
            System.out.println("Problem not found at [" + filename + "], retrieving and saving it.");
            file.getParentFile().mkdirs();
            URL url = new URL("https://adventofcode.com/" + year + "/day/" + day);
            URLConnection urlConnection = url.openConnection();
            urlConnection.addRequestProperty("Cookie", "session=" + getSession());
            try (BufferedReader externalReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                 BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
                while (externalReader.ready()) {
                    writer.write(externalReader.readLine() + "\n");
                }
            }
        } else {
            System.out.println("Problem found at [" + filename + "], using cached version.");
        }
    }
}
