package com.oafijev.advent.y2023.d02;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day02 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int idCount = 0;

        for (String line : input) {
            int gameNumber = Integer.parseInt(line.split(":")[0].substring(5));
            String[] bags = line.split(":")[1].trim().split(";");

            boolean isGamePossible = true;
            for (String bag : bags) {
                Map<String, Integer> maxByColour = Map.of("red", 12, "green", 13, "blue", 14);
                for (String colourValue : bag.split(",")) {
                    if (!isGamePossible(maxByColour, colourValue)) {
                        isGamePossible = false;
                    }
                }
            }
            if (isGamePossible) {
                idCount += gameNumber;
            }
        }

        System.out.println("Part A: " + idCount);

        int powerSum = 0;
        for (String line : input) {
            String[] bags = line.split(":")[1].trim().split(";");

            boolean isGamePossible = true;
            int maxBlue = 0, maxGreen = 0, maxRed = 0;
            for (String bag : bags) {
                for (String colourValue : bag.split(",")) {
                    switch (colourValue.trim().split(" ")[1]) {
                        case "red" -> maxRed = Math.max(maxRed, Integer.parseInt(colourValue.trim().split(" ")[0]));
                        case "blue" -> maxBlue = Math.max(maxBlue, Integer.parseInt(colourValue.trim().split(" ")[0]));
                        case "green" -> maxGreen = Math.max(maxGreen, Integer.parseInt(colourValue.trim().split(" ")[0]));
                    }
                }
            }
            int power = maxGreen * maxRed * maxBlue;
            powerSum += power;
        }

        System.out.println("Part B: " + powerSum);
    }

    private static boolean isGamePossible(Map<String, Integer> maxByColour, String colourValue) {
        int amount = Integer.parseInt(colourValue.trim().split(" ")[0]);
        String colour = colourValue.trim().split(" ")[1];

        return maxByColour.get(colour) >= amount;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2023, 2);
        InputUtil.retrieveAndSaveProblemIfNecessary(2023, 2);
        return InputUtil.readCachedInput(2023, 2);
    }

}
