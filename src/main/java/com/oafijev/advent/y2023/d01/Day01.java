package com.oafijev.advent.y2023.d01;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Day01 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int accumulator = 0;
        for (String line : input) {
            accumulator += getFirstDigit(line) * 10 + getFirstDigit((new StringBuilder(line)).reverse().toString());
        }
        System.out.println("Part A: " + accumulator);
        accumulator = 0;
        for (String line : input) {
            line = line.replace("one", "on1ne");
            line = line.replace("two", "tw2wo");
            line = line.replace("three", "thre3hree");
            line = line.replace("four", "fou4our");
            line = line.replace("five", "fiv5ive");
            line = line.replace("six", "si6ix");
            line = line.replace("seven", "seve7even");
            line = line.replace("eight", "eigh8ight");
            line = line.replace("nine", "nin9ine");
            accumulator += getFirstDigit(line) * 10 + getFirstDigit((new StringBuilder(line)).reverse().toString());
        }
        System.out.println("Part B: " + accumulator);
    }

    private static int getFirstDigit(String line) {
        for (char c : line.toCharArray()) {
            if (Character.isDigit(c)) {
                return c - '0';
            }
        }
        throw new IllegalArgumentException();
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2023, 1);
        InputUtil.retrieveAndSaveProblemIfNecessary(2023, 1);
        return InputUtil.readCachedInput(2023, 1);
    }

}
