package com.oafijev.advent.y2022.d03;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day03 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);

        int priorities = 0;

        for (String line : input) {
            int length = line.length();
            String first = line.substring(0, length / 2);
            String last = line.substring(length / 2, length);

            for (char c : first.toCharArray()) {
                if (last.contains("" + c)) {
                    priorities += getPriority(c);
                    break;
                }
            }
        }

        System.out.println("Part A: " + priorities);

        int badges = 0;
        for (int i = 0; i < input.size(); i = i + 3) {
            String line0 = input.get(i);
            String line1 = input.get(i+1);
            String line2 = input.get(i+2);
            Set<Character> set0 = new HashSet<>();
            Set<Character> set1 = new HashSet<>();
            Set<Character> set2 = new HashSet<>();
            line0.chars().forEach(c -> set0.add((char) c));
            line1.chars().forEach(c -> set1.add((char) c));
            line2.chars().forEach(c -> set2.add((char) c));

            set0.retainAll(set1);
            set0.retainAll(set2);
            badges += getPriority(set0.stream().findFirst().get());
        }

        System.out.println("Part b: " + badges);

    }

    private static int getPriority(char c) {
        if (c <= 'z' && c >= 'a') {
            return 1 + c - 'a';
        } else {
            return 27 + c - 'A';
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 3);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 3);
        return InputUtil.readCachedInput(2022, 3);
    }

}
