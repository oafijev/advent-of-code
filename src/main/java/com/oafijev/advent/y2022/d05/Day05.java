package com.oafijev.advent.y2022.d05;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class Day05 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);

        int blankLineIndex = 0;
        for (String line : input) {
            if (line.isBlank()) {
                break;
            }
            blankLineIndex++;
        }

        int buckets = Arrays.stream(input.get(blankLineIndex - 1).split(" ")).filter(s -> !s.isBlank()).toList().size();
        System.out.println(buckets);

        Stack<String>[] stacks = new Stack[buckets];
        for (int i = 0; i < buckets; i++) {
            stacks[i] = new Stack<>();
        }

        Stack<String>[] newStacks = new Stack[buckets];
        for (int i = 0; i < buckets; i++) {
            newStacks[i] = new Stack<>();
        }

        for (int i = blankLineIndex - 2; i >= 0; i--) {
            for (int j = 0; j < buckets; j++) {
                int prospectiveIndex = 1 + j * 4;
                String line = input.get(i);
                if (prospectiveIndex >= line.length()) {
                    continue;
                }
                char c = line.charAt(prospectiveIndex);
                if (c >= 'A' && c <= 'Z') {
                    stacks[j].push("" + c);
                    newStacks[j].push("" + c);
                }
            }
        }

        for (int i = blankLineIndex + 1; i < input.size(); i++) {
            String line = input.get(i);
//            System.out.println("args = " + line);
            String[] split = line.split(" ");
            int numberToMove = Integer.parseInt(split[1]);
            int from = Integer.parseInt(split[3]) - 1;
            int to = Integer.parseInt(split[5]) - 1;

            Stack<String> tempStackForNewStack = new Stack<>();
            for (int j = 0; j < numberToMove; j++) {
                stacks[to].push(stacks[from].pop());
                tempStackForNewStack.push(newStacks[from].pop());
            }

            for (int j = 0; j < numberToMove; j++) {
                newStacks[to].push(tempStackForNewStack.pop());
            }
        }
        System.out.println("Part A: " + Arrays.stream(stacks).map(Stack::pop).collect(Collectors.joining()));
        System.out.println("Part B: " + Arrays.stream(newStacks).map(Stack::pop).collect(Collectors.joining()));
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 5);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 5);
        return InputUtil.readCachedInput(2022, 5);
    }

}
