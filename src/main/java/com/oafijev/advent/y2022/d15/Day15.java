package com.oafijev.advent.y2022.d15;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;

import java.io.IOException;
import java.util.*;

public class Day15 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        List<Point> sensors = new ArrayList<>();
        List<Point> beacons = new ArrayList<>();

//        int targetY = 10;
        int targetY = 2000000;

        List<Range> rangesWhereBeaconsCannotExist = getBeaconCannotExistRanges(input, targetY, false);
        rangesWhereBeaconsCannotExist = reduceRanges(rangesWhereBeaconsCannotExist);
        System.out.println("Part A: " + addRanges(rangesWhereBeaconsCannotExist));

        // Sample problem
//        int min = 0;
//        int max = 20;

        // Puzzle input.  Can take a long time to run
//        int min = 0;
//        int max = 4000000;

        // Puzzle input, zoomed in on the solution area
        int min = 3019100;
        int max = 3020000;

        for (int i = min; i < max; i++) {
            List<Range> rangesWhereDistressBeaconCannotExist = getBeaconCannotExistRanges(input, i, true);
            rangesWhereDistressBeaconCannotExist = reduceRanges(rangesWhereDistressBeaconCannotExist);
            if (rangesWhereDistressBeaconCannotExist.size() > 1) {
                for (int j = 0; j < rangesWhereDistressBeaconCannotExist.size() - 1; j++) {
                    Range range = rangesWhereDistressBeaconCannotExist.get(j);
                    Range nextRange = rangesWhereDistressBeaconCannotExist.get(j + 1);
                    if (range.getMax() + 1 < nextRange.getMin()) {
                        System.out.println("Found solution: " + (range.getMax() + 1) + ", " + i);
                        System.out.println("Part B: " + (4000000 * ((long) range.getMax() + 1) + i));
                        break;
                    }
                }
            }
            if (i % 50000 == 0) {
                System.out.println(i);
            }
        }
    }

    private static void log(String s) {
//        System.out.println(s);
    }

    private static List<Range> getBeaconCannotExistRanges(List<String> input, int targetY, boolean includeBeacons) {
        List<Range> beaconCannotExist = new ArrayList<>();
        for (String line : input) {
            int sensorX = Integer.parseInt(line.split(":")[0].split(", ")[0].split("=")[1]);
            int sensorY = Integer.parseInt(line.split(":")[0].split(", ")[1].split("=")[1]);
            int beaconX = Integer.parseInt(line.split("is at")[1].split(", ")[0].split("=")[1]);
            int beaconY = Integer.parseInt(line.split("is at")[1].split(", ")[1].split("=")[1]);

            Range range = calculateBeaconCannotExistRange(sensorX, sensorY, beaconX, beaconY, targetY, includeBeacons);
            if (range != null) {
                beaconCannotExist.add(range);
            }
        }
        return beaconCannotExist;
    }

    private static int addRanges(List<Range> ranges) {
        int count = 0;
        for (Range range : ranges) {
            count += range.getMax() - range.getMin() + 1;
        }
        return count;
    }

    private static List<Range> reduceRanges(List<Range> ranges) {
        Collections.sort(ranges, (o1, o2) -> o1.min == o2.min ? o1.max - o2.max : o1.min - o2.min);

        List<Range> newRange = new ArrayList<>();
        ArrayDeque<Range> deque = new ArrayDeque<>(ranges);

        while(!deque.isEmpty()) {
            if (deque.size() == 1) {
                newRange.add(deque.removeFirst());
                break;
            }

            Range range = deque.removeFirst();
            Range nextRange = deque.removeFirst();
            Range overlap = calculateOverlap(range, nextRange);
            if (overlap == null) {
                newRange.add(range);
                deque.addFirst(nextRange);
            } else {
                deque.addFirst(overlap);
            }
        }

        return newRange;
    }

    private static Range calculateOverlap(Range range1, Range range2) {
        if (range1.getMax() >= range2.getMin() && range1.getMin() <= range2.getMax()) {
            return new Range(Math.min(range1.getMin(), range2.getMin()), Math.max(range1.getMax(), range2.getMax()));
        }
        return null;
    }

    private static Range calculateBeaconCannotExistRange(int sensorX, int sensorY, int beaconX, int beaconY, int targetY, boolean includeBeacons) {
        int distance = getManhattanDistance(sensorX, sensorY, beaconX, beaconY);

        if (Math.abs(sensorY - targetY) > distance) {
            log("No intersection between " + new Point(sensorY, sensorX) + " and target line y=" + targetY);
            return null;
        } else {
            log("Intersection found for " + new Point(sensorY, sensorX) + ", tbd calculation");
            int intersectionLength = distance - Math.abs(sensorY - targetY);
            log("  intersection length: " + intersectionLength);

            int rangeMin = sensorX - intersectionLength;
            int rangeMax = sensorX + intersectionLength;
            if (beaconY == targetY && !includeBeacons) {
                if (intersectionLength == 0) {
                    log("  Not adding range for " + sensorY + ", " + sensorX + ": intersection length 0 and sensor is on the target line.");
                    return null;
                } else if (beaconX == rangeMin) {
                    rangeMin++;
                } else if (beaconX == rangeMax) {
                    rangeMax--;
                }
            }
            return new Range(rangeMin, rangeMax);
        }
    }

    private static int getManhattanDistance(int x1, int y1, int x2, int y2) {
        return Math.abs(x2 - x1) + Math.abs(y2 - y1);
    }

    static class Range {
        private int min;
        private int max;

        public Range(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public int getMin() {
            return min;
        }

        public int getMax() {
            return max;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 15);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 15);
        return InputUtil.readCachedInput(2022, 15);
    }

}
