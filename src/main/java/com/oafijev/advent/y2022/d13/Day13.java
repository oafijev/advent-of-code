package com.oafijev.advent.y2022.d13;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Day13 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int index = 1;
        int inOrderIndicesSum = 0;
        List packets = new ArrayList();
        for (int i = 0; i < input.size(); i++) {
            List list1 = readList(input.get(i++));
            List list2 = readList(input.get(i++));
            packets.add(list1);
            packets.add(list2);

            log("- Compare " + list1 + " vs " + list2);
            boolean rightOrder = isRightOrder(list1, list2, "");
            if (rightOrder) {
                inOrderIndicesSum += index;
            }
            log("  right order? " + rightOrder);
            log("");
            index++;
        }
        System.out.println("Part A: " + inOrderIndicesSum);

        List<List<Integer>> dividerPacket1 = List.of(List.of(6));
        List<List<Integer>> dividerPacket2 = List.of(List.of(2));
        packets.add(dividerPacket1);
        packets.add(dividerPacket2);

        Collections.sort(packets, new Comparator<List>() {
            @Override
            public int compare(List o1, List o2) {
                return isRightOrder(o1, o2, "") ? -1 : 1;
            }
        });
        System.out.println("Part B: " + (packets.indexOf(dividerPacket1) + 1) * (packets.indexOf(dividerPacket2) + 1));
    }

    private static void log(String s) {
//        System.out.println(s);
    }

    private static Boolean isRightOrder(List left, List right, String prefix) {
        for (int i = 0; i < Math.min(left.size(), right.size()); i++) {
//            if (i >= left.size()) {
//                // This can't actually happen
//                log(prefix + "    - Left side ran out of items so inputs are in the right order");
//                return true;
//            } else if (i >= right.size()) {
//                log(prefix + "    - Right side ran out of items so inputs are not in the right order");
//                return false;
//            }

            Object leftItem = left.get(i);
            Object rightItem = right.get(i);

            log(prefix + "  - Compare " + leftItem + " vs " + rightItem);
            if (leftItem instanceof Integer && rightItem instanceof Integer) {
                if ((Integer) leftItem < (Integer) rightItem) {
                    log(prefix + "    - Left side is smaller, so inputs are in the right order.");
                    return true;
                }
                if ((Integer) leftItem > (Integer) rightItem) {
                    log(prefix + "    - Right side is smaller, so inputs are not in the right order.");
                    return false;
                }
            } else { // At least one is a list
                List leftItemList = new ArrayList();
                List rightItemList = new ArrayList();
                if (leftItem instanceof Integer) {
                    leftItemList.add(leftItem);
                } else {
                    leftItemList = (List) leftItem;
                }
                if (rightItem instanceof Integer) {
                    rightItemList.add(rightItem);
                } else {
                    rightItemList = (List) rightItem;
                }
                log(prefix + "  - (After possible conversion) Compare " + leftItemList + " vs " + rightItemList);
                Boolean rightOrder = isRightOrder(leftItemList, rightItemList, prefix + "  ");
                if (rightOrder != null) {
                    return rightOrder;
                }
            }
        }
        if (left.size() < right.size()) {
            log(prefix + "  - Left side ran out of items, so inputs are in the right order.");
            return true;
        } else if (left.size() > right.size()) {
            log(prefix + "    - Right side ran out of items so inputs are not in the right order");
            return false;
        }
        return null;
    }

    private static List readList(String line) throws IOException {
        StringReader reader = new StringReader(line);
        reader.read(); // First char is always [
        return readList(reader);
    }

    private static List readList(StringReader reader) throws IOException {
        List result = new ArrayList();
        boolean inNumber = false;
        String parsedNumber = "";
        while (reader.ready()) {
            char c = (char) reader.read();
            if (c == '[') {
                result.add(readList(reader));
            } else if (c == ']') {
                if (inNumber) {
                    result.add(Integer.parseInt(parsedNumber));
                }
                return result;
            } else if (c == ',') {
                if (inNumber) {
                    result.add(Integer.parseInt(parsedNumber));
                }
                inNumber = false;
                parsedNumber = "";
            } else if (c == 0xffff) {
                if (inNumber) {
                    result.add(Integer.parseInt(parsedNumber));
                }
                return result;
            } else {
                inNumber = true;
                parsedNumber = parsedNumber + c;
            }
        }

        return result;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 13);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 13);
        return InputUtil.readCachedInput(2022, 13);
    }

}
