package com.oafijev.advent.y2022.d16;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Day16 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        PipeNetwork network = new PipeNetwork("AA");
        for (String line : input) {
            String name = line.split(" ")[1];
            int flowRate = Integer.parseInt(line.split("=")[1].split(";")[0]);
            List<String> connectedValves = Arrays.stream(line.replace("valves", "valve").split(" to valve ")[1].split(", ")).collect(Collectors.toList());
            Valve valve = new Valve(name, flowRate, connectedValves);
            network.addValve(valve);
        }

        String currentValveName = network.getStartName();
        int totalPressureRelieved = 0;
        Set<String> alreadyOpened = new HashSet<>();
        int timeRemaining = 30;
//        while(timeRemaining > 0) {
//            Map<String, Integer> valveToTimeMap = calculateValveToTimeMap(network, currentValveName);
//            Map<String, Integer> priorityMap = calculatePriorityMap(network, alreadyOpened, currentValveName, timeRemaining);
//
//            if (!priorityMap.isEmpty()) {
//                Optional<Map.Entry<String, Integer>> first = priorityMap.entrySet().stream().sorted(new Comparator<Map.Entry<String, Integer>>() {
//                    @Override
//                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
//                        return o2.getValue() - o1.getValue();
//                    }
//                }).findFirst();
//
//                if (first.isPresent()) {
//                    Valve nextValve = network.getValve(first.get().getKey());
//                    String nextValveName = nextValve.getName();
//                    timeRemaining = timeRemaining - 1 - valveToTimeMap.get(nextValveName);
//                    totalPressureRelieved += priorityMap.get(nextValveName);
//                    currentValveName = nextValveName;
//                    alreadyOpened.add(currentValveName);
//                    log("Opened " + currentValveName + " next, after travelling " + valveToTimeMap.get(currentValveName) + " for " + priorityMap.get(nextValveName) + " relief in the remaining " + timeRemaining + " minutes.");
//                } else {
//                    throw new IllegalArgumentException("abc");
//                }
//            } else {
//                log("Priority map empty with " + timeRemaining + " minutes left.");
//                break;
//            }
//        }

        System.out.println("Part A: " + totalPressureRelieved);
        System.out.println("Part B: " + 2);
    }

    private static Map<String, Integer> calculatePriorityMap(PipeNetwork network, Set<String> alreadyOpened, String currentValveName, int timeRemaining) {
        Map<String, Integer> priorityMap = new HashMap<>();
        Set<String> visited = new HashSet<>();
        ArrayDeque<Valve> deque = new ArrayDeque<>();
        Map<String, Integer> timeToTravel = new HashMap<>();

        Valve currentValve = network.getValve(currentValveName);
        deque.addLast(currentValve);
        timeToTravel.put(currentValveName, 0);

        while (!deque.isEmpty()) {
            Valve valve = deque.removeFirst();
            int timeToThisValve = timeToTravel.get(valve.getName());
            if (!alreadyOpened.contains(valve.getName()) && !priorityMap.containsKey(valve.getName()) && valve.getFlowRate() > 0) {
                int value = valve.getFlowRate() * (timeRemaining - timeToThisValve - 1);
                if (value > 0 && !valve.getName().equals(currentValveName)) {
                    priorityMap.put(valve.getName(), value);
                }
            }

            for (String nextValveName : valve.getConnectedValveNames()) {
                if (!visited.contains(nextValveName)) {
                    deque.addLast(network.getValve(nextValveName));
                    timeToTravel.put(nextValveName, timeToThisValve + 1);
                }
            }

            visited.add(valve.getName());
        }

        return priorityMap;
    }

    private static Map<String, Integer> calculateValveToTimeMap(PipeNetwork network, String currentValveName) {
        Map<String, Integer> valveToTimeMap = new HashMap<>();
        ArrayDeque<Valve> deque = new ArrayDeque<>();

        Valve currentValve = network.getValve(currentValveName);
        deque.addLast(currentValve);
        valveToTimeMap.put(currentValveName, 0);

        while (!deque.isEmpty()) {
            Valve valve = deque.removeFirst();
            int timeToThisValve = valveToTimeMap.get(valve.getName());

            for (String nextValveName : valve.getConnectedValveNames()) {
                if (!valveToTimeMap.containsKey(nextValveName)) {
                    deque.addLast(network.getValve(nextValveName));
                    valveToTimeMap.put(nextValveName, timeToThisValve + 1);
                }
            }
        }

        return valveToTimeMap;
    }

    private static void log(String s) {
        System.out.println(s);
    }

    static class PipeNetwork {
        private String startName;
        private Map<String, Valve> valves;

        public PipeNetwork(String startName) {
            this.startName = startName;
            this.valves = new HashMap<>();
        }

        public String getStartName() {
            return startName;
        }

        public Map<String, Valve> getValves() {
            return valves;
        }

        public Valve getValve(String name) {
            return this.valves.get(name);
        }

        public void addValve(Valve valve) {
            this.valves.put(valve.getName(), valve);
        }
    }

    static class Valve {
        private String name;
        private int flowRate;
        private List<String> connectedValveNames;

        public Valve(String name, int flowRate, List<String> connectedValveNames) {
            this.name = name;
            this.flowRate = flowRate;
            this.connectedValveNames = connectedValveNames;
        }

        public String getName() {
            return name;
        }

        public int getFlowRate() {
            return flowRate;
        }

        public List<String> getConnectedValveNames() {
            return connectedValveNames;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 16);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 16);
        return InputUtil.readCachedInput(2022, 16);
    }

}
