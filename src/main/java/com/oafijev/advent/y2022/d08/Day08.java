package com.oafijev.advent.y2022.d08;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day08 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        int[][] forest = new int[input.size()][];
        for (int i = 0; i < forest.length; i++) {
            String line = input.get(i);
            forest[i] = new int[line.length()];
            for (int j = 0; j < line.length(); j++) {
                forest[i][j] = Integer.parseInt("" + line.charAt(j));
            }
        }

        int visible = 0;
        for (int i = 0; i < forest.length; i++) {
            for (int j = 0; j < forest[i].length; j++) {
                if (isVisible(forest, i, j)) {
                    visible++;
                }
            }
        }

        int bestScenic = 0;
        for (int i = 0; i < forest.length; i++) {
            for (int j = 0; j < forest[i].length; j++) {
                int scenic = scenic(forest, i, j);
                if (scenic > bestScenic) {
                    bestScenic = scenic;
                }
            }
        }


        System.out.println("Part A: " + visible);
        System.out.println("Part B: " + bestScenic);
    }

    private static int scenic(int[][] forest, int row, int col) {
        int scenicTop = 0;
        int scenicBottom = 0;
        int scenicLeft = 0;
        int scenicRight = 0;
        int height = forest[row][col];

        for (int i = row - 1; i >= 0; i--) {
            scenicTop++;
            if (forest[i][col] >= height) {
                break;
            }
        }

        for (int i = row + 1; i < forest.length; i++) {
            scenicBottom++;
            if (forest[i][col] >= height) {
                break;
            }
        }

        for (int j = col - 1; j >= 0; j--) {
            scenicLeft++;
            if (forest[row][j] >= height) {
                break;
            }
        }

        for (int j = col + 1; j < forest[row].length; j++) {
            scenicRight++;
            if (forest[row][j] >= height) {
                break;
            }
        }

        return scenicBottom * scenicLeft * scenicRight * scenicTop;
    }

    private static boolean isVisible(int[][] forest, int row, int col) {
        if (row == 0 || row == forest.length - 1) {
            return true;
        } else if (col == 0 || col == forest[0].length - 1) {
            return true;
        }

        boolean visibleTop = true;
        boolean visibleBottom = true;
        boolean visibleLeft = true;
        boolean visibleRight = true;
        for (int i = 0; i < row; i++) {
            if (forest[row][col] <= forest[i][col]) {
                visibleTop = false;
                break;
            }
        }

        for (int i = row + 1; i < forest.length; i++) {
            if (forest[row][col] <= forest[i][col]) {
                visibleBottom = false;
                break;
            }
        }

        for (int j = 0; j < col; j++) {
            if (forest[row][col] <= forest[row][j]) {
                visibleLeft = false;
                break;
            }
        }

        for (int j = col + 1; j < forest.length; j++) {
            if (forest[row][col] <= forest[row][j]) {
                visibleRight = false;
                break;
            }
        }

        return visibleBottom || visibleTop || visibleRight || visibleLeft;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 8);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 8);
        return InputUtil.readCachedInput(2022, 8);
    }

}
