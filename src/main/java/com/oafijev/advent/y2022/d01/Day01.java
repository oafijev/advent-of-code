package com.oafijev.advent.y2022.d01;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day01 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);
//        System.out.println(line);

        List<Integer> elves = new ArrayList<>();
        int currentElfCalories = 0;

        for (String line : input) {
            if (line.isBlank()) {
                elves.add(currentElfCalories);
                currentElfCalories = 0;
                continue;
            }
            int calories = Integer.parseInt(line);
            currentElfCalories += calories;
        }

        int size = elves.size();
        System.out.println("Part A = " + elves.stream().sorted().toList().get(size - 1));
        System.out.println("Part B = " + (
                elves.stream().sorted().toList().get(size - 1)
                + elves.stream().sorted().toList().get(size - 2)
                + elves.stream().sorted().toList().get(size - 3)));

    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 1);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 1);
        return InputUtil.readCachedInput(2022, 1);
    }

}
