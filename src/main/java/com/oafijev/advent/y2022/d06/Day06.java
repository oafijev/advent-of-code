package com.oafijev.advent.y2022.d06;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day06 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
//        System.out.println(line);

        for (int i = 0; i < line.length(); i++) {
            if (isNotKey(line, i, 4)) {
                continue;
            }
            System.out.println("Part A: " + (i + 1));
            break;
        }
        for (int i = 0; i < line.length(); i++) {
            if (isNotKey(line, i, 14)) {
                continue;
            }
            System.out.println("Part B: " + (i + 1));
            break;
        }
    }

    private static boolean isNotKey(String line, int lastIndexOfMarker, int lengthOfMarker) {
        if (lastIndexOfMarker < lengthOfMarker) {
            return true;
        }

        for (int i = 0; i < lengthOfMarker - 1; i++) {
            for (int j = i + 1; j < lengthOfMarker; j++) {
                if (line.charAt(lastIndexOfMarker - i) == line.charAt(lastIndexOfMarker - j)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 6);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 6);
        return InputUtil.readCachedInput(2022, 6);
    }

}
