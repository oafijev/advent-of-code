package com.oafijev.advent.y2022.d17;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

public class Day17 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        int lineCount = 0;

        List<char[]> grid = new ArrayList<>();


        int height = 0;
        long rockCount = 0;
        Piece piece = new SquarePiece();
        while (rockCount < 15000) {
//        while (rockCount < 2022) {
            piece = piece.nextPiece();
            int pieceHeight = piece.getHeight();
            while (grid.size() < height + 3 + pieceHeight) {
                grid.add(".......".toCharArray());
            }

            int bottomLeftRow = height + 3;
            int bottomLeftColumn = 2;

            log("--- New rock");
            log("bottom Left point = [" + bottomLeftRow + ", " + bottomLeftColumn + "]");

            while (true) {
                // Jet
                char jet = line.charAt(lineCount++);
                if (lineCount >= line.length()) {
                    log("Resetting lineCount.");
                    lineCount = 0;
                }
                if (jet == '<') {
                    if (piece.freeLeft(grid, bottomLeftRow, bottomLeftColumn)) {
                        log("Jet pushing rock left.");
                        bottomLeftColumn--;
                    }
                } else if (jet == '>') {
                    if (piece.freeRight(grid, bottomLeftRow, bottomLeftColumn)) {
                        log("Jet pushing rock right.");
                        bottomLeftColumn++;
                    }
                }
                log("bottom Left point = [" + bottomLeftRow + ", " + bottomLeftColumn + "]");

                // Fall
                if (piece.freeFall(grid, bottomLeftRow, bottomLeftColumn)) {
                    bottomLeftRow--;
                    log("Rock falls one unit down.");
                } else {
                    piece.freezeToGrid(grid, bottomLeftRow, bottomLeftColumn);
                    log("Rock falls and hit obstacle, freezing in place.");
                    log("bottom Left point = [" + bottomLeftRow + ", " + bottomLeftColumn + "]");
                    break;
                }

                log("bottom Left point = [" + bottomLeftRow + ", " + bottomLeftColumn + "]");
            }

//            if (rockCount % 500 == 0) {
//                printGrid(grid);
//                System.out.println(rockCount);
//            }
            height = calculateNewHeight(grid, height);
            rockCount++;
        }

        System.out.println("Part A: " + height);

        // Find cycles of full row line numbers
        List<Integer> fullRows = calculateFullRows(grid);
        List<Integer> fullRowDifferences = new ArrayList<>();
        for (int i = 0; i < fullRows.size() - 1; i++) {
            int difference = fullRows.get(i) - fullRows.get(i + 1);
            System.out.println("Difference: " + difference);
            fullRowDifferences.add(difference);
        }
        System.out.println("");

        int firstFullRowDifference = fullRowDifferences.get(0);
        int cycleLength = firstFullRowDifference;
        int row = 1;
        while(fullRowDifferences.get(row) != firstFullRowDifference) {
            cycleLength += fullRowDifferences.get(row);
            row++;
        }
        int firstFullRowDifferenceActualRow = fullRowDifferences.stream().mapToInt(Integer::intValue).sum();

        System.out.println("Part B: " + 2);
    }

    private static int calculateNewHeight(List<char[]> grid, int height) {
        while (!".......".equals(new String(grid.get(height)))) {
            height++;
        }
        return height;
    }

    public static void printGrid(List<char[]> grid) {
        for (int i = grid.size() - 1; i >= 0; i--) {
            System.out.printf("%6d |" + new String(grid.get(i)) + "|\n", i);
        }
        System.out.println("+-------+");
        System.out.println("");
    }

    public static List<Integer> calculateFullRows(List<char[]> grid) {
        List<Integer> fullRows = new ArrayList<>();
        for (int i = grid.size() - 1; i >= 0; i--) {
            if ("#######".equals(new String(grid.get(i)))) {
                fullRows.add(i);
            }
        }
        return fullRows;
    }

    public static void log(String s) {
//        System.out.println(s);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 17);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 17);
        return InputUtil.readCachedInput(2022, 17);
    }

    private static abstract class Piece {
        public abstract int getHeight();

        public abstract boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn);

        public abstract boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn);

        public abstract boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn);

        public abstract void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn);

        public abstract Piece nextPiece();
    }

    private static class HorizontalPiece extends Piece {
        public int getHeight() {
            return 1;
        }

        public boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn - 1] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn >= 3) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn + 4] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftRow <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow - 1)[bottomLeftColumn] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 2] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 3] != '.') {
                return false;
            }

            return true;
        }

        public void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            char[] row = grid.get(bottomLeftRow);
            row[bottomLeftColumn] = '#';
            row[bottomLeftColumn + 1] = '#';
            row[bottomLeftColumn + 2] = '#';
            row[bottomLeftColumn + 3] = '#';
        }

        public Piece nextPiece() {
            return new CrossPiece();
        }
    }

    private static class CrossPiece extends Piece {
        public int getHeight() {
            return 3;
        }

        public boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn >= 4) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn + 2] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn + 3] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn + 2] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftRow <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow)[bottomLeftColumn + 2] != '.') {
                return false;
            }

            return true;
        }

        public void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            grid.get(bottomLeftRow)[bottomLeftColumn + 1] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn + 1] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn + 2] = '#';
            grid.get(bottomLeftRow + 2)[bottomLeftColumn + 1] = '#';
        }

        public Piece nextPiece() {
            return new CornerPiece();
        }
    }

    private static class CornerPiece extends Piece {
        public int getHeight() {
            return 3;
        }

        public boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn == 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn + 1] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn >= 4) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn + 3] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn + 3] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn + 3] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftRow <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow - 1)[bottomLeftColumn] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 2] != '.') {
                return false;
            }

            return true;
        }

        public void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            grid.get(bottomLeftRow)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow)[bottomLeftColumn + 1] = '#';
            grid.get(bottomLeftRow)[bottomLeftColumn + 2] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn + 2] = '#';
            grid.get(bottomLeftRow + 2)[bottomLeftColumn + 2] = '#';
        }

        public Piece nextPiece() {
            return new VerticalPiece();
        }
    }

    private static class VerticalPiece extends Piece {
        public int getHeight() {
            return 4;
        }

        public boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn == 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 3)[bottomLeftColumn - 1] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn >= 6) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow + 2)[bottomLeftColumn + 1] != '.'
                    || grid.get(bottomLeftRow + 3)[bottomLeftColumn + 1] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftRow <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow - 1)[bottomLeftColumn] != '.') {
                return false;
            }

            return true;
        }

        public void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            grid.get(bottomLeftRow)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow + 2)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow + 3)[bottomLeftColumn] = '#';
        }

        public Piece nextPiece() {
            return new SquarePiece();
        }
    }

    private static class SquarePiece extends Piece {
        public int getHeight() {
            return 2;
        }

        public boolean freeLeft(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn == 0) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn - 1] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn - 1] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeRight(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftColumn >= 5) {
                return false;
            }

            if (grid.get(bottomLeftRow)[bottomLeftColumn + 2] != '.'
                    || grid.get(bottomLeftRow + 1)[bottomLeftColumn + 2] != '.') {
                return false;
            }

            return true;
        }

        public boolean freeFall(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            if (bottomLeftRow <= 0) {
                return false;
            }

            if (grid.get(bottomLeftRow - 1)[bottomLeftColumn] != '.'
                    || grid.get(bottomLeftRow - 1)[bottomLeftColumn + 1] != '.') {
                return false;
            }

            return true;
        }

        public void freezeToGrid(List<char[]> grid, int bottomLeftRow, int bottomLeftColumn) {
            grid.get(bottomLeftRow)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow)[bottomLeftColumn + 1] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn] = '#';
            grid.get(bottomLeftRow + 1)[bottomLeftColumn + 1] = '#';
        }

        public Piece nextPiece() {
            return new HorizontalPiece();
        }
    }

}
