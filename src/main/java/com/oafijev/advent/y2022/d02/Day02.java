package com.oafijev.advent.y2022.d02;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day02 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        List<String> input = new ArrayList<>();

        int score = 0;
        for (String line : input) {
            score += getScore(line);
        }

        System.out.println("Part A: " + score);

        score = 0;
        for (String line : input) {
            char result = getMyThrow(line.charAt(0), line.charAt(2));
            score += getScore("" + line.charAt(0) + ' ' + result);
        }

        System.out.println("Part B: " + score);
    }

    private static char getMyThrow(char opponent, char necessaryResult) {
        if (opponent == 'A' && necessaryResult == 'X') {
            return 'Z';
        } else if (opponent == 'A' && necessaryResult == 'Y') {
            return 'X';
        } else if (opponent == 'A' && necessaryResult == 'Z') {
            return 'Y';
        } else if (opponent == 'B' && necessaryResult == 'X') {
            return 'X';
        } else if (opponent == 'B' && necessaryResult == 'Y') {
            return 'Y';
        } else if (opponent == 'B' && necessaryResult == 'Z') {
            return 'Z';
        } else if (opponent == 'C' && necessaryResult == 'X') {
            return 'Y';
        } else if (opponent == 'C' && necessaryResult == 'Y') {
            return 'Z';
        } else if (opponent == 'C' && necessaryResult == 'Z') {
            return 'X';
        }
        throw new IllegalArgumentException("oops getMyThrow");
    }

    private static int getScore(String line) {
        if ("A X".equals(line)) {
            return 4;
        } else if ("A Y".equals(line)) {
            return 8;
        } else if ("A Z".equals(line)) {
            return 3;
        } else if ("B X".equals(line)) {
            return 1;
        } else if ("B Y".equals(line)) {
            return 5;
        } else if ("B Z".equals(line)) {
            return 9;
        } else if ("C X".equals(line)) {
            return 7;
        } else if ("C Y".equals(line)) {
            return 2;
        } else if ("C Z".equals(line)) {
            return 6;
        }
        throw new IllegalArgumentException("oops getScore");
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 2);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 2);
        return InputUtil.readCachedInput(2022, 2);
    }

}
