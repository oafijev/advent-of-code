package com.oafijev.advent.y2022.d25;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;

public class Day25 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        long sum = 0;

        for (String line : input) {
            sum += snafuToDecimal(line);
        }

        System.out.println("Part A: " + decimalToSnafu(sum));
        System.out.println("Part B: " + 2);
    }

    private static String decimalToSnafu(long decimal) {
        StringBuilder snafu = new StringBuilder();

        while (decimal != 0) {
            if (decimal % 5 == 0) {
                snafu.insert(0, "0");
            } else if (decimal % 5 == 4) {
                snafu.insert(0, "-");
                decimal = decimal + 1;
            } else if (decimal % 5 == 3) {
                snafu.insert(0, "=");
                decimal = decimal + 2;
            } else if (decimal % 5 == 2) {
                snafu.insert(0, "2");
                decimal = decimal - 2;
            } else if (decimal % 5 == 1) {
                snafu.insert(0, "1");
                decimal = decimal - 1;
            }

            decimal = decimal / 5;
        }

        return snafu.toString();
    }

    private static long snafuToDecimal(String line) {
        ArrayDeque<String> decimalDigits = new ArrayDeque<>(Arrays.stream(line.split("")).toList());
        long powerOfFive = 1;
        long sum = 0;
        while (!decimalDigits.isEmpty()) {
            char digit = decimalDigits.removeLast().charAt(0);
            long digitValue = switch (digit) {
                case '=' -> -2;
                case '-' -> -1;
                case '0' -> 0;
                case '1' -> 1;
                case '2' -> 2;
                default -> 0;
            };
            sum += digitValue * powerOfFive;
            powerOfFive *= 5;
        }
        return sum;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 25);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 25);
        return InputUtil.readCachedInput(2022, 25);
    }

}
