package com.oafijev.advent.y2022.d22;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.oafijev.advent.y2022.d22.Day22.Direction.*;

public class Day22 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        char[][] grid = new char[input.size() - 2][];
        int maxRowLength = 0;
        for (int i = 0; i < input.size() - 2; i++) {
            maxRowLength = Math.max(maxRowLength, input.get(i).length());
        }

        for (int i = 0; i < input.size() - 2; i++) {
            grid[i] = (input.get(i) + " ".repeat(maxRowLength - input.get(i).length())).toCharArray();
        }

        int i = 0;
        int j = input.get(0).indexOf(".");
        Direction direction = RIGHT;

        String directionLine = input.get(input.size() - 1);
        List<Integer> distances = Arrays.stream(directionLine.replace("L", " ").replace("R", " ").split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        List<Character> turns = Arrays.stream(directionLine.replaceAll("\\d", "").split("")).map(c -> c.charAt(0)).collect(Collectors.toList());

        for (int k = 0; k < distances.size(); k++) {
            for (int l = 0; l < distances.get(k); l++) {
                if (isBlocked(grid, i, j, direction)) {
                    break;
                } else {
                    if (direction == RIGHT) {
                        j = getJMoveRight(grid, i, j);
                    } else if (direction == DOWN) {
                        i = getIMoveDown(grid, i, j);
                    } else if (direction == LEFT) {
                        j = getJMoveLeft(grid, i, j);
                    } else if (direction == UP) {
                        i = getIMoveUp(grid, i, j);
                    }
                }
            }

            if (k < turns.size()) {
                direction = turns.get(k) == 'R' ? direction.rightTurn() : direction.leftTurn();
            }
        }

        System.out.println("Part A: " + (1000 * (i + 1) + 4 * (j + 1) + direction.getValue()));


        System.out.println("Part B: " + 2);
    }

    private static int getIMoveUp(char[][] grid, int i, int j) {
        if (i <= 0 || grid[i -1][j] == ' ') {
            int m = i;
            while (m < grid.length && grid[m][j] != ' ') {
                m++;
            }
            i = m - 1;
        } else {
            i--;
        }
        return i;
    }

    private static int getJMoveLeft(char[][] grid, int i, int j) {
        if (j <= 0 || grid[i][j -1] == ' ') {
            int m = j;
            while (m < grid[i].length && grid[i][m] != ' ') {
                m++;
            }
            j = m - 1;
        } else {
            j--;
        }
        return j;
    }

    private static int getIMoveDown(char[][] grid, int i, int j) {
        if (i >= grid.length - 1 || grid[i +1][j] == ' ') {
            int m = i;
            while (m >= 0 && grid[m][j] != ' ') {
                m--;
            }
            i = m + 1;
        } else {
            i++;
        }
        return i;
    }

    private static int getJMoveRight(char[][] grid, int i, int j) {
        if (j >= grid[i].length - 1 || grid[i][j +1] == ' ') {
            int m = j;
            while (m >= 0 && grid[i][m] != ' ') {
                m--;
            }
            j = m + 1;
        } else {
            j++;
        }
        return j;
    }

    private static boolean isBlocked(char[][] grid, int i, int j, Direction direction) {
        if (direction == RIGHT) {
            if (j >= grid[i].length - 1 || grid[i][j + 1] == ' ') {
                int k = j;
                while (k >= 0 && grid[i][k] != ' ') {
                    k--;
                }
                return grid[i][k + 1] == '#';
            } else if (grid[i][j + 1] == '#') {
                return true;
            }
        } else if (direction == DOWN) {
            if (i >= grid.length - 1 || grid[i + 1][j] == ' ') {
                int k = i;
                while (k >= 0 && grid[k][j] != ' ') {
                    k--;
                }
                return grid[k + 1][j] == '#';
            } else if (grid[i + 1][j] == '#') {
                return true;
            }
        } else if (direction == LEFT) {
            if (j <= 0 || grid[i][j - 1] == ' ') {
                int k = j;
                while (k < grid[i].length && grid[i][k] != ' ') {
                    k++;
                }
                return grid[i][k - 1] == '#';
            } else if (grid[i][j - 1] == '#') {
                return true;
            }
        } else if (direction == UP) {
            if (i <= 0 || grid[i - 1][j] == ' ') {
                int k = i;
                while (k < grid.length && grid[k][j] != ' ') {
                    k++;
                }
                return grid[k - 1][j] == '#';
            } else if (grid[i - 1][j] == '#') {
                return true;
            }
        }
        return false;
    }

    enum Direction {
        RIGHT(0),
        DOWN(1),
        LEFT(2),
        UP(3);

        private final int value;

        Direction(int value) {
            this.value = value;
        }

        public Direction rightTurn() {
            switch (this) {
                case UP:
                    return RIGHT;
                case RIGHT:
                    return DOWN;
                case DOWN:
                    return LEFT;
                case LEFT:
                    return UP;
            }
            throw new IllegalArgumentException("Unrecognized current direction while turning right: " + this);
        }

        public Direction leftTurn() {
            switch (this) {
                case UP:
                    return LEFT;
                case RIGHT:
                    return UP;
                case DOWN:
                    return RIGHT;
                case LEFT:
                    return DOWN;
            }
            throw new IllegalArgumentException("Unrecognized current direction while turning left: " + this);
        }

        public int getValue() {
            return value;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 22);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 22);
        return InputUtil.readCachedInput(2022, 22);
    }

}
