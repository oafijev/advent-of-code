package com.oafijev.advent.y2022.d12;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.y2022.d09.Day09;

import java.io.IOException;
import java.util.*;

public class Day12 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        ElevationMap elevationMap = new ElevationMap(input);
        PathCalculator pathCalculator = new PathCalculator(elevationMap);
        pathCalculator.calculateDistanceMap();
        System.out.println("Part A: " + pathCalculator.getDistanceAtEnd());

        List<Point> lowPoints = elevationMap.getAllPointsAtElevation(0);

        int lowestDistance = Integer.MAX_VALUE;
        System.out.println("Considering " + lowPoints.size() + " lowest elevation points.");

        for (Point lowPoint : lowPoints) {
//            System.out.println("Considering point " + lowPoint);
            pathCalculator.calculateDistanceMap(lowPoint.getY(), lowPoint.getX());
            int distanceAtEnd = pathCalculator.getDistanceAtEnd();
            if (distanceAtEnd == Integer.MAX_VALUE) {
//                System.out.println("-- could not find route to end.");
            } else if (distanceAtEnd < lowestDistance) {
//                System.out.println("-- distance was " + distanceAtEnd + ", which is the new best distance.");
                lowestDistance = distanceAtEnd;
            } else {
//                System.out.println("-- distance was " + distanceAtEnd + ", which is not better than the current distance of " + lowestDistance + ".");
            }
        }
        System.out.println("Part B: " + lowestDistance);
    }

    private static class PathCalculator {
        private final ElevationMap elevationMap;
        private Integer[][] distanceMap;

        public PathCalculator(ElevationMap elevationMap) {
            this.elevationMap = elevationMap;
        }

        public void calculateDistanceMap() {
            calculateDistanceMap(elevationMap.iStart, elevationMap.jStart);
        }

        public void calculateDistanceMap(int iStart, int jStart) {
            distanceMap = new Integer[elevationMap.getIMaximum()][elevationMap.getJMaximum()];
            ArrayDeque<Point> deque = new ArrayDeque<>();
            deque.addLast(new Point(iStart, jStart));
            distanceMap[iStart][jStart] = 0;

            while (!deque.isEmpty()) {
                Point point = deque.removeFirst();
                int i = point.getY();
                int j = point.getX();
                int currentStepValue = distanceMap[i][j];

                if (elevationMap.canGoUp(i, j)) {
                    setDistanceAndQueuePointIfShorter(deque, i - 1, j, currentStepValue);
                }
                if (elevationMap.canGoRight(i, j)) {
                    setDistanceAndQueuePointIfShorter(deque, i, j + 1, currentStepValue);
                }
                if (elevationMap.canGoDown(i, j)) {
                    setDistanceAndQueuePointIfShorter(deque, i + 1, j, currentStepValue);
                }
                if (elevationMap.canGoLeft(i, j)) {
                    setDistanceAndQueuePointIfShorter(deque, i, j - 1, currentStepValue);
                }

//                if (deque.isEmpty()) {
//                    printDistanceMap();
//                }
            }
        }

        private void printDistanceMap() {
            for (int i = 0; i < distanceMap.length; i++) {
                for (int j = 0; j < distanceMap[i].length; j++) {

                    if (i == elevationMap.getiEnd() && j == elevationMap.getjEnd()) {
                        System.out.printf("%4d", distanceMap[i][j]);
                    } else {
                        System.out.printf("%5d", distanceMap[i][j]);
                    }
                    System.out.print("" + ((char) (elevationMap.elevations[i][j] + 'a')));
                }
                System.out.println();
            }
            System.out.println();
        }

        private void setDistanceAndQueuePointIfShorter(ArrayDeque<Point> deque, int i, int j, int currentStepValue) {
            Integer existingValue = distanceMap[i][j];
//            if (existingValue == null || existingValue > currentStepValue) {
            if (existingValue == null) {
                distanceMap[i][j] = currentStepValue + 1;
                Point point = new Point(i, j);
                if (!deque.contains(point)) {
                    deque.addLast(point);
                }
            }
        }

        public int getDistanceAtEnd() {
            return Optional.ofNullable(distanceMap[elevationMap.getiEnd()][elevationMap.getjEnd()]).orElse(Integer.MAX_VALUE);
        }
    }

    private static class ElevationMap {
        int[][] elevations;
        int iStart;
        int jStart;
        int iEnd;
        int jEnd;

        public ElevationMap(List<String> input) {
            elevations = new int[input.size()][];
            for (int i = 0; i < input.size(); i++) {
                String line = input.get(i);
                elevations[i] = new int[line.length()];
                for (int j = 0; j < line.length(); j++) {
                    if ('S' == line.charAt(j)) {
                        iStart = i;
                        jStart = j;
                        elevations[i][j] = 0;
                    } else if ('E' == line.charAt(j)) {
                        iEnd = i;
                        jEnd = j;
                        elevations[i][j] = 25;
                    } else {
                        elevations[i][j] = line.charAt(j) - 'a';
                    }
                }
            }
        }

        public int getIMaximum() {
            return elevations.length;
        }

        public int getJMaximum() {
            return elevations[0].length;
        }

        public boolean canGoUp(int i, int j) {
            return (i > 0) && (elevations[i - 1][j] <= (elevations[i][j] + 1));
        }

        public boolean canGoRight(int i, int j) {
            return (j < getJMaximum() - 1) && (elevations[i][j + 1] <= (elevations[i][j] + 1));
        }

        public boolean canGoDown(int i, int j) {
            return (i < getIMaximum() - 1) && (elevations[i + 1][j] <= (elevations[i][j] + 1));
        }

        public boolean canGoLeft(int i, int j) {
            return (j > 0) && (elevations[i][j - 1] <= (elevations[i][j] + 1));
        }

        public int getiEnd() {
            return iEnd;
        }

        public int getjEnd() {
            return jEnd;
        }

        public List<Point> getAllPointsAtElevation(int elevation) {
            List<Point> result = new ArrayList<>();
            for (int i = 0; i < elevations.length; i++) {
                for (int j = 0; j < elevations[i].length; j++) {
                    if (elevations[i][j] == elevation) {
                        result.add(new Point(i, j));
                    }
                }
            }
            return result;
        }
    }

    private static class Point {
        private int x;
        private int y;

        public Point(int y, int x) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Day12.Point point = (Day12.Point) o;
            return x == point.x && y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 12);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 12);
        return InputUtil.readCachedInput(2022, 12);
    }
}
