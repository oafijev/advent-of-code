package com.oafijev.advent.y2022.d04;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day04 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);
//        String line = "8-9,7-8";
        int containsCount = 0;
        int overlapCount = 0;

        for (String line : input) {
            if (contains(line)) {
                containsCount++;
            }
            if (overlap(line)) {
                overlapCount++;
            }
        }
//        System.out.println(line);
        System.out.println("Part A: " + containsCount);
        System.out.println("Part B: " + overlapCount);
    }

    private static boolean contains(String line) {
        String[] elves = line.split(",");
        String[] elf1 = elves[0].split("-");
        String[] elf2 = elves[1].split("-");

        int elf1Start = Integer.parseInt(elf1[0]);
        int elf1End = Integer.parseInt(elf1[1]);
        int elf2Start = Integer.parseInt(elf2[0]);
        int elf2End = Integer.parseInt(elf2[1]);

        return (elf1Start <= elf2Start && elf1End >= elf2End) || (elf2Start <= elf1Start && elf2End >= elf1End);

    }

    private static boolean overlap(String line) {
        String[] elves = line.split(",");
        String[] elf1 = elves[0].split("-");
        String[] elf2 = elves[1].split("-");

        int elf1Start = Integer.parseInt(elf1[0]);
        int elf1End = Integer.parseInt(elf1[1]);
        int elf2Start = Integer.parseInt(elf2[0]);
        int elf2End = Integer.parseInt(elf2[1]);

        return (elf1Start >= elf2Start && elf1Start <= elf2End) || (elf1End >= elf2Start && elf1End <= elf2End)
                || (elf2Start >= elf1Start && elf2Start <= elf1End) || (elf2End >= elf1Start && elf2End <= elf1End);

    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 4);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 4);
        return InputUtil.readCachedInput(2022, 4);
    }

}
