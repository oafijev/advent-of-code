package com.oafijev.advent.y2022.d21;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Long.MAX_VALUE;
import static java.lang.Long.MIN_VALUE;

public class Day21 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        Map<String, Monkey> monkeys = readMonkeys(input, false);
        System.out.println("Part A: " + monkeys.get("root").getValuePartA(monkeys));

        OperationMonkey root = (OperationMonkey) monkeys.get("root");

        ConstantMonkey human = (ConstantMonkey) monkeys.get("humn");

        // Picked these values by hand, one different is greater than zero and one is less.
        long initialLow = 1000000000000L;
        long initialHigh = 10000000000000L;

        long low = initialLow; // eventual value is positive
        human.setNumber(low);
        BigDecimal lowDifference = getDifference(monkeys, root);
        System.out.println(lowDifference);

        long high = initialHigh; // eventual value is negative
        human.setNumber(high);
        BigDecimal highDifference = getDifference(monkeys, root);
        System.out.println(highDifference);

        BigDecimal difference = BigDecimal.ONE;
        while (BigDecimal.ZERO.compareTo(difference) != 0) {
            long mid = (high + low) / 2;
            human.setNumber(mid);
            difference = getDifference(monkeys, root);

            if (BigDecimal.ZERO.compareTo(difference) < 0) {
                low = mid;
            } else if (BigDecimal.ZERO.compareTo(difference) > 0) {
                high = mid;
            } else {
                System.out.println("high, low: " + high + ", " + low);
                System.out.println("Part B: " + mid);
                break;
            }
        }
    }

    private static BigDecimal getDifference(Map<String, Monkey> monkeys, OperationMonkey root) {
        Monkey left = monkeys.get(root.getMonkey1Id());
        Monkey right = monkeys.get(root.getMonkey2Id());
        return left.getValuePartA(monkeys).subtract(right.getValuePartA(monkeys));
    }

    private static Map<String, Monkey> readMonkeys(List<String> input, boolean isPartB) {
        Map<String, Monkey> monkeys = new HashMap<>();
        for(String line : input) {
            String id = line.split(": ")[0];
            String[] instruction = line.split(": ")[1].split(" ");
            if (instruction.length <= 1) {
                monkeys.put(id, new ConstantMonkey(Integer.parseInt(instruction[0])));
            } else {
                String operation = instruction[1];
                if ("root".equals(id) && isPartB) {
                    operation = "=";
                }
                monkeys.put(id, new OperationMonkey(instruction[0], operation, instruction[2]));
            }
        }
        return monkeys;
    }

    interface Monkey {
        BigDecimal getValuePartA(Map<String, Monkey> map);
    }

    static class OperationMonkey implements Monkey {
        String monkey1Id;
        char operation;
        String monkey2Id;

        public OperationMonkey(String monkey1Id, String operation, String monkey2Id) {
            assert operation.length() == 1;
            this.monkey1Id = monkey1Id;
            this.operation = operation.charAt(0);
            this.monkey2Id = monkey2Id;
        }

        @Override
        public BigDecimal getValuePartA(Map<String, Monkey> map) {
            switch(operation) {
                case '+': return map.get(monkey1Id).getValuePartA(map).add(map.get(monkey2Id).getValuePartA(map));
                case '-': return map.get(monkey1Id).getValuePartA(map).subtract(map.get(monkey2Id).getValuePartA(map));
                case '*': return map.get(monkey1Id).getValuePartA(map).multiply(map.get(monkey2Id).getValuePartA(map));
                case '/': return map.get(monkey1Id).getValuePartA(map).divide(map.get(monkey2Id).getValuePartA(map), RoundingMode.HALF_EVEN);
            }
            throw new IllegalArgumentException("no operation found");
        }

        public String getMonkey1Id() {
            return monkey1Id;
        }

        public String getMonkey2Id() {
            return monkey2Id;
        }
    }

    static class ConstantMonkey implements Monkey {
        long number;

        public ConstantMonkey(long number) {
            this.number = number;
        }

        @Override
        public BigDecimal getValuePartA(Map<String, Monkey> map) {
            return BigDecimal.valueOf(number);
        }

        public void setNumber(long number) {
            this.number = number;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 21);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 21);
        return InputUtil.readCachedInput(2022, 21);
    }

}
