package com.oafijev.advent.y2022.d19;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day19 {

    private static final int TIME = 32;
    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        // Test data
        // Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
        // Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.

        int qualityLevel = 0;
//        for (String line : input) {
//            int id = Integer.parseInt(line.split(": ")[0].split(" ")[1]);
//            int oreOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[0].split(" ")[4]);
//            int clayOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[1].split(" ")[4]);
//            int obsidianOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[2].split(" ")[4]);
//            int obsidianClayCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[2].split(" ")[7]);
//            int geodeOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[3].split(" ")[4]);
//            int geodeObsidianCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[3].split(" ")[7]);
//
//            int geodes = calculateMaximumGeodesMined(oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost);
//            int quality = geodes * id;
//            System.out.println(id + ": " + quality);
//            qualityLevel += quality;
//        }

        System.out.println("Use TIME=24 for Part A, TIME=32 for Part B");
        System.out.println("Part A: " + qualityLevel);

        int geodeProduct = 1;
        for (int i = 0; i < 3; i++) {
            String line = input.get(i);
            int id = Integer.parseInt(line.split(": ")[0].split(" ")[1]);
            int oreOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[0].split(" ")[4]);
            int clayOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[1].split(" ")[4]);
            int obsidianOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[2].split(" ")[4]);
            int obsidianClayCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[2].split(" ")[7]);
            int geodeOreCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[3].split(" ")[4]);
            int geodeObsidianCost = Integer.parseInt(line.split(": ")[1].split("\\. ")[3].split(" ")[7]);

            int geodes = calculateMaximumGeodesMined(oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost);
            System.out.println(id + ": " + geodes);
            geodeProduct *= geodes;
        }


        System.out.println("Part B: " + geodeProduct);
    }

    private static int calculateMaximumGeodesMined(int oreOreCost, int clayOreCost, int obsidianOreCost, int obsidianClayCost, int geodeOreCost, int geodeObsidianCost) {
        return recurseState(new State(0, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost, 0, 0, 0, 0, 1, 0, 0, 0));
//        return recurseState("CCCBCBGGG", new State(0, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost, 0, 0, 0, 0, 1, 0, 0, 0));
//        return recurseState("OCCCCCCCBBBBGBGGGGGGGGG", new State(0, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost, 0, 0, 0, 0, 1, 0, 0, 0));
    }

//    private static int recurseState(String instructions, State state) {
    private static int recurseState(State state) {
        if (state.getMinute() >= TIME) {
            return state.getGeodes();
        }

//        State nextState;
//        switch(instructions.charAt(0)) {
//            case 'O':
//                nextState = state.withBuildingOreRobot();
//                break;
//            case 'C':
//                nextState = state.withBuildingClayRobot();
//                break;
//            case 'B':
//                nextState = state.withBuildingObsidianRobot();
//                break;
//            case 'G':
//                nextState = state.withBuildingGeodeRobot();
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + instructions.charAt(0));
//        }
//
//        int geodes = recurseState(instructions.substring(1), nextState);
//        return geodes;

        // Ore robot next
        int oreGeodes = 0;
        if (state.getOreRobots() < 2) {
            oreGeodes = recurseState(state.withBuildingOreRobot());
        }

        // Clay robot next
        int clayGeodes = 0;
        if (state.getClayRobots() < 7) {
            recurseState(state.withBuildingClayRobot());
        }

        // Obsidian robot next
        int obsidianGeodes = recurseState(state.withBuildingObsidianRobot());

        // Geode robot next
        int geodeGeodes = recurseState(state.withBuildingGeodeRobot());
        if (state.getMinute() == 0) {
            System.out.println("here");
        }

        return Math.max(Math.max(Math.max(oreGeodes, clayGeodes), obsidianGeodes), geodeGeodes);
    }


    static class State {
        private int oreRobots;
        private int clayRobots;
        private int obsidianRobots;
        private int geodeRobots;
        private int minute;

        private int ore;
        private int clay;
        private int obsidian;
        private int geodes;

        private int oreOreCost;
        private int clayOreCost;
        private int obsidianOreCost;
        private int obsidianClayCost;
        private int geodeOreCost;
        private int geodeObsidianCost;

        public State(int minute, int oreOreCost, int clayOreCost, int obsidianOreCost, int obsidianClayCost, int geodeOreCost, int geodeObsidianCost,
                     int ore, int clay, int obsidian, int geodes,
                     int oreRobots, int clayRobots, int obsidianRobots, int geodeRobots) {
            this.minute = minute;
            this.oreOreCost = oreOreCost;
            this.clayOreCost = clayOreCost;
            this.obsidianOreCost = obsidianOreCost;
            this.obsidianClayCost = obsidianClayCost;
            this.geodeOreCost = geodeOreCost;
            this.geodeObsidianCost = geodeObsidianCost;
            this.ore = ore;
            this.clay = clay;
            this.obsidian = obsidian;
            this.geodes = geodes;
            this.oreRobots = oreRobots;
            this.clayRobots = clayRobots;
            this.obsidianRobots = obsidianRobots;
            this.geodeRobots = geodeRobots;
        }

        public int getOreRobots() {
            return oreRobots;
        }

        public int getClayRobots() {
            return clayRobots;
        }

        public int getObsidianRobots() {
            return obsidianRobots;
        }

        public int getGeodeRobots() {
            return geodeRobots;
        }

        public int getMinute() {
            return minute;
        }

        public int getOre() {
            return ore;
        }

        public int getClay() {
            return clay;
        }

        public int getObsidian() {
            return obsidian;
        }

        public int getGeodes() {
            return geodes;
        }

        public int getOreOreCost() {
            return oreOreCost;
        }

        public int getClayOreCost() {
            return clayOreCost;
        }

        public int getObsidianOreCost() {
            return obsidianOreCost;
        }

        public int getObsidianClayCost() {
            return obsidianClayCost;
        }

        public int getGeodeOreCost() {
            return geodeOreCost;
        }

        public int getGeodeObsidianCost() {
            return geodeObsidianCost;
        }

        public State withBuildingOreRobot() {
            if (ore >= oreOreCost) {
                return new State(minute + 1, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                        ore - oreOreCost + oreRobots, clay + clayRobots, obsidian + obsidianRobots, geodes + geodeRobots,
                        oreRobots + 1, clayRobots, obsidianRobots, geodeRobots);
            } else {
                int timeRequired = (int) Math.ceil((double) (oreOreCost - ore) / (double) oreRobots) + 1; // Add one to build the robot
                if (timeRequired + minute < TIME) {
                    return new State(minute + timeRequired, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots - oreOreCost, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots + 1, clayRobots, obsidianRobots, geodeRobots);
                } else {
                    timeRequired = TIME - minute;
                    return new State(TIME, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots, geodeRobots);
                }
            }
        }

        public State withBuildingClayRobot() {
            if (ore >= clayOreCost) {
                return new State(minute + 1, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                        ore - clayOreCost + oreRobots, clay + clayRobots, obsidian + obsidianRobots, geodes + geodeRobots,
                        oreRobots, clayRobots + 1, obsidianRobots, geodeRobots);
            } else {
                int timeRequired = (int) Math.ceil((double) (clayOreCost - ore) / (double) oreRobots) + 1;  // Add one to build the robot
                if (timeRequired + minute < TIME) {
                    return new State(minute + timeRequired, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots - clayOreCost, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots + 1, obsidianRobots, geodeRobots);
                } else {
                    timeRequired = TIME - minute;
                    return new State(TIME, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots, geodeRobots);
                }
            }
        }

        public State withBuildingObsidianRobot() {
            if (ore >= obsidianOreCost && clay >= obsidianClayCost) {
                return new State(minute + 1, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                        ore - obsidianOreCost + oreRobots, clay - obsidianClayCost + clayRobots, obsidian + obsidianRobots, geodes + geodeRobots,
                        oreRobots, clayRobots, obsidianRobots + 1, geodeRobots);
            } else {
                if (clayRobots == 0) {
                    // Will never return geodes
                    return new State(TIME + 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
                int oreTimeRequired = (int) Math.ceil((double) (obsidianOreCost - ore) / (double) oreRobots);
                int clayTimeRequired = (int) Math.ceil((double) (obsidianClayCost - clay) / (double) clayRobots);
                int timeRequired = Math.max(oreTimeRequired, clayTimeRequired) + 1;  // Add one to build the robot
                if (timeRequired + minute < TIME) {
                    return new State(minute + timeRequired, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots - obsidianOreCost, clay + timeRequired * clayRobots - obsidianClayCost, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots + 1, geodeRobots);
                } else {
                    timeRequired = TIME - minute;
                    return new State(TIME, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots, geodeRobots);

                }
            }
        }

        public State withBuildingGeodeRobot() {
            if (ore >= geodeOreCost && obsidian >= geodeObsidianCost) {
                return new State(minute + 1, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                        ore - geodeOreCost + oreRobots, clay + clayRobots, obsidian - geodeObsidianCost + obsidianRobots, geodes + geodeRobots,
                        oreRobots, clayRobots, obsidianRobots, geodeRobots + 1);
            } else {
                if (obsidianRobots == 0) {
                    // Will never return geodes
                    return new State(TIME + 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
                int oreTimeRequired = (int) Math.ceil((double) (geodeOreCost - ore) / (double) oreRobots);
                int obsidianTimeRequired = (int) Math.ceil((double) (geodeObsidianCost - obsidian) / (double) obsidianRobots);
                int timeRequired = Math.max(oreTimeRequired, obsidianTimeRequired) + 1;  // Add one to build the robot
                if (timeRequired + minute < TIME) {
                    return new State(minute + timeRequired, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots - geodeOreCost, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots - geodeObsidianCost, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots, geodeRobots + 1);
                } else {
                    timeRequired = TIME - minute;
                    return new State(TIME, oreOreCost, clayOreCost, obsidianOreCost, obsidianClayCost, geodeOreCost, geodeObsidianCost,
                            ore + timeRequired * oreRobots, clay + timeRequired * clayRobots, obsidian + timeRequired * obsidianRobots, geodes + timeRequired * geodeRobots,
                            oreRobots, clayRobots, obsidianRobots, geodeRobots);
                }
            }
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 19);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 19);
        return InputUtil.readCachedInput(2022, 19);
    }

}
