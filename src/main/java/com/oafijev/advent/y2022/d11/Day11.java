package com.oafijev.advent.y2022.d11;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day11 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        HashMap<String, Monkey> monkeys = readMonkeys(input);
        runInspectionSimulation(monkeys, 3, 20);
        List<Long> inspectionCounts = monkeys.values().stream().map(Monkey::getInspects).sorted().toList();
        System.out.println("Part A: " + (inspectionCounts.get(inspectionCounts.size() - 1) * inspectionCounts.get(inspectionCounts.size() - 2)));

        monkeys = readMonkeys(input);
        runInspectionSimulation(monkeys, 1, 10000);
        inspectionCounts = monkeys.values().stream().map(Monkey::getInspects).sorted().toList();
        System.out.println("Part B: " + (inspectionCounts.get(inspectionCounts.size() - 1) * inspectionCounts.get(inspectionCounts.size() - 2)));
    }

    private static void runInspectionSimulation(HashMap<String, Monkey> monkeys, int worryFactor, int rounds) {
        BigDecimal FLAT_DIVISOR = monkeys.values().stream().map(Monkey::getFactorToTest).reduce(BigDecimal.ONE, BigDecimal::multiply);

        BigDecimal worryManagementDivisor = new BigDecimal(worryFactor);
        for (int i = 0; i < rounds; i++) {
//            System.out.println("Round " + i);
            for (Monkey monkey : monkeys.values()) {
                while (monkey.hasItems()) {
//                    System.out.println("Monkey " + monkey.getName());
                    BigDecimal itemValue = monkey.dequeue();
//                    System.out.println("  Monkey inspects an item with a worry level of " + itemValue.toString());
                    itemValue = monkey.getOperation().apply(itemValue);
//                    System.out.println(itemValue.toString());
                    itemValue = itemValue.divideToIntegralValue(worryManagementDivisor);
                    BigDecimal[] dividedItemValue = itemValue.divideAndRemainder(monkey.getFactorToTest());
                    if (BigDecimal.ZERO.equals(dividedItemValue[1])) {

                        /*
                          MAGIC HERE
                          Over 10000 rounds, the numbers grow boundlessly so the calculation for 10000 rounds takes too long
                          I didn't work out the math but fortunately it worked - modding the numbers by the product of all
                          test factors preserves the modulo and keeps the numbers small.
                         */
                        monkeys.get(monkey.getTrueTarget()).enqueue(itemValue.divideAndRemainder(FLAT_DIVISOR)[1]);
//                        System.out.println("    Current worry level is divisible by " + monkey.getFactorToTest().toString());
//                        System.out.println("    Item with worry level " + itemValue.toString() + " is thrown to monkey " + monkey.getTrueTarget());
//                        monkeys.get(monkey.getTrueTarget()).enqueue(itemValue);
                    } else {
//                        System.out.println("    Current worry level is not divisible by " + monkey.getFactorToTest().toString());
//                        System.out.println("    Item with worry level " + itemValue.toString() + " is thrown to monkey " + monkey.getFalseTarget());
                        monkeys.get(monkey.getFalseTarget()).enqueue(itemValue);
                    }
                }
            }

            if (i % 1000 == 999) {
                System.out.println(monkeys.values().stream().map(Monkey::getInspects).collect(Collectors.toList()));
            }
        }
    }

    private static HashMap<String, Monkey> readMonkeys(List<String> input) {
        HashMap<String, Monkey> monkeys = new LinkedHashMap<>();
        for (int i = 0; i < input.size(); i++) {
            String name = input.get(i++).split(" ")[1].replace(":", "");
            List<BigDecimal> items = Arrays.stream(input.get(i++).split(": ")[1].split(", ")).map(BigDecimal::new).toList();
            Function<BigDecimal, BigDecimal> operation = getOperation(input.get(i++));
            BigDecimal factorToTest = new BigDecimal(input.get(i++).split(" ")[5]);
            String trueTarget = input.get(i++).split(" ")[9];
            String falseTarget = input.get(i++).split(" ")[9];

            Monkey monkey = new Monkey(name, operation, factorToTest, trueTarget, falseTarget);
            items.forEach(monkey::enqueue);
            monkeys.put(name, monkey);
        }
        return monkeys;
    }

    private static Function<BigDecimal, BigDecimal> getOperation(String line) {
        String operation = line.split(" ")[6];
        String parameter = line.split(" ")[7];

        if ("*".equals(operation)) {
            return (value) -> value.multiply(getValue(parameter, value));
        } else if ("+".equals(operation)) {
            return (value) -> value.add(getValue(parameter, value));
        }

        return null;
    }

    private static BigDecimal getValue(String parameter, BigDecimal value) {
        if ("old".equals(parameter)) {
            return value;
        } else {
            return new BigDecimal(parameter);
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 11);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 11);
        return InputUtil.readCachedInput(2022, 11);
    }

    private static class Monkey {
        private String name;
        private List<BigDecimal> items;
        private Function<BigDecimal, BigDecimal> operation;
        private BigDecimal factorToTest;
        private String trueTarget;
        private String falseTarget;
        private long inspects = 0;

        public Monkey(String name, Function<BigDecimal, BigDecimal> operation, BigDecimal factorToTest, String trueTarget, String falseTarget) {
            this.name = name;
            this.items = new ArrayList<>();
            this.operation = operation;
            this.factorToTest = factorToTest;
            this.trueTarget = trueTarget;
            this.falseTarget = falseTarget;
        }

        public String getName() {
            return name;
        }

        public Function<BigDecimal, BigDecimal> getOperation() {
            return operation;
        }

        public BigDecimal getFactorToTest() {
            return factorToTest;
        }

        public String getTrueTarget() {
            return trueTarget;
        }

        public String getFalseTarget() {
            return falseTarget;
        }

        public boolean hasItems() {
            return items.size() > 0;
        }

        public BigDecimal dequeue() {
            inspects++;
            return items.remove(0);
        }

        public void enqueue(BigDecimal value) {
            items.add(value);
        }

        public long getInspects() {
            return inspects;
        }
    }
}
