package com.oafijev.advent.y2022.d23;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.oafijev.advent.y2022.d23.Day23.Direction.*;

public class Day23 {

    private static final int BUFFER = 100;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int maxX = input.get(0).length();
        int maxY = input.size();
        int elfCount = 0;

        char[][] grove = new char[maxY + 2 * BUFFER][maxX + 2 * BUFFER];
        for (int i = 0; i < grove.length; i++) {
            for (int j = 0; j < grove[i].length; j++) {
                if (i >= BUFFER && i < grove.length - BUFFER && j >= BUFFER && j < grove[i].length - BUFFER) {
                    grove[i][j] = input.get(i - BUFFER).charAt(j - BUFFER);
                    if (grove[i][j] == '#') {
                        elfCount++;
                    }
                } else {
                    grove[i][j] = '.';
                }
            }
        }

        LinkedList<Direction> directions = Stream.of(NORTH, SOUTH, WEST, EAST).collect(Collectors.toCollection(LinkedList::new));
        int round = 1;
        while(true) {
            Map<Point, Point> proposedMoves = new HashMap<>();
            Set<Point> proposedFinalLocations = new HashSet<>();
            Set<Point> duplicatedProposedFinalLocations = new HashSet<>();

            for (int i = 0; i < grove.length; i++) {
                for (int j = 0; j < grove[i].length; j++) {
                    if (grove[i][j] == '#') {
                        Point elf = new Point(i, j);

                        if (isElfAlone(grove, elf)) {
                            continue;
                        }

                        Optional<Point> proposedMove = findProposedMove(grove, elf, directions);
                        proposedMove.ifPresent(p -> {
                            proposedMoves.put(elf, p);
                            if (!proposedFinalLocations.contains(p)) {
                                proposedFinalLocations.add(p);
                            } else {
                                duplicatedProposedFinalLocations.add(p);
                            }
                        });
                    }
                }
            }

            boolean moveActuallyHappened = false;

            for (Map.Entry<Point, Point> entry : proposedMoves.entrySet()) {
                Point startLocation = entry.getKey();
                Point finalLocation = entry.getValue();
                if (duplicatedProposedFinalLocations.contains(finalLocation)) {
                    continue;
                }

                moveActuallyHappened = true;
                assert grove[startLocation.getY()][startLocation.getX()] == '#';
                grove[startLocation.getY()][startLocation.getX()] = '.';
                grove[finalLocation.getY()][finalLocation.getX()] = '#';
            }

//            printGrove(grove);

            Direction firstDirection = directions.removeFirst();
            directions.addLast(firstDirection);

            if (round == 10) {
                System.out.println("Part A: " + getEmptyTilesInMinimalRectangle(elfCount, grove));
            }

            if (!moveActuallyHappened) {
                System.out.println("Part B: " + round);
                break;
            }
            round++;
        }


    }

    private static int getEmptyTilesInMinimalRectangle(int elfCount, char[][] grove) {
        int minI = Integer.MAX_VALUE;
        int maxI = Integer.MIN_VALUE;
        int minJ = Integer.MAX_VALUE;
        int maxJ = Integer.MIN_VALUE;

        for (int i = 0; i < grove.length; i++) {
            for (int j = 0; j < grove[i].length; j++) {
                if (grove[i][j] == '#') {
                    minI = Math.min(i, minI);
                    maxI = Math.max(i, maxI);
                    minJ = Math.min(j, minJ);
                    maxJ = Math.max(j, maxJ);
                }
            }
        }

        return (maxI - minI + 1) * (maxJ - minJ + 1) - elfCount;
    }

    private static void printGrove(char[][] grove) {
        for (char[] chars : grove) {
            for (char aChar : chars) {
                System.out.print(aChar);
            }
            System.out.println();
        }
        System.out.println();
    }

    private static boolean isElfAlone(char[][] grove, Point elf) {
        int i = elf.getY();
        int j = elf.getX();
        return grove[i - 1][j - 1] == '.'
                && grove[i - 1][j] == '.'
                && grove[i - 1][j + 1] == '.'
                && grove[i][j - 1] == '.'
                && grove[i][j + 1] == '.'
                && grove[i + 1][j - 1] == '.'
                && grove[i + 1][j] == '.'
                && grove[i + 1][j + 1] == '.';
    }

    private static Optional<Point> findProposedMove(char[][] grove, Point elf, LinkedList<Direction> directions) {
        for (int i = 0; i < 4; i++) {
            Direction direction = directions.get(i);

            if (isValidProposedMove(grove, elf, direction)) {
                return Optional.of(getValidProposedMove(elf, direction));
            }
        }
        return Optional.empty();
    }

    private static Point getValidProposedMove(Point elf, Direction direction) {
        int i = elf.getY();
        int j = elf.getX();
        switch (direction) {
            case NORTH:
                return new Point(i - 1, j);
            case SOUTH:
                return new Point(i + 1, j);
            case WEST:
                return new Point(i, j - 1);
            case EAST:
                return new Point(i, j + 1);
        }
        throw new IllegalArgumentException("Direction not found calculating getValidProposedMove");
    }

    private static boolean isValidProposedMove(char[][] grove, Point elf, Direction direction) {
        int i = elf.getY();
        int j = elf.getX();
        switch (direction) {
            case NORTH:
                return grove[i - 1][j - 1] == '.' && grove[i - 1][j] == '.' && grove[i - 1][j + 1] == '.';
            case SOUTH:
                return grove[i + 1][j - 1] == '.' && grove[i + 1][j] == '.' && grove[i + 1][j + 1] == '.';
            case WEST:
                return grove[i - 1][j - 1] == '.' && grove[i][j - 1] == '.' && grove[i + 1][j - 1] == '.';
            case EAST:
                return grove[i - 1][j + 1] == '.' && grove[i][j + 1] == '.' && grove[i + 1][j + 1] == '.';
        }
        throw new IllegalArgumentException("Direction not found calculating isValidProposedMove");
    }

    enum Direction {
        NORTH,
        SOUTH,
        WEST,
        EAST
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 23);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 23);
        return InputUtil.readCachedInput(2022, 23);
    }

}
