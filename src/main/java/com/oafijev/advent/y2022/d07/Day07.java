package com.oafijev.advent.y2022.d07;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day07 {

    private static final int TOTAL_DISK_SPACE = 70000000;
    private static final int UNUSED_SPACE_REQUIRED = 30000000;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);

        Session session = new Session();

        for (String line : input) {
            if (line.startsWith("$ ")) {
                String command = line.substring(2);
                String operation = command.split(" ")[0];
                if ("cd".equals(operation)) {
                    String parameter = command.split(" ")[1];
                    session.cd(parameter);
                } else if ("ls".equals(operation)){
                    System.out.println("Action: parsing directory contents for directory: " + session.getCurrentDirectory().getName());
                } else {
                    System.out.println("Command: " + command);
                }
            } else {
                if (line.startsWith("dir")) {
                    String parameter = line.substring(4);
                    session.createDirectoryIfNecessary(parameter);
                } else {
                    int size = Integer.parseInt(line.split(" ")[0]);
                    String name = line.split(" ")[1];
                    session.createFileIfNecessary(name, size);
                }
            }
        }

        session.cd("/");

        System.out.println("Part A: " + sumOfSizesOfDirectoriesNoLargerThan(session.getRoot(), 100000));
        System.out.println("Part B: " + findSizeOfDirectoryToDelete(session.getRoot(), UNUSED_SPACE_REQUIRED - (TOTAL_DISK_SPACE - getTotalSize(session.getRoot()))));
    }

    private static int findSizeOfDirectoryToDelete(Node node, int leastSizeToDelete) {
        int currentSizeToDelete = -1;
        int nodeSize = getTotalSize(node);
        if (nodeSize > leastSizeToDelete && node.isDirectory()) {
            currentSizeToDelete = nodeSize;
        }
        for (Node child : node.getChildren()) {
            int childSizeToDelete = findSizeOfDirectoryToDelete(child, leastSizeToDelete);
            if (childSizeToDelete < currentSizeToDelete && childSizeToDelete != -1) {
                currentSizeToDelete = childSizeToDelete;
            }
        }
        return currentSizeToDelete;
    }

    private static int sumOfSizesOfDirectoriesNoLargerThan(Node node, int maxSize) {
        int accumlatedSize = node.getChildren().stream().map(c -> sumOfSizesOfDirectoriesNoLargerThan(c, maxSize)).mapToInt(Integer::intValue).sum();

        if (node.isDirectory()) {
            int nodeSize = getTotalSize(node);
            if (nodeSize <= maxSize) {
                accumlatedSize += nodeSize;
            }
        }

        return accumlatedSize;
    }

    public static int getTotalSize(Node node) {
        if (!node.isDirectory()) {
            return node.getSize();
        } else {
            return node.getChildren().stream().map(Day07::getTotalSize).mapToInt(Integer::intValue).sum();
        }
    }

    public static class Session {
        private Node root;
        private Node currentDirectory;

        public Node getRoot() {
            return root;
        }

        public Node getCurrentDirectory() {
            return currentDirectory;
        }

        public void cd(String path) {
//            System.out.println("Action: changing to directory: " + path);
            if ("/".equals(path)) {
                if (root == null) {
                    root = new Node(null, "/", 0, true);
                }
                currentDirectory = root;
            } else if ("..".equals(path)) {
                currentDirectory = currentDirectory.getParent();
            } else {
                currentDirectory = currentDirectory.children.stream().filter(node -> path.equals(node.getName())).findFirst().orElseThrow(() -> new IllegalArgumentException("Tried to change to a directory that didn't exist."));
            }
        }

        public void createDirectoryIfNecessary(String name) {
            if (currentDirectory.hasChildWithName(name)) {
                return;
            }
            currentDirectory.addChild(new Node(currentDirectory, name, 0, true));
        }

        public void createFileIfNecessary(String name, int size) {
            if (currentDirectory.hasChildWithName(name)) {
                return;
            }
            currentDirectory.addChild(new Node(currentDirectory, name, size, false));
        }
    }

    public static class Node {
        private String name;
        private boolean isDirectory;
        private int size;
        private Set<Node> children = new HashSet<>();
        private Node parent;

        public Node(Node parent, String name, int size, boolean isDirectory) {
            this.parent = parent;
            this.name = name;
            this.size = size;
            this.isDirectory = isDirectory;
        }

        public Node getParent() {
            return parent;
        }

        public String getName() {
            return name;
        }

        public boolean isDirectory() {
            return isDirectory;
        }

        public int getSize() {
            return size;
        }

        public Set<Node> getChildren() {
            return children;
        }

        public void addChild(Node child) {
            this.children.add(child);
        }

        public boolean hasChildWithName(String name) {
            return children.stream().anyMatch(child -> name.equals(child.getName()));
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 7);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 7);
        return InputUtil.readCachedInput(2022, 7);
    }

}
