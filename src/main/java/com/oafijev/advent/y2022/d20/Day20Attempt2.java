package com.oafijev.advent.y2022.d20;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.sql.Array;
import java.util.*;
import java.util.stream.Collectors;

public class Day20Attempt2 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        List<Node> nodes = new ArrayList<>();

        Node zeroNode = readInputIntoListAndReturnZeroNode(input, nodes, 1);
        mix(nodes, 1);

        System.out.println("Part A: " + (calculateOffsetValueFromNode(1000, zeroNode, nodes.size()) + calculateOffsetValueFromNode(2000, zeroNode, nodes.size()) + calculateOffsetValueFromNode(3000, zeroNode, nodes.size())));

        nodes = new ArrayList<>();
        zeroNode = readInputIntoListAndReturnZeroNode(input, nodes, 811589153);
        mix(nodes, 10);

        System.out.println("Part B: " + (calculateOffsetValueFromNode(1000, zeroNode, nodes.size()) + calculateOffsetValueFromNode(2000, zeroNode, nodes.size()) + calculateOffsetValueFromNode(3000, zeroNode, nodes.size())));
    }

    private static void mix(List<Node> nodes, int times) {
        for (int i = 0; i < times; i++) {
            for (Node node : nodes) {
                move(node, nodes.size());
            }
        }
    }

    private static Node readInputIntoListAndReturnZeroNode(List<String> input, List<Node> nodes, long decryptionKey) {
        Node zeroNode = null;
        for (String line : input) {
            long value = Long.parseLong(line) * decryptionKey;
            Node node = new Node(value);
            nodes.add(node);
            if (value == 0) {
                zeroNode = node;
            }
            if (nodes.size() > 1) {
                Node previous = nodes.get(nodes.size() - 2);
                previous.next = node;
                node.previous = previous;
            }
        }

        // Circularize linked list
        nodes.get(0).previous = nodes.get(nodes.size() - 1);
        nodes.get(nodes.size() - 1).next = nodes.get(0);

        return zeroNode;
    }

    private static void printNodeValues(Node node, int totalNodes) {
        Node current = node;
        List<Long> values = new ArrayList<>();
        for (int i = 0; i < totalNodes; i++) {
            values.add(current.value);
            current = current.next;
        }
        log(values.stream().map(i -> "" + i).collect(Collectors.joining(", ")));

    }

    private static long calculateOffsetValueFromNode(int offset, Node node, int totalNodes) {
        offset = offset % totalNodes;
        Node current = node;

        for (int i = 0; i < offset; i++) {
            current = current.next;
        }
        return current.value;
    }

    private static void move(Node node, int totalNodes) {
        removeFromLinkedList(node);
        long amountToMove = Math.floorMod(node.value, (totalNodes - 1));

        // shuttle node through list, not actually adding it until the end
        for (int i = 0; i < amountToMove; i++) {
            node.next = node.next.next;
        }
        node.previous = node.next.previous;

        adjustNewAdjoiningNodes(node);
    }

    private static void removeFromLinkedList(Node node) {
        node.previous.next = node.next;
        node.next.previous = node.previous;
    }

    private static void adjustNewAdjoiningNodes(Node node) {
        node.previous.next = node;
        node.next.previous = node;
    }

    static class Node {
        private long value;
        private Node next;
        private Node previous;

        public Node(long value) {
            this.value = value;
        }
    }

    private static void log(String s) {
        System.out.println(s);
    }


    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 20);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 20);
        return InputUtil.readCachedInput(2022, 20);
    }

}
