package com.oafijev.advent.y2022.d20;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Day20 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        CircularList list = readCircularList(input, 1);
        List<UUID> uuids = list.getUuids();
        mixList(list, uuids, 1);

        System.out.println("Part A: " + (list.getValueFromZeroOffsetBy(1000) + list.getValueFromZeroOffsetBy(2000) + list.getValueFromZeroOffsetBy(3000)));

        list = readCircularList(input, 811589153);
        uuids = list.getUuids();
        mixList(list, uuids, 10);

        System.out.println("Part B: " + (list.getValueFromZeroOffsetBy(1000) + list.getValueFromZeroOffsetBy(2000) + list.getValueFromZeroOffsetBy(3000)));
    }

    private static void mixList(CircularList list, List<UUID> uuids, int times) {
        for (int i = 0; i < times; i++) {
            for (UUID uuid : uuids) {
                list.moveUuid(uuid);
            }
        }
    }

    private static CircularList readCircularList(List<String> input, long decryptionKey) {
        CircularList list = new CircularList();
        for (String line : input) {
           list.addNumber(Long.parseLong(line) * decryptionKey);
        }
        return list;
    }

    private static void log(String s) {
        System.out.println(s);
    }

    static class CircularList {
        private List<NumberContainer> list = new ArrayList<>();
        private Map<UUID, NumberContainer> map = new HashMap<>();

        public void addNumber(long value) {
            NumberContainer numberContainer = new NumberContainer(value);
            list.add(numberContainer);
            map.put(numberContainer.getUuid(), numberContainer);
        }

        public void moveUuid(UUID uuid) {
            NumberContainer numberContainer = map.get(uuid);
            int index = list.indexOf(numberContainer);
            long value = numberContainer.getValue();
//            log("Moving " + value);
            long amountToMove = value % (list.size() - 1);
            if (index + amountToMove >= list.size()) {
                amountToMove -= (list.size() - 1);
            } else if (index + amountToMove <= 0) {
                amountToMove += (list.size() - 1);
            }
            list.remove(index);
            list.add(index + (int) amountToMove, numberContainer);
        }

        public List<UUID> getUuids() {
            return list.stream().map(NumberContainer::getUuid).collect(Collectors.toList());
        }

        public long getValueFromZeroOffsetBy(int offset) {
            for (int i = 0; i < list.size(); i++) {
                NumberContainer numberContainer = list.get(i);
                if (numberContainer.getValue() == 0) {
                    int offsetFromIndex = offset % list.size();
                    if (i + offsetFromIndex >= list.size()) {
                        offsetFromIndex -= list.size();
                    }
                    return list.get(i + offsetFromIndex).getValue();
                }
            }

            throw new IllegalArgumentException("Did not find zero.");
        }

        @Override
        public String toString() {
            return "CircularList{" +
                    "list=" + list.stream().map(c -> "" + c.getValue()).collect(Collectors.joining(", ")) +
                    '}';
        }
    }

    static class NumberContainer {
        private UUID uuid;
        private long value;

        public NumberContainer(long value) {
            this.value = value;
            this.uuid = UUID.randomUUID();
        }

        public UUID getUuid() {
            return uuid;
        }

        public long getValue() {
            return value;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 20);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 20);
        return InputUtil.readCachedInput(2022, 20);
    }

}
