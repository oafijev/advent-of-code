package com.oafijev.advent.y2022.d10;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day10 {

    private static final int MAX_ROW = 40;

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int signal = 0;

        Machine machine = new Machine();
        String row = "";

        for (String line : input) {
//            System.out.println("====");
//            System.out.println(line);
            String[] split = line.split(" ");
            String operation = split[0];
            String parameter = split.length >= 2 ? split[1] : null;

//            System.out.println("Start cycle " + machine.getCycle() + ": begin executing " + line);
            machine.operation(operation, parameter);

            while (!machine.ready()) {

                int cycle = machine.getCycle();
                int position = (cycle - 1) % 40;
                int x = machine.getX();
//                System.out.println("During cycle " + machine.getCycle() + ": CRT draws pixel in position " + position);

                if (position <= x + 1 && position >= x - 1) {
                    System.out.print("#");
                    row = row + "#";
                } else {
                    System.out.print(".");
                    row = row + ".";
                }
//                System.out.println("Current CRT row: " + row);

                if (position % 40 == 39) {
                    row = "";
                    System.out.println();
                }

                machine.tick();


//                System.out.println("Cycle = " + machine.getCycle());
//                System.out.println("X = " + machine.getX());

                if (cycle % 40 == 20) {
                    signal += cycle * x;
                }
            }
        }


        System.out.println("Part A: " + signal);
        System.out.println("Part B: see output above");
    }

    public static class Machine {
        int cycle = 1;
        int x = 1;

        String operation;
        String parameter;
        int operationCyclesRemaining = 0;

        public void operation(String operation, String parameter) {
            this.operation = operation;
            if ("noop".equals(operation)) {
                this.operationCyclesRemaining = 1;
            } else if ("addx".equals(operation)) {
                this.parameter = parameter;
                this.operationCyclesRemaining = 2;
            }
        }

        public boolean ready() {
            return operationCyclesRemaining == 0;
        }

        public void tick() {
            operationCyclesRemaining = Math.max(0, operationCyclesRemaining - 1);

            if (operationCyclesRemaining == 0 && null != operation) {
//                System.out.print("End of cycle " + cycle + ": finish executing " + operation + " " + parameter);
                if ("noop".equals(operation)) {
                } else if ("addx".equals(operation)) {
                    x += Integer.parseInt(parameter);
                }
//                System.out.println(" (Register is now " + x + ")");
                parameter = null;
                operation = null;
            }

            cycle++;
//            System.out.println();
        }

        public int getCycle() {
            return cycle;
        }

        public int getX() {
            return x;
        }
    }
    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 10);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 10);
        return InputUtil.readCachedInput(2022, 10);
    }

}
