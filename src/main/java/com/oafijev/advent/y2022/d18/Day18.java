package com.oafijev.advent.y2022.d18;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;
import com.oafijev.advent.common.Point3D;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day18 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int[][][] droplet = readDroplet(input);

        System.out.println("Part A: " + getSurfaceArea(input, droplet));

        System.out.println("Part B: " + getExteriorSurfaceArea(droplet, 0, 0, 0));
    }

    private static int getExteriorSurfaceArea(int[][][] droplet, int startI, int startJ, int startK) {
        int surfaceArea = 0;
        int maxI = droplet.length;
        int maxJ = droplet[0].length;
        int maxK = droplet[0][0].length;

        ArrayDeque<Point3D> deque = new ArrayDeque<>();
        Set<Point3D> visited = new HashSet<>();
        deque.addLast(new Point3D(startK, startJ, startI));

        while(!deque.isEmpty()) {
            Point3D point = deque.removeFirst();
            if (visited.contains(point)) {
                continue;
            }
            visited.add(point);
            int i = point.getX();
            int j = point.getY();
            int k = point.getZ();
            if (i > 0 && !visited.contains(new Point3D(k, j, i - 1))) {
                if (droplet[i-1][j][k] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k, j, i - 1));
                }
            }
            if (i < maxI - 1 && !visited.contains(new Point3D(k, j, i + 1))) {
                if (droplet[i+1][j][k] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k, j, i + 1));
                }
            }
            if (j > 0 && !visited.contains(new Point3D(k, j - 1, i))) {
                if (droplet[i][j-1][k] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k, j - 1, i));
                }
            }
            if (j < maxJ - 1 && !visited.contains(new Point3D(k, j + 1, i))) {
                if (droplet[i][j+1][k] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k, j + 1, i));
                }
            }
            if (k > 0 && !visited.contains(new Point3D(k - 1, j, i))) {
                if (droplet[i][j][k - 1] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k - 1, j, i));
                }
            }
            if (k < maxK - 1 && !visited.contains(new Point3D(k + 1, j, i))) {
                if (droplet[i][j][k+1] == 1) {
                    surfaceArea++;
                } else {
                    deque.addLast(new Point3D(k + 1, j, i));
                }
            }
        }


        return surfaceArea;
    }

    private static int[][][] readDroplet(List<String> input) {
        int maxI = 0;
        int maxJ = 0;
        int maxK = 0;
        for (String line : input) {
            int i = Integer.parseInt(line.split(",")[0]);
            int j = Integer.parseInt(line.split(",")[1]);
            int k = Integer.parseInt(line.split(",")[2]);
            maxI = Math.max(i, maxI);
            maxJ = Math.max(j, maxJ);
            maxK = Math.max(k, maxK);
        }

        int[][][] droplet = new int[maxI + 3][maxJ + 3][maxK + 3];
        for (String line : input) {
            int i = Integer.parseInt(line.split(",")[0]);
            int j = Integer.parseInt(line.split(",")[1]);
            int k = Integer.parseInt(line.split(",")[2]);
            droplet[i+1][j+1][k+1] = 1;
        }
        return droplet;
    }

    private static int getSurfaceArea(List<String> input, int[][][] droplet) {
        int surfaceArea = 0;
        for (String line : input) {
            int i = Integer.parseInt(line.split(",")[0]) + 1;
            int j = Integer.parseInt(line.split(",")[1]) + 1;
            int k = Integer.parseInt(line.split(",")[2]) + 1;

            if (droplet[i-1][j][k] == 0) {
                surfaceArea++;
            }
            if (droplet[i][j-1][k] == 0) {
                surfaceArea++;
            }
            if (droplet[i][j][k-1] == 0) {
                surfaceArea++;
            }
            if (droplet[i+1][j][k] == 0) {
                surfaceArea++;
            }
            if (droplet[i][j+1][k] == 0) {
                surfaceArea++;
            }
            if (droplet[i][j][k+1] == 0) {
                surfaceArea++;
            }
        }
        return surfaceArea;
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 18);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 18);
        return InputUtil.readCachedInput(2022, 18);
    }

}
