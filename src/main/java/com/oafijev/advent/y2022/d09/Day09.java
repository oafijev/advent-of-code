package com.oafijev.advent.y2022.d09;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.*;

public class Day09 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        System.out.println("Part A: " + run(input, 2).size());
        System.out.println("Part B: " + run(input, 10).size());
    }

    private static Set<Point> run(List<String> input, int segments) {
        List<Point> snake = new ArrayList<>();
        for (int i = 0; i < segments; i++) {
            snake.add(new Point(0, 0));
        }

        Set<Point> tailVisited = new HashSet<>();
        tailVisited.add(snake.get(snake.size() - 1));
        for (String line : input) {
            String[] split = line.split(" ");
            String direction = split[0];
            int distance = Integer.parseInt(split[1]);
            for (int i = 0; i < distance; i++) {
                Point next = getMovedPoint(snake.get(0), direction);
                snake.get(0).setX(next.getX());
                snake.get(0).setY(next.getY());
                for (int j = 0; j < snake.size() - 1; j++) {
                    Point segment = snake.get(j);
                    Point nextSegment = snake.get(j+1);

                    if (tailNeedsToMove(segment, nextSegment)) {
                        Point movedTail = newTailPosition(segment, nextSegment);
                        snake.get(j+1).setY(movedTail.getY());
                        snake.get(j+1).setX(movedTail.getX());
                        if (j+1 == snake.size() - 1) {
                            tailVisited.add(new Point(snake.get(j + 1).getY(), snake.get(j + 1).getX()));
                        }
                    }

                }
            }
        }
        return tailVisited;
    }

    private static Point getMovedPoint(Point currentHead, String direction) {
        if ("R".equals(direction)) {
            return  new Point(currentHead.getY(), currentHead.getX() + 1);
        } else if ("L".equals(direction)) {
            return new Point(currentHead.getY(), currentHead.getX() - 1);
        } else if ("U".equals(direction)) {
            return new Point(currentHead.getY() + 1, currentHead.getX());
        } else if ("D".equals(direction)) {
            return new Point(currentHead.getY() - 1, currentHead.getX());
        }
        throw new IllegalArgumentException("Direction " + direction + " not known");
    }

    private static Point newTailPosition(Point head, Point tail) {
        boolean yMove = Math.abs(head.getY() - tail.getY()) >= 2;
        boolean xMove = Math.abs(head.getX() - tail.getX()) >= 2;

        int xDiff = 0;
        int yDiff = 0;

        if (head.getX() - tail.getX() >= 2) {
            xDiff--;
        } else if (tail.getX() - head.getX() >= 2) {
            xDiff++;
        }

        if (head.getY() - tail.getY() >= 2) {
            yDiff--;
        } else if (tail.getY() - head.getY() >= 2) {
            yDiff++;
        }

        return new Point(head.getY() + yDiff, head.getX() + xDiff);
    }

    private static boolean tailNeedsToMove(Point head, Point tail) {
        return Math.abs(head.getX() - tail.getX()) >= 2 || Math.abs(head.getY() - tail.getY()) >= 2;
    }


    private static class Point {
        private int x;
        private int y;

        public Point(int y, int x) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x && y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 9);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 9);
        return InputUtil.readCachedInput(2022, 9);
    }

}
