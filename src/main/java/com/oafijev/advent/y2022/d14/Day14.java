package com.oafijev.advent.y2022.d14;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.y2022.d12.Day12;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Day14 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        int maxI = 0;
        int maxJ = 0;
        int minJ = Integer.MAX_VALUE;

        for (String line : input) {
            String[] coordStrings = line.split(" -> ");
            for (String coordString : coordStrings) {
                String[] coordStringSplit = coordString.split(",");
                int i = Integer.parseInt(coordStringSplit[1]);
                int j = Integer.parseInt(coordStringSplit[0]);

                if (i > maxI) {
                    maxI = i;
                }

                if (j > maxJ) {
                    maxJ = j;
                }

                if (j < minJ) {
                    minJ = j;
                }
            }
        }


        int sourceJIndex = 500 - minJ;
        System.out.println("Shift everything " + minJ + " cells to the left.");
        List<List<Point>> rocks = parseRocks(input, minJ);

        char[][] grid = calculateGrid(maxI, maxJ, minJ, rocks);
        System.out.println("Part A: " + calculateTrappedSand(grid, sourceJIndex));

        minJ = 0;
        maxI = maxI + 2;
        maxJ = 1000;
        sourceJIndex = 500;

        rocks = parseRocks(input, minJ);
        rocks.add(List.of(new Point(maxI, minJ), new Point(maxI, maxJ)));
        grid = calculateGrid(maxI, maxJ, minJ, rocks);
        System.out.println("Part B: " + calculateTrappedSand(grid, sourceJIndex));
    }

    private static List<List<Point>> parseRocks(List<String> input, int minJ) {
        List<List<Point>> rocks = new ArrayList<>();
        for (String line : input) {
            String[] coordStrings = line.split(" -> ");
            List<Point> rockPath = new ArrayList<>();
            for (String coordString : coordStrings) {
                String[] coordStringSplit = coordString.split(",");
                int i = Integer.parseInt(coordStringSplit[1]);
                int j = Integer.parseInt(coordStringSplit[0]);

                rockPath.add(new Point(i, j - minJ));
            }
            rocks.add(rockPath);
        }
        return rocks;
    }

    private static char[][] calculateGrid(int maxI, int maxJ, int minJ, List<List<Point>> rocks) {
        char[][] grid = new char[maxI + 1][maxJ - minJ + 1];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = '.';
            }
        }

        for (List<Point> rockPath : rocks) {
            for (int k = 0; k < rockPath.size() - 1; k++) {
                Point beginRock = rockPath.get(k);
                Point endRock = rockPath.get(k + 1);
                int beginI = beginRock.getY();
                int endI = endRock.getY();
                int beginJ = beginRock.getX();
                int endJ = endRock.getX();

                if (beginJ == endJ) {
                    assert beginI != endI;

                    if (beginI > endI) {
                        for (int m = beginI; m >= endI; m--) {
                            grid[m][beginJ] = '#';
                        }
                    } else if (beginI < endI) {
                        for (int m = beginI; m <= endI; m++) {
                            grid[m][beginJ] = '#';
                        }
                    }
                } else if (beginI == endI) {
                    assert beginJ != endJ;

                    if (beginJ > endJ) {
                        for (int m = beginJ; m >= endJ; m--) {
                            grid[beginI][m] = '#';
                        }
                    } else if (beginJ < endJ) {
                        for (int m = beginJ; m <= endJ; m++) {
                            grid[beginI][m] = '#';
                        }
                    }
                }
            }
        }
        return grid;
    }

    private static int calculateTrappedSand(char[][] grid, int sourceJIndex) {
        boolean sandEscaped = false;
        int trappedSand = 0;
        do {
            int i = 0;
            int j = sourceJIndex;

            if (grid[i][j] != '.') {
                System.out.println("Trapped at source location!");
                return trappedSand;
            }

            while (true) {
                if (i >= grid.length - 1) {
                    sandEscaped = true;
                    break;
                }
                if (grid[i+1][j] == '.') {
                    // fall straight down
                    i = i + 1;
                    continue;
                }

                if (j <= 0) {
                    sandEscaped = true;
                    break;
                }

                if (grid[i+1][j-1] == '.') {
                    // fall down left
                    i = i + 1;
                    j = j - 1;
                    continue;
                }

                if (j >= grid[i].length) {
                    sandEscaped = true;
                    break;
                }

                if (grid[i+1][j+1] == '.') {
                    // fall down right
                    i = i + 1;
                    j = j + 1;
                    continue;
                }

                // stuck
                grid[i][j] = 'o';
                trappedSand++;
                break;
            }

        } while(!sandEscaped);
        return trappedSand;
    }

    private static void printGrid(char[][] grid) {
        for (char[] row : grid) {
            System.out.println(new String(row));

        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 14);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 14);
        return InputUtil.readCachedInput(2022, 14);
    }

    private static class Point {
        private int x;
        private int y;

        public Point(int y, int x) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x && y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
