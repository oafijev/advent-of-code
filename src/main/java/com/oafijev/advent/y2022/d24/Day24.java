package com.oafijev.advent.y2022.d24;

import com.oafijev.advent.common.InputUtil;
import com.oafijev.advent.common.Point;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.oafijev.advent.y2022.d24.Day24.Direction.*;

public class Day24 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();

        List<Blizzard> blizzards = new ArrayList<>();
        int rows = input.size() - 2;
        int columns = input.get(0).length() - 2;
        for (int i = 1; i < rows + 1; i++) {
            for (int j = 1; j < columns + 1; j++) {
                char character = input.get(i).charAt(j);
                if ('^' == character) {
                    blizzards.add(new Blizzard(new Point(i - 1, j - 1), UP));
                } else if ('>' == character) {
                    blizzards.add(new Blizzard(new Point(i - 1, j - 1), RIGHT));
                } else if ('v' == character) {
                    blizzards.add(new Blizzard(new Point(i - 1, j - 1), DOWN));
                } else if ('<' == character) {
                    blizzards.add(new Blizzard(new Point(i - 1, j - 1), LEFT));
                }
            }
        }

        printGrid(rows, columns, blizzards);
        advanceBlizzards(rows, columns, blizzards);

        System.out.println();
        System.out.println("Part A: " + 1);
        System.out.println("Part B: " + 2);
    }

    private static List<Blizzard> advanceBlizzards(int rows, int columns, List<Blizzard> blizzards) {
//        blizzards.map
        return null;
    }

    private static void printGrid(int rows, int columns, List<Blizzard> blizzards) {
        char[][] grid = new char[rows][columns];
        for (Blizzard blizzard : blizzards) {
            int i = blizzard.getPosition().getY();
            int j = blizzard.getPosition().getX();
            Direction direction = blizzard.getDirection();
            if (grid[i][j] == (char) 0) {
                grid[i][j] = direction.getDisplay();
            } else if (grid[i][j] == '2') {
                grid[i][j] = '3';
            } else if (grid[i][j] == '3') {
                grid[i][j] = '4';
            } else {
                grid[i][j] = '2';
            }
        }

        System.out.print("#.");
        for (int j = 0; j < columns; j++) {
            System.out.print("#");
        }

        System.out.println();

        for (int i = 0; i < rows; i++) {
            System.out.print("#");
            for (int j = 0; j < columns; j++) {
                System.out.print(grid[i][j] == 0 ? '.' : grid[i][j]);
            }
            System.out.println("#");
        }

        for (int j = 0; j < columns; j++) {
            System.out.print("#");
        }
        System.out.println(".#");
    }

    static class Blizzard {
        private Point position;
        private Direction direction;

        public Blizzard(Point position, Direction direction) {
            this.position = position;
            this.direction = direction;
        }

        public Point getPosition() {
            return position;
        }

        public Direction getDirection() {
            return direction;
        }
    }

    enum Direction {
        UP('^'),
        RIGHT('>'),
        DOWN('v'),
        LEFT('<');

        private final char display;

        Direction(char display) {
            this.display = display;
        }

        public char getDisplay() {
            return display;
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2022, 24);
        InputUtil.retrieveAndSaveProblemIfNecessary(2022, 24);
        return InputUtil.readCachedInput(2022, 24);
    }

}
