package com.oafijev.advent.y2015.d01;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day01 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
//        String line = "()())";

        int floor = line.replace(")", "").length() - line.replace("(", "").length();
        System.out.println("Part A: " + floor);

        floor = 0;
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (c == '(') {
                floor++;
            } else if (c == ')') {
                floor--;
            }

            if (floor == -1) {
                System.out.println("Part B: " + (i + 1));
                break;
            }
        }
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2015, 1);
        InputUtil.retrieveAndSaveProblemIfNecessary(2015, 1);
        return InputUtil.readCachedInput(2015, 1);
    }

}
