package com.oafijev.advent.y2015.d04;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.List;

public class Day04 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
        String line = input.get(0);
        System.out.println(line);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2015, 4);
        InputUtil.retrieveAndSaveProblemIfNecessary(2015, 4);
        return InputUtil.readCachedInput(2015, 4);
    }

}
