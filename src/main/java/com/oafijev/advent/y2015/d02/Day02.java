package com.oafijev.advent.y2015.d02;

import com.oafijev.advent.common.InputUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Day02 {

    public static void main(String[] args) throws Exception {
        List<String> input = readInput();
//        String line = input.get(0);
//        String line = "1x1x10";
        int area = 0;

        for (String line : input) {
            Integer[] sides = Arrays.stream(line.split("x")).map(Integer::parseInt).toList().toArray(new Integer[]{});
            area += 2 * (sides[0] * sides[1] + sides[1] * sides[2] + sides[2] * sides[0]) + Math.min(
                    sides[0] * sides[1], Math.min(sides[1] * sides[2], sides[2] * sides[0]));
        }
        System.out.println("Part A: " + area);

        int length = 0;
        for (String line : input) {
            Integer[] sides = Arrays.stream(line.split("x")).map(Integer::parseInt).toList().toArray(new Integer[]{});
            length += 2 * Math.min(sides[0] + sides[1], Math.min(sides[1] + sides[2], sides[2] + sides[0])) + sides[0] * sides[1] * sides[2];
        }
        System.out.println("Part B: " + length);
    }

    private static List<String> readInput() throws IOException {
        InputUtil.retrieveAndSaveInputFileIfNecessary(2015, 2);
        InputUtil.retrieveAndSaveProblemIfNecessary(2015, 2);
        return InputUtil.readCachedInput(2015, 2);
    }

}
